import numpy as np
import matplotlib.pyplot as plt
import matplotlib
# import warnings
# warnings.simplefilter("ignore")
plt.rcParams['axes.unicode_minus'] = False # 負號問題
plt.rcParams['mathtext.fontset'] = 'custom' 
plt.rcParams['mathtext.it'] = 'STIXGeneral:italic' 
plt.rcParams['mathtext.bf'] = 'STIXGeneral:italic:bold' 
matplotlib.rc('font', family='mingliu') # , weight='bold') # 設定新細明體
# matplotlib.rc('font', family='PingFang HK') # , weight='bold') # 設定蘋方字體

# from matplotlib.font_manager import FontManager
# fm = FontManager()
# mat_fonts = set(f.name for f in fm.ttflist)
# print (mat_fonts)

def PL_Hata(fc,d,htx,hrx,Etype):
  """Hata Pathloss Model
  ===
  https://www.gaussianwaves.com/2019/03/hata-okumura-model-for-outdoor-propagation/

  Args:
      fc (float): carrier frequency[Hz] [ 150 MHz to 1500 MHz] 
      d (float): between base station and mobile station[m], model use km as unit
      htx (float): height of transmitter[m] [ 30 m to 200 m ]
      hrx (float): height of receiver[m] [1m to 10 m]
      Etype (String): Environment Type('open','suburban','small/medium','big')

  Returns:
      float: Path Loss fo Hata model
  """
  d = d/1000
  hrx = np.random.rand(len(d))*10
  fc=fc/(10**6) # formating Hz to MHz for model input
  a_hrx = (1.1*np.log10(fc)-0.7)*hrx-(1.56*np.log10(fc)-0.8) # Eq. (1.8) #open rural, sub-urban, small/medium ciity
  
  # Urban
  if Etype =='small/medium':
    a_hrx = 0.8 + (1.1*np.log10(fc)-0.7)*hrx - 1.56*np.log10(fc)
    C = 0
  if Etype =='big':
    C = 3
    if fc>=150 and fc<=200: # valid - 200MHz metropolitian big city
      a_hrx = 8.29*(np.log10(1.54*hrx)**2)-1.1
    if fc>200: # 200MHz metropolitian 
      a_hrx = 3.2*(np.log10(11.75*hrx)**2)-4.92

  if Etype == 'suburban':
    C = -2*(np.log10(fc/28)**2)-5.4
  

  if Etype == 'open': # open rural
    C = -4.78*(np.log10(fc)**2)+18.33*np.log10(fc)-40.98

  A = 69.55+26.16*np.log10(fc) - 13.82*np.log10(htx) - a_hrx
  B = 44.9-6.55*np.log10(htx)
  
  PL = A+B*np.log10(d)+C
  return PL

# Code
PTx=0.001 ##watt
PTxdBm=10*np.log10(PTx*1000)

fc=900*10**6; htx=70; hrx=1.5
distance=np.linspace(1,20*10**3,1000)
y_suburban = PL_Hata(fc,distance,htx,hrx,'suburban')
y_open = PL_Hata(fc,distance,htx,hrx,'open')
y_small = PL_Hata(fc,distance,htx,hrx,'small/medium')
y_big = PL_Hata(fc,distance,htx,hrx,'big')

# y_suburban = PTxdBm -PL_Hata(fc,distance,htx,hrx,'suburban')
# y_open = PTxdBm -PL_Hata(fc,distance,htx,hrx,'open')
# y_small = PTxdBm -PL_Hata(fc,distance,htx,hrx,'small/medium')
# y_big = PTxdBm-PL_Hata(fc,distance,htx,hrx,'big')

plt.semilogx(distance,y_small,label='小型/中型都市',alpha=0.6)
plt.semilogx(distance,y_big,label='市中心',alpha=0.6)
plt.semilogx(distance,y_suburban,label='郊區',alpha=0.6)
plt.semilogx(distance,y_open,label='空曠空間',alpha=0.6)

# plt.plot(distance,y_small,label='小型/中型都市',alpha=0.6)
# plt.plot(distance,y_big,label='市中心',alpha=0.6)
# plt.plot(distance,y_suburban,label='郊區',alpha=0.6)
# plt.plot(distance,y_open,label='空曠空間',alpha=0.6)


# axis([1 1000 40 110]),
plt.title( r'Hata模型 $H_{tx}$=%.1fm, $H_{rx}$=%.1fm $f_c$=%.1fMHz'%(htx,hrx,fc/10**6) )
# plt.title( r'Hata模型 $H_{tx}$=%.1fm, $H_{rx}$=0~10m $f_c$=%.1fMHz'%(htx,fc/10**6) )
# plt.title( r'Hata模型 $P_{Tx}$=1mW, $H_{tx}$=%.1fm, $H_{rx}$=%.1fm $f_c$=%.1fMHz'%(htx,hrx,fc/10**6) )
# plt.title( r'Hata模型 $P_{Tx}$=1mW, $H_{tx}$=%.1fm, $H_{rx}$=0~10m $f_c$=%.1fMHz'%(htx,fc/10**6) )

plt.xlabel(r'距離[km]'); 
plt.ylabel(r'路徑損耗 $P_{loss}$[dB]')
# plt.ylabel(r'接收平均功率 $P_{rx}$ [dB]')
plt.grid(True,which='both') 
plt.legend()
plt.tight_layout()
plt.show()