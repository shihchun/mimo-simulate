import numpy as np 
import matplotlib.pyplot as plt 
from ModulationPy import PSKModem, QAMModem
from functions import awgn,PL_Hata
from PL3gpp38901 import PL_RMa,PL_UMa,PL_UMi,PL_InH

N = 10**6
N = int(1e5)

d1 = 500; d2 = 100    #Distances of users from base station (BS)
a1 = 0.75; a2 = 0.25   #Power allocation factors
eta = 4
#Generate rayleigh fading coefficient for both users
h1 = np.sqrt(d1**(-eta))*(np.random.randn(N)+1j*np.random.randn(N))/np.sqrt(2)
h2 = np.sqrt(d2**(-eta))*(np.random.randn(N)+1j*np.random.randn(N))/np.sqrt(2)
hh1 = (np.random.randn(N)+1j*np.random.randn(N))/np.sqrt(2)
hh2 = (np.random.randn(N)+1j*np.random.randn(N))/np.sqrt(2)

g1 = (abs(h1))**2
g2 = (abs(h2))**2

Pt = np.arange(0,40+1,2)    #Transmit power in dBm
pt = (10**-3)*10**(Pt/10)   #Transmit power in linear scale
BW = 10**6                  #System bandwidth
No = -174 + 10*np.log10(BW)   #Noise power (dBm)
no = (10**-3)*10**(No/10)   #Noise power (linear scale)

#Generate noise samples for both users
w1 = np.sqrt(no)*(np.random.randn(N)+1j*np.random.randn(N))/np.sqrt(2)
w2 = np.sqrt(no)*(np.random.randn(N)+1j*np.random.randn(N))/np.sqrt(2)

#Generate random binary data for two users

data1 = np.random.randint(0, 1+1, N)  #Data bits of user 1
data2 = np.random.randint(0, 1+1, N)  #Data bits of user 2

#Do BPSK modulation of data
x1 = 2*data1 - 1
x2 = 2*data2 - 1

p = len(Pt)
ber1 = [0]*p
ber2 = [0]*p
ber_th1 = [0]*p
ber_th2 = [0]*p
gam_a = [0]*p
gamma1 = [0]*p
gamma2 = [0]*p
for u in np.arange(p):
    # pr = Pt[u] - ( PATHLOSS - Antenna_gain) 
    # variance = 10**(-pr/10) # to watt linear scale
    # h1 = np.sqrt(variance/2)*h1 # antenna height is random 0~10m
    # G1 = np.sqrt((10**(-(Pt[u]-PL_Hata(900*10**6,d1,70,np.random.rand()*10,'big')+12)/10))/2)

    # pathloss = 148.1+37.6*np.log10(0.01) + 8*np.random.randn() - 10 # pathloss + shadow fading - antenna gain
    # variance = 10**(-pathloss/10)
    # G1 = np.sqrt(variance/2)
    # G1 = 10**(0.1*(Antenna_gain - pathloss))
    
    Antenna_gain=-12
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'big')
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'small/medium')
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'suburban')
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'open')
    # PL_RMa(3.5,d1,70,1.5)
    # PL_UMa(3.5,d1,1.5)
    # PL_UMi(3.5,d1,1.5)
    # PL_InH(3.5,10)
    PL1 = PL_RMa(3.5,d1,70,1.5,condition='NLOS')
    PL2 = PL_RMa(3.5,d2,70,1.5,condition='NLOS')
    G1 = np.sqrt((10**((Pt[u]-(PL1-Antenna_gain) )/10))/2)
    G2 = np.sqrt((10**((Pt[u]-(PL2-Antenna_gain) )/10))/2)
    print("Hata: G1=%f, G2= %f \nsqrt(d^(-eta)): G1=%f, G2=%f" %(G1,G2,np.sqrt(d1**(-eta)),np.sqrt(d2**(-eta))))
    ######### 揭開註解就是用Hata Pathloss，註解掉就是用 log-distance Pt=0dB時候推出來的pathloss np.sqrt(d**(-eta))
    h1 = G1*hh1
    h2 = G2*hh2
    #########
    g1 = (abs(h1))**2
    g2 = (abs(h2))**2
    #Do superposition coding
    x = np.sqrt(pt[u])*(np.sqrt(a1)*x1 + np.sqrt(a2)*x2)
    #Received signals
    y1 = h1*x + w1
    y2 = h2*x + w2
    y1 = awgn(h1*x + w1,Pt[u])
    y2 = awgn(h2*x + w2,Pt[u])
    # Channel Estimation is here
    #Equalize 
    eq1 = y1/h1
    eq2 = y2/h2
    eq1 = awgn(eq1,Pt[u])
    eq2 = awgn(eq2,Pt[u])

    #AT USER 1--------------------
    #Direct decoding of x1 from y1
    x1_hat = np.zeros(N)
    # x1_hat(eq1>0) = 1
    x1_hat[np.where(eq1 >0)]=1
    
    #Compare decoded x1_hat with data1 to estimate BER
    ber1[u] = np.sum( data1 != x1_hat)/N
    #----------------------------------
    
    #AT USER 2-------------------------
    #Direct decoding of x1 from y2
    x12_hat = np.ones(N)
    # x12_hat(eq2<0) = -1
    x12_hat[np.where(eq2<0)]=-1


    y2_dash = eq2 - np.sqrt(a1*pt[u])*x12_hat
    x2_hat = np.zeros(N)
    x2_hat[np.where(np.real(y2_dash)>0)] = 1

    ber2[u] = np.sum( x2_hat != data2)/N
    #-----------------------------------   
    
    gam_a = 2*((np.sqrt(a1*pt[u])-np.sqrt(a2*pt[u]))**2)*np.mean(g1)/no
    gam_b = 2*((np.sqrt(a1*pt[u])+np.sqrt(a2*pt[u]))**2)*np.mean(g1)/no
    ber_th1[u] = 0.25*(2 - np.sqrt(gam_a/(2+gam_a)) - np.sqrt(gam_b/(2+gam_b)))
    
    gam_c = 2*a2*pt[u]*np.mean(g2)/no
    gam_d = 2*((np.sqrt(a2) + np.sqrt(a1))**2)*pt[u]*np.mean(g2)/no
    gam_e = 2*((np.sqrt(a2) + 2*np.sqrt(a1))**2)*pt[u]*np.mean(g2)/no
    gam_f = 2*((-np.sqrt(a2) + np.sqrt(a1))**2)*pt[u]*np.mean(g2)/no
    gam_g = 2*((-np.sqrt(a2) + 2*np.sqrt(a1))**2)*pt[u]*np.mean(g2)/no
    
    gc = (1 - np.sqrt(gam_c/(2+gam_c)))
    gd = (1-np.sqrt(gam_d/(2+gam_d)))
    ge = (1-np.sqrt(gam_e/(2+gam_e)))
    gf = (1-np.sqrt(gam_f/(2+gam_f)))
    gg = (1-np.sqrt(gam_g/(2+gam_g)))
    
    ber_th2[u] = 0.5*gc - 0.25*gd + 0.25*(ge+gf-gg)
    gamma1[u] = a1*pt[u]*np.mean(g1)/(a2*pt[u]*np.mean(g1) + no)
    gamma2[u] = a2*pt[u]*np.mean(g2)/no
    print("BER: \nRAYLEIGH_ONLY_Theo UE1: %e, UE2: %e\nSIM_RAYLEIGH_AWGN UE1: %e, UE2: %e \n\n"%(ber_th1[u],ber_th2[u],ber1[u],ber2[u]))

fig = plt.figure()
plt.semilogy(Pt, ber1,'-.',marker='o',label='Sim. User 1 - Far user %dm'%d1)
plt.semilogy(Pt, ber2,'-.',marker='o',label='Sim. User 2 - Near user %dm'%d2)
plt.semilogy(Pt, ber_th1,'--',linewidth=4,alpha=0.6,label='Theo. User 1 - Far user %dm'%d1)
plt.semilogy(Pt, ber_th2,'--',linewidth=4,alpha=0.6,label='Theo. User 2 - Near user %dm'%d2)
plt.title('NOMA Rayleigh BER')
plt.xlabel('Transmit power (P in dBm)')
plt.ylabel('BER')
plt.legend()
plt.grid(True,which='both')
plt.tight_layout()

fig = plt.figure()
plt.title('Constellation')
plt.plot(np.real( y2), np.imag( y2  ), '.r', label = 'Dec1')
plt.plot(np.real( y2_dash ), np.imag( y2_dash  ), '.g', label = 'Dec2')
plt.xlabel(r'I')
plt.ylabel(r'Q')
plt.legend(loc='upper right')
plt.grid(True,which='both')
plt.tight_layout()
plt.show()

