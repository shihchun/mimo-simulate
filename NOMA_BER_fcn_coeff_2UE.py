import numpy as np
import matplotlib.pyplot as plt
from ModulationPy import PSKModem, QAMModem
from functions import NOMA_SIC,NOMA_SPC,NOMA_channel,awgn,threshold_bit,PL_Hata
import scipy.linalg as la
import matplotlib
from PL3gpp38901 import PL_RMa,PL_UMa,PL_UMi,PL_InH # load pathloss
plt.rcParams['axes.unicode_minus'] = False # 負號問題
plt.rcParams['mathtext.fontset'] = 'custom' 
plt.rcParams['mathtext.it'] = 'STIXGeneral:italic' 
plt.rcParams['mathtext.bf'] = 'STIXGeneral:italic:bold' 
matplotlib.rc('font', family='mingliu') # , weight='bold') # 設定新細明體
# matplotlib.rc('font', family='PingFang HK') # , weight='bold') # 設定蘋方字體

# from matplotlib.font_manager import FontManager
# fm = FontManager()
# mat_fonts = set(f.name for f in fm.ttflist)
# print (mat_fonts)

modem = QAMModem(4)
# modem = PSKModem(2)
# modem.plot_const() # plot constellation
modem.soft_decision = False
BW = 90*10**6
N = 5*10**5
N = 5*10**4
N= 300
Pt = 40
for Pt in [0,10,20,30,40]:
    No = -114
    pt = (10**-3)* 10**(Pt/10) # db2pow(Pt)
    no = (10**-3)* 10**(No/10) # db2pow(No)

    # a1 = 0:0.01:1
    a1 = np.arange(0,1,0.01)
    a2 = 1 - a1

    d1 = 500; d2 = 200

    eta = 4
    # h1 = np.sqrt(d1**(-eta))*(np.random.randn(N) + 1j*np.random.randn(N))/np.sqrt(2) 
    # h2 = np.sqrt(d2**(-eta))*(np.random.randn(N) + 1j*np.random.randn(N))/np.sqrt(2) 
    nSample = int(N/modem.N)
    hh1 = (np.random.randn(nSample) + 1j*np.random.randn(nSample))/np.sqrt(2)
    hh2 = (np.random.randn(nSample) + 1j*np.random.randn(nSample))/np.sqrt(2)

    ber1 = [0]*len(a1)
    ber2 = [0]*len(a1)

    for u in np.arange(len(a1)):
        Antenna_gain=12
        # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'big')
        # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'small/medium')
        # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'suburban')
        # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'open')
        # PL_RMa(3.5,d1,70,1.5)
        # PL_UMa(3.5,d1,1.5)
        # PL_UMi(3.5,d1,1.5)
        # PL_InH(3.5,10)
        PL1 = PL_RMa(3.5,d1,70,1.5,condition='NLOS')
        PL2 = PL_RMa(3.5,d1,70,1.5,condition='NLOS')
        G1 = np.sqrt((10**((Pt-(PL1-Antenna_gain) )/10))/2)
        G2 = np.sqrt((10**((Pt-(PL2-Antenna_gain) )/10))/2)
        h1 = G1*hh1
        h2 = G2*hh2
        h = [h1,h2]
        g1 = (abs(hh1))**2
        g2 = (abs(hh2))**2
        alpha = [a1[u],a2[u]]
        SPC = NOMA_SPC(N, alpha, modem)
        x = SPC['x']
        
        # Equalize 
        y = NOMA_channel(x, h, pt, BW, -174) # SISO
        # modulator additive noise 
        # y[0] = awgn(y[0],Pt)
        # y[1] = awgn(y[1],Pt)

        eq1 = y[0]/h1
        eq2 = y[1]/h2
        # eq1 = awgn(eq1,Pt)
        # eq2 =  awgn(eq2,Pt)
        dec1 = NOMA_SIC(eq1, h1, pt, alpha, modem)
        dec2 = NOMA_SIC(eq2, h2, pt, alpha, modem)
        ber1[u] = np.sum( dec1['0'] != SPC['0'])/N
        ber2[u] = np.sum( dec2['1'] != SPC['1'])/N
        # -----------------------------------   

    plt.semilogy(a1, ber1,'-o',label='弱用戶 d=%dm Pt=%ddB'%(d1,Pt),alpha=0.6)
    plt.semilogy(a1, ber2,'-v',label='強用戶 d=%dm Pt=%ddB'%(d2,Pt),alpha=0.6)

plt.xlabel(r'$\alpha_1$')
plt.ylabel('BER')
plt.title(r'兩個使用者時誤碼率與$\alpha$分配關係')
plt.legend()
plt.grid(True,which='both')
plt.tight_layout()
plt.show()
