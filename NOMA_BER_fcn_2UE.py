import numpy as np
import matplotlib.pyplot as plt
from ModulationPy import PSKModem, QAMModem
from functions import NOMA_SIC,NOMA_SPC,NOMA_channel,awgn,threshold_bit,PL_Hata
import scipy.linalg as la
from PL3gpp38901 import PL_RMa,PL_UMa,PL_UMi,PL_InH # load pathloss
import matplotlib
plt.rcParams['axes.unicode_minus'] = False # 負號問題
plt.rcParams['mathtext.fontset'] = 'custom' 
plt.rcParams['mathtext.it'] = 'STIXGeneral:italic' 
plt.rcParams['mathtext.bf'] = 'STIXGeneral:italic:bold' 
matplotlib.rc('font', family='mingliu') # , weight='bold') # 設定新細明體
# matplotlib.rc('font', family='PingFang HK') # , weight='bold') # 設定蘋方字體

# from matplotlib.font_manager import FontManager
# fm = FontManager()
# mat_fonts = set(f.name for f in fm.ttflist)
# print (mat_fonts)

#Create QPSKModulator and QPSKDemodulator objects
# modem = PSKModem(2）
modem = PSKModem(4) 
modem = QAMModem(4)
# modem.plot_const() # plot constellation
modem.soft_decision = False
# modem.demodulate( modem.modulate([0,1,1,1])).astype(int) # hard decision

N = 5*10**5
N = int(2*1e6)
# N = 3000
Pt = np.arange(0,40+1,2) #Transmit power (dBm) 0:2:40
# Pt = np.arange(0,40+1,5) #Transmit power (dBm) 0:2:40
# Pt = np.arange(-40,50+1,3) #Transmit power (dBm) 0:2:40
# Pt = np.arange(0,20+1,2) #Transmit power (dBm) 0:2:40
pt = (10**(-3))*10**(Pt/10)  #db2pow(Pt) #Transmit power (linear scale)

BW = 10**6   #Bandwidth = 1 MHz

d1 = 1000; d2 = 200	#Distances 
a1 = 0.75; a2 = 0.25   #Power allocation factors

eta = 8	#Path loss exponent
nSample= int(N/modem.N) # qpsk/4QAM

hh1 = (np.random.randn(nSample) + 1j*np.random.randn(nSample))/np.sqrt(2)
hh2 = (np.random.randn(nSample) + 1j*np.random.randn(nSample))/np.sqrt(2)

#Do super position coding
alpha=[a1,a2]
SPC = NOMA_SPC(N, alpha, modem)
x = SPC['x']

ber1 = [0]*len(Pt)
ber2 = [0]*len(Pt)
ber3 = [0]*len(Pt)
comb = np.zeros( (3,nSample),dtype='cfloat' )

for u in np.arange(len(Pt)):
    Antenna_gain=12
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'big')
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'small/medium')
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'suburban')
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'open')
    # PL_RMa(3.5,d1,70,1.5)
    # PL_UMa(3.5,d1,1.5)
    # PL_UMi(3.5,d1,1.5)
    # PL_InH(3.5,10)
    PL1 = PL_RMa(3.5,d1,70,1.5,condition='NLOS')
    PL2 = PL_RMa(3.5,d2,70,1.5,condition='NLOS')
    G1 = np.sqrt((10**((Pt[u]-(PL1-Antenna_gain) )/10))/2)
    G2 = np.sqrt((10**((Pt[u]-(PL2-Antenna_gain) )/10))/2)
    h1 = G1*hh1
    h2 = G2*hh2
    h = [h1,h2] # SISO channel 

    # No = -174 + 10*np.log10(BW)+7	#Noise power (dBm)
    # no = (10**(-3))*10**(No/10) #db2pow(No) #Noise power (linear scale) 
    y = NOMA_channel(x, h, pt[u],BW,-174) # Channel with noise
    d = [d1,d2]
    for _ in range(2):
        # awgn additive noise
        y[_] = awgn(y[_],Pt[u]) # add noise already
        # equalize
        k = y[_]/h[_] # h[_]
        # modulator additive AWGN noise after equalize
        # k = awgn(k,Pt[u])
        # maybe you can do sth
        comb[_] = k

    eq1 = comb[0]
    eq2 = comb[1]
    
    dec1 = NOMA_SIC(eq1, h1, pt[u], alpha, modem)
    dec2 = NOMA_SIC(eq2, h2, pt[u], alpha, modem)
    ber1[u] = np.sum( dec1['0'] != SPC['0'])/N
    ber2[u] = np.sum( dec2['1'] != SPC['1'])/N
    rem1 = dec1['rem0']
    rem2 = dec2['rem1']

    # input signal SPC no need h
    decxx = NOMA_SIC(np.sqrt(pt[u])*x, h[0], pt[u], alpha, modem)
    remm1 = decxx['rem0']
    remm2 = decxx['rem1']

    # compute abs avoid np.sqrt get -value less than 0
    # EVM = np.sqrt( ( (I-I')**2+(Q-Q')**2 ) /N )
    # EVM% = np.sqrt( ( (I-I')**2+(Q-Q')**2 ) /N )*100
    # EVM dB = 20*np.log10(EVM) 3% is ~= -30.45757490560675 dB
    EVM1 = np.sqrt( sum(abs( (rem1.real-remm1.real)**2 -(rem1.imag-remm1.imag)**2 )) /nSample )
    EVM2 = np.sqrt( sum(abs( (rem2.real-remm2.real)**2 -(rem2.imag-remm2.imag)**2 )) /nSample )
    EVM1_dB = 20*np.log10(EVM1)
    EVM2_dB = 20*np.log10(EVM2)

print(EVM1)
print((rem1.real-remm1.real)**2)
fig2 = plt.figure()
plt.semilogy(Pt, ber1,'r-',marker='o',label='UE1 弱用戶 %dm'%(d1))
plt.semilogy(Pt, ber2,'g--',marker='o',label='UE2 強用戶 %dm'%d2)
plt.title('NOMA兩個使用者BER')
plt.xlabel('Transmit power (dBm)')
plt.ylabel('BER')
plt.legend()
plt.grid(True,which='both')
plt.tight_layout()


fig = plt.figure()
plt.title('Constellation')
plt.plot(np.real( rem1), np.imag( rem1  ), 'ro', label = 'UE1, EVM=%.3f'%(EVM1*100)+'%',markerfacecolor='none')
plt.plot(np.real( rem2 ), np.imag( rem2  ), 'gv', label = 'UE2 EVM=%.3f'%(EVM2*100)+'%',markerfacecolor='none')
plt.xlabel(r'I')
plt.ylabel(r'Q')
plt.legend(loc='upper right')
plt.grid(True,which='both')
plt.tight_layout()

fig = plt.figure()
plt.title('疊加碼星座圖')
plt.plot(np.real( remm1), np.imag( remm1  ), 'rx', label = 'UE1')
plt.plot(np.real( remm2 ), np.imag( remm2  ), 'g+', label = 'UE2')
plt.xlabel(r'I')
plt.ylabel(r'Q')
plt.legend(loc='upper right')
plt.grid(True,which='both')
plt.tight_layout()

plt.show()

# from plotly.tools import mpl_to_plotly

# plotly_fig = mpl_to_plotly(fig)
# plotly_fig.update_layout(template="none")
# plotly_fig.write_html("constellation.html")

# plotly_fig = mpl_to_plotly(fig2)
# plotly_fig.update_layout(template="none")
# plotly_fig.write_html("BER.html")