import numpy as np
import matplotlib.pyplot as plt
####Parameters Setting#####
DistanceMsr=5
fc=3e8 # speed of light
BlueTooth=2400*1000000 ##hz    
Zigbee=915.0e6 ##hz

Freq=2.4*10e9 # BlueTooth
Gt=1 ##db
Gr=1 ##db
Dref=1 ##Meter
PTx=0.001 ##watt
sigma=6 ##Sigma from 6 to 12 # Principles of communication systems simulation with wireless application P.548
# mean=0 #
PathLossExponent=2 ## Line Of sight
######   FRIIS Equation #########
# Friis free space propagation model:
#        Pt * Gt * Gr * (Wavelength**2)
#  Pr = --------------------------
#        (4 *np.pi * d)**2 * L

Wavelength=fc/Freq
# PTxdBm=10*np.log10(PTx*1000)
PTxdBm=0
M = Wavelength / (4 * np.pi * Dref) #

#########################################
######  Pr0 with calculator Friis #######
#########################################
Pr0=PTxdBm + Gt + Gr- (20*np.log10(1/M)) ## http://www.daycounter.com/Calculators/Friis-Calculator.phtml
Pr0 = PTxdBm*Gt*Gr*(3*10e8)**2/((4*np.pi*1)**2)*1
# log normal shadowing radio propagation model:
# Pr0 = friss(d0)
# Pr(db) = Pr0(db) - 10*n*np.log(d/d0) + X0
# where X0 is a Gaussian random variable with zero mean and a variance in db
#        Pt * Gt * Gr * (lambda**2)   d0**passlossExp    (X0/10)
#  Pr = --------------------------*-----------------*10
#        (4 *np.pi * d0)**2 * L          d**passlossExp
# get power loss by adding a np.log-normal random variable (shadowing)
# the power loss is relative to that at reference distance d0
#  reset rand does influcence random

#GaussRandom=normrnd(0,6)#mean+np.random.randn()*sigma#    #Help on np.random.randn()
GaussRandom= np.random.randn()# (np.random.randn()*0+0)#
# disp(GaussRandom)
DistanceMsr=1000
Pr=Pr0-(10*PathLossExponent* np.log10(DistanceMsr/Dref))+GaussRandom

for PathLossExponent in [2,3,4,6]:
    d = np.linspace(1,10000+1,300)
    Prr = np.zeros(len(d))
    Prr_free = np.zeros(len(d))
    for i in np.arange(len(d)):
        Prr[i]=Pr0-(10*PathLossExponent* np.log10(d[i] /Dref))+ sigma*np.random.randn()
        M = Wavelength / (4 * np.pi* d[i])**2
        #        Pt * Gt * Gr * (Wavelength**2)
        #  Pr = --------------------------
        #        (4 *np.pi * d)**2 * L
        # Prr_free[i] = PTxdBm + Gt + Gr- (20*np.log10(1/M))
        Prr_free[i] = Pr0-(10*PathLossExponent* np.log10(d[i] /Dref))
    # plt.plot(d,Prr,alpha=0.6)
    plt.plot(d,Prr,alpha=0.6)
    plt.plot(d,Prr_free)

# shallowing

plt.tight_layout()
plt.grid(True,axis='both')
plt.show()
