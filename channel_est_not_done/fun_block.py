import numpy as np
from numpy.linalg import inv, pinv, det, svd, matrix_rank, norm
import scipy.special as sc
from numpy import exp, abs, angle
import matplotlib.pyplot as plt

def NOMA_SPC(N, alpha, modem, Block_size = 64, pilot_per_block=8,repeatbit=1):
    """NOMA superposition coding
    ---
    .. doctest::
        >>> SPC = NOMA_SPC(N, alpha, modem, Block_size = 64, pilot_per_block=1) # one frame one pilot
        >>> alpha=[0.8,0.15,0.05] # power allocation coeff
        >>> SPC = NOMA_SPC(N, alpha, modem)
        >>> x = SPC['x'];
    Args:
        N (int): 64*3000, modem.N*Block_size*Block_num
        alpha (ndarray): noma coef
        modem (Obj): modem of ModulationPY
        Block_size (int, optional): [description]. Defaults to 64.
        pilot (int, optional): [description]. Defaults to 8.

    Returns:
        dict: {x, N, '0', '1'....}
        'x': SPC signal
        'N': samples num after modulation
        '0','1'..., ndarray: label of msg sent
        'pilot':  if pilot is ok to apply will have info
    """
    res = {}
    pilot_dict = {}
    pilot_dict['value'] = modem.code_book[max(modem.code_book)] # value select for spc coding
    pilot_dict['bin'] = np.array([ int(i) for i in max(modem.code_book) ]) # length = modem.N
    pilot_dict['pilot_per_block'] = pilot_per_block
    pilot_dict['size'] = Block_size
    for _ in np.arange(len(alpha)): # ue amout
        temp = []
        if np.mod(N/modem.N,Block_size)==0 and np.mod(Block_size,pilot_per_block)==0:
            pilot_dict['state'] = True
            pilot_dict['block_num'] = N//modem.N//Block_size # (N/2) qpsk samples / Block_size = Block nums
            for i in np.arange(N//modem.N // (Block_size//pilot_per_block) ): # per block 64 point with pilot 
                temp = np.hstack([temp, np.hstack([ pilot_dict['bin'] ,np.random.binomial(n=1, p=0.5, size=(((Block_size//pilot_per_block)-1)*modem.N, ))]) ] )
        else:
            pilot_dict['state'] = False
            temp = np.random.binomial(n=1, p=0.5, size=(N, ))
        # temp = np.random.binomial(n=1, p=0.5, size=(N, ))
        # temp = np.random.randint(0, 1+1, N) #Generate random binary message data for the users

        temp = np.repeat( temp , repeatbit, axis=0)
        xmod = modem.modulate(temp.astype('int'))
        res[str(_)] = temp.astype('int')
        if _ == 0:
            x = np.sqrt(alpha[_])*xmod
        else:
            x += np.sqrt(alpha[_])*xmod
    res['x']=x
    res['N'] = int(repeatbit*(N/(modem.N)))
    pilot_dict['spc_value'] = x[0] # NOMA use this to estimaxte channel
    pilot_dict['ind'] = np.arange(0,int(repeatbit*N/(modem.N)), (Block_size//pilot_per_block)) #  np.where(SPC['x']==SPC['x'][0]) 
    res['pilot'] = pilot_dict
    # x = np.sqrt(a1)*xmod1 + np.sqrt(a2)*xmod2 + np.sqrt(a3)*xmod3
    return res

def ray( fftSize,numBlocks,fs,kM):
    fd = fs/10
    numSamples=fftSize*numBlocks; # total number of samples
    fM=fd/fs       #n ormalized doppler shift
    NfM=fftSize*fM
    kM=np.floor(NfM) # maximum freq of doppler filter in FFT samples
    doppFilter = np.hstack([0,
        1/np.sqrt(2*np.sqrt(1-( ((np.arange(1,kM-1+1,1))/NfM)**2 ))),
        np.sqrt((kM/2)*((np.pi/2)-np.arctan((kM-1)/np.sqrt(2*kM-1)))),
        np.zeros(int(fftSize-2*kM-1)),
        np.sqrt( (kM/2)*((np.pi/2)-np.arctan((kM-1)/np.sqrt(2*kM-1))) ),
        1/np.sqrt(2*np.sqrt(1-((( np.arange(kM-1,1-1,-1) )/NfM)**2)))
        ]).T
    sigmaG=np.sqrt((2*2/(fftSize**2))*np.sum(doppFilter**2))
    gSamplesI=np.random.randn(numSamples,2)  # i.i.d gaussian input samples (in phase)
    gSamplesQ=np.random.randn(numSamples,2)  # i.i.d gaussian input samples (quadrature phase)
    gSamplesI=(1/sigmaG)*(gSamplesI[:,0]+1j*gSamplesI[:,1])
    gSamplesQ=(1/sigmaG)*(gSamplesQ[:,0]+1j*gSamplesQ[:,1])
    # filtering
    filterSamples= np.kron(np.ones(numBlocks),doppFilter)
    filterSamples[np.where(filterSamples==0)]=1
    gSamplesI = gSamplesI*filterSamples
    gSamplesQ = gSamplesQ*filterSamples
    freqSignal = gSamplesI-1j*gSamplesQ # conj()
    freqSignal = freqSignal.reshape( fftSize,numBlocks ) 
    # outSignal = ifft(freqSignal,fftSize) #python is different from matlab
    # outSignal = fft[0]+ [2*fft[1:fftSize/2]] 
    outSignal = np.fft.ifft(freqSignal,fftSize).flatten(order='C')*2
    outSignal = outSignal[np.arange(int(fftSize*numBlocks))]
    return outSignal

def hoyt( fftSize,numBlocks,fs,kM,q=0.5):
    # q=0.5;
    sfq = (1+q**2)/(2*q**2)
    fd = fs/10
    numSamples=fftSize*numBlocks; # total number of samples
    fM=fd/fs       #n ormalized doppler shift
    NfM=fftSize*fM
    kM=np.floor(NfM) # maximum freq of doppler filter in FFT samples
    doppFilter = np.hstack([0,
        1/np.sqrt(2*np.sqrt(1-( ((np.arange(1,kM-1+1,1))/NfM)**2 ))),
        np.sqrt((kM/2)*((np.pi/2)-np.arctan((kM-1)/np.sqrt(2*kM-1)))),
        np.zeros(int(fftSize-2*kM-1)),
        np.sqrt( (kM/2)*((np.pi/2)-np.arctan((kM-1)/np.sqrt(2*kM-1))) ),
        1/np.sqrt(2*np.sqrt(1-((( np.arange(kM-1,1-1,-1) )/NfM)**2)))
        ]).T
    sigmaG=np.sqrt((2*2/(fftSize**2))*sum(doppFilter**2))
    # sigmaG=sqrt((2*2/(fftSize.^2))*sum(doppFilter.^2));
    gSamplesI=np.random.randn(numSamples,2)  # i.i.d gaussian input samples (in phase)
    gSamplesQ=np.random.randn(numSamples,2)  # i.i.d gaussian input samples (quadrature phase)
    gSamplesI=(1/sigmaG)*(gSamplesI[:,0]+1j*gSamplesI[:,1])
    gSamplesQ=(1/sigmaG)*(gSamplesQ[:,0]+1j*gSamplesQ[:,1])
    # filtering
    # filterSamples= kron(ones(numBlocks),doppFilter)
    filterSamples= np.kron(np.ones(numBlocks),doppFilter)
    filterSamples[np.where(filterSamples==0)]=1
    gSamplesI = gSamplesI*filterSamples
    gSamplesQ = q*gSamplesQ*filterSamples
    freqSignal = gSamplesI-1j*gSamplesQ
    freqSignal = freqSignal.reshape( fftSize,numBlocks )
    # outSignal = ifft(freqSignal,fftSize) #python is different from matlab
    # outSignal = fft[0]+ [2*fft[1:fftSize/2]] 
    outSignal = np.fft.ifft(freqSignal,fftSize).flatten(order='C')*2
    outSignal = outSignal[np.arange(int(fftSize*numBlocks))]
    return outSignal

def Jakes(center_freq=200e6,Fs=1e5,ranges=100000,v_mph = 60,plott=False,N = 100,fres=1):
    # Simulation Params, feel free to tweak these
    # v_mph = 60 # velocity of either TX or RX, in miles per hour
    # center_freq = 200e6 # RF carrier frequency in Hz
    # Fs = 1e5 # sample rate of simulation
    # N = 100 # number of sinusoids to sum
    v = v_mph * 0.44704 # convert to m/s
    fd = v*center_freq/3e8 # max Doppler shift
    print("max Doppler shift:", fd)
    t = np.arange(0, 1, 1/Fs/fres) # time vector. (start, stop, step)
    # fres = 1/len(t) #frequency resolution
    t = t[np.arange(ranges)]

    x = np.zeros(len(t))
    y = np.zeros(len(t))
    for i in range(N):
        alpha = (np.random.rand() - 0.5) * 2 * np.pi
        phi = (np.random.rand() - 0.5) * 2 * np.pi
        x = x + np.random.randn() * np.cos(2 * np.pi * fd * t * np.cos(alpha) + phi)
        y = y + np.random.randn() * np.sin(2 * np.pi * fd * t * np.cos(alpha) + phi)

    # z is the complex coefficient representing channel, you can think of this as a phase shift and magnitude scale
    z = (1/np.sqrt(N)) * (x + 1j*y) # this is what you would actually use when simulating the channel
    z_mag = np.abs(z) # take magnitude for the sake of plotting
    z_mag_dB = 10*np.log10(z_mag) # convert to dB
    if plott==True:
        # Plot fading over time
        plt.plot(t, z_mag_dB)
        plt.plot([0, 1], [0, 0], ':r') # 0 dB
        plt.legend(['Rayleigh Fading', 'No Fading'])
        plt.axis([0, 1, -15, 5])
        plt.xlabel('Time(msecs)')
        plt.ylabel('Envelope(dB)')
        plt.grid(True)
        plt.title('Rayleigh Fading')
        #plt.show()
    print("generation done!")
    return z
    
import torch
import contextlib

class Interp1d(torch.autograd.Function):
    def __call__(self, x, y, xnew, out=None):
        return self.forward(x, y, xnew, out)

    def forward(ctx, x, y, xnew, out=None):
        """
        Linear 1D interpolation on the GPU for Pytorch.
        This function returns interpolated values of a set of 1-D functions at
        the desired query points `xnew`.
        This function is working similarly to Matlab™ or scipy functions with
        the `linear` interpolation mode on, except that it parallelises over
        any number of desired interpolation problems.
        The code will run on GPU if all the tensors provided are on a cuda
        device.
        Parameters
        ----------
        x : (N, ) or (D, N) Pytorch Tensor
            A 1-D or 2-D tensor of real values.
        y : (N,) or (D, N) Pytorch Tensor
            A 1-D or 2-D tensor of real values. The length of `y` along its
            last dimension must be the same as that of `x`
        xnew : (P,) or (D, P) Pytorch Tensor
            A 1-D or 2-D tensor of real values. `xnew` can only be 1-D if
            _both_ `x` and `y` are 1-D. Otherwise, its length along the first
            dimension must be the same as that of whichever `x` and `y` is 2-D.
        out : Pytorch Tensor, same shape as `xnew`
            Tensor for the output. If None: allocated automatically.
        """
        # making the vectors at least 2D
        is_flat = {}
        require_grad = {}
        v = {}
        device = []
        eps = torch.finfo(y.dtype).eps
        for name, vec in {'x': x, 'y': y, 'xnew': xnew}.items():
            assert len(vec.shape) <= 2, 'interp1d: all inputs must be '\
                                        'at most 2-D.'
            if len(vec.shape) == 1:
                v[name] = vec[None, :]
            else:
                v[name] = vec
            is_flat[name] = v[name].shape[0] == 1
            require_grad[name] = vec.requires_grad
            device = list(set(device + [str(vec.device)]))
        assert len(device) == 1, 'All parameters must be on the same device.'
        device = device[0]

        # Checking for the dimensions
        assert (v['x'].shape[1] == v['y'].shape[1]
                and (
                     v['x'].shape[0] == v['y'].shape[0]
                     or v['x'].shape[0] == 1
                     or v['y'].shape[0] == 1
                    )
                ), ("x and y must have the same number of columns, and either "
                    "the same number of row or one of them having only one "
                    "row.")

        reshaped_xnew = False
        if ((v['x'].shape[0] == 1) and (v['y'].shape[0] == 1)
           and (v['xnew'].shape[0] > 1)):
            # if there is only one row for both x and y, there is no need to
            # loop over the rows of xnew because they will all have to face the
            # same interpolation problem. We should just stack them together to
            # call interp1d and put them back in place afterwards.
            original_xnew_shape = v['xnew'].shape
            v['xnew'] = v['xnew'].contiguous().view(1, -1)
            reshaped_xnew = True

        # identify the dimensions of output and check if the one provided is ok
        D = max(v['x'].shape[0], v['xnew'].shape[0])
        shape_ynew = (D, v['xnew'].shape[-1])
        if out is not None:
            if out.numel() != shape_ynew[0]*shape_ynew[1]:
                # The output provided is of incorrect shape.
                # Going for a new one
                out = None
            else:
                ynew = out.reshape(shape_ynew)
        if out is None:
            ynew = torch.zeros(*shape_ynew, device=device)

        # moving everything to the desired device in case it was not there
        # already (not handling the case things do not fit entirely, user will
        # do it if required.)
        for name in v:
            v[name] = v[name].to(device)

        # calling searchsorted on the x values.
        ind = ynew.long()

        # expanding xnew to match the number of rows of x in case only one xnew is
        # provided
        if v['xnew'].shape[0] == 1:
            v['xnew'] = v['xnew'].expand(v['x'].shape[0], -1)

        torch.searchsorted(v['x'].contiguous(),
                           v['xnew'].contiguous(), out=ind)

        # the `-1` is because searchsorted looks for the index where the values
        # must be inserted to preserve order. And we want the index of the
        # preceeding value.
        ind -= 1
        # we clamp the index, because the number of intervals is x.shape-1,
        # and the left neighbour should hence be at most number of intervals
        # -1, i.e. number of columns in x -2
        ind = torch.clamp(ind, 0, v['x'].shape[1] - 1 - 1)

        # helper function to select stuff according to the found indices.
        def sel(name):
            if is_flat[name]:
                return v[name].contiguous().view(-1)[ind]
            return torch.gather(v[name], 1, ind)

        # activating gradient storing for everything now
        enable_grad = False
        saved_inputs = []
        for name in ['x', 'y', 'xnew']:
            if require_grad[name]:
                enable_grad = True
                saved_inputs += [v[name]]
            else:
                saved_inputs += [None, ]
        # assuming x are sorted in the dimension 1, computing the slopes for
        # the segments
        is_flat['slopes'] = is_flat['x']
        # now we have found the indices of the neighbors, we start building the
        # output. Hence, we start also activating gradient tracking
        with torch.enable_grad() if enable_grad else contextlib.suppress():
            v['slopes'] = (
                    (v['y'][:, 1:]-v['y'][:, :-1])
                    /
                    (eps + (v['x'][:, 1:]-v['x'][:, :-1]))
                )

            # now build the linear interpolation
            ynew = sel('y') + sel('slopes')*(
                                    v['xnew'] - sel('x'))

            if reshaped_xnew:
                ynew = ynew.view(original_xnew_shape)

        ctx.save_for_backward(ynew, *saved_inputs)
        return ynew

    @staticmethod
    def backward(ctx, grad_out):
        inputs = ctx.saved_tensors[1:]
        gradients = torch.autograd.grad(
                        ctx.saved_tensors[0],
                        [i for i in inputs if i is not None],
                        grad_out, retain_graph=True)
        result = [None, ] * 5
        pos = 0
        for index in range(len(inputs)):
            if inputs[index] is not None:
                result[index] = gradients[pos]
                pos += 1
        return (*result,)