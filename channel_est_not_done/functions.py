import numpy as np
from numpy.linalg import inv, pinv, det, svd, matrix_rank, norm
import scipy.special as sc
from numpy import exp, abs, angle
def PL_Hata(fc,d,htx,hrx,Etype):
  """Hata Pathloss Model
  ===
  https://www.gaussianwaves.com/2019/03/hata-okumura-model-for-outdoor-propagation/
  PL_Hata(fc=900*10**6,d,htx=70,hrx=1.5,Etype='big')
  Args:
      fc (float): carrier frequency[Hz] [ 150 MHz to 1500 MHz] 
      d (float): between base station and mobile station[m], model user km as unit
      htx (float): height of transmitter[m] [ 30 m to 200 m ]
      hrx (float): height of receiver[m] [1m to 10 m]
      Etype (String): Environment Type('open','suburban','small/medium','big')

  Returns:
      float: Path Loss fo Hata model
  """  
  d = d/1000
  #hrx = np.random.rand()*10
  fc=fc/(10**6) # formating Hz to MHz for model input
  a_hrx = (1.1*np.log10(fc)-0.7)*hrx-(1.56*np.log10(fc)-0.8) # Eq. (1.8) #open rural, sub-urban, small/medium ciity
  
  # Urban
  if Etype =='small/medium':
    a_hrx = 0.8 + (1.1*np.log10(fc)-0.7)*hrx - 1.56*np.log10(fc)
    C = 0
  if Etype =='big':
    C = 3
    if fc>=150 and fc<=200: # valid - 200MHz metropolitian big city
      a_hrx = 8.29*(np.log10(1.54*hrx)**2)-1.1
    if fc>200: # 200MHz metropolitian 
      a_hrx = 3.2*(np.log10(11.75*hrx)**2)-4.92

  if Etype == 'suburban':
    C = -2*(np.log10(fc/28)**2)-5.4
  

  if Etype == 'open': # open rural
    C = -4.78*(np.log10(fc)**2)+18.33*np.log10(fc)-40.98

  A = 69.55+26.16*np.log10(fc) - 13.82*np.log10(htx) - a_hrx
  B = 44.9-6.55*np.log10(htx)
  
  PL = A+B*np.log10(d)+C
  return PL

from typing import Tuple
def gmd(U: np.ndarray,
        S: np.ndarray,
        V_H: np.ndarray,
        tol: float = 0.0) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    """
    Perform the Geometric Mean Decomposition of a matrix A, whose SVD is
    given by `[U, S, V_H] = np.linalg.svd(A)`.
    The Geometric Mean Decomposition (GMD) is described in paper "Joint
    Transceiver Design for MIMO Communications Using Geometric Mean
    Decomposition."
    Parameters
    ----------
    U : np.ndarray
        First matrix obtained from the SVD decomposition of the original
        matrix you want to decompose.
    S : np.ndarray
        Second matrix obtained from the SVD decomposition of the original
        matrix you want to decompose.
    V_H : np.ndarray
       Third matrix obtained from the SVD decomposition of the original
       matrix you want to decompose.
    tol : float
        The tolerance.
    Returns
    -------
    (np.ndarray,np.ndarray,np.ndarray)
        The three matrices `Q`, `R` and `P` such that `A = QRP^H`, `R` is
        an upper triangular matrix and `Q` and `P` are unitary matrices.
    """
    # Note: The code here was adapted from the MATLAB code provided by the
    # original GMD authors in
    # http://www.sal.ufl.edu/yjiang/papers/gmd.m

    # \(\mtA = \mtU \mtS \mtV^H\)
    # \(\mtR = \mtU_r \mtS \mtV_r^H\)
    # \(A = \mtU \mtU_r^H S \mtV_R \mtV\)
    m = U.shape[0]
    n = V_H.shape[0]

    # Initialize R, P and Q
    R = np.zeros([m, n])
    P = V_H.conj().T.copy()
    Q = U.copy()

    # 'd' is a vector with the singular values
    d = np.copy(S)  # We copy here to avoid changing 'S'

    # l = min(m, n)
    # noinspection PyTypeChecker
    p = np.sum(S >= tol).item()  # Number of singular values >= tol

    # If there is no singular value greater than the tolerance, then we
    # throw an exception
    if p < 1:  # pragma: no cover
        raise RuntimeError(
            "This is no singular value greater than the tolerance")

    # If we only have one singular value, that will be our diagonal
    # element
    if p < 2:
        R[0, 0] = d[0]

    z = np.zeros([p - 1])  # Vector
    large = 1  # index of the largest diagonal element
    small = p - 1  # index of the smallest diagonal element
    perm = np.r_[0:p]  # perm (i) = location in d of i-th largest entry
    invperm = np.r_[0:p]  # maps diagonal entries to perm

    # Geometric Mean of the 'p' largest singular values
    sigma_bar = np.prod(S[0:p])**(1. / p)

    for k in range(p - 1):
        flag = 0

        # xxxxx If flag is changed to 1 here we will not rotate xxxxxxx
        if d[k] >= sigma_bar:
            i = perm[small]
            small -= 1
            if d[i] >= sigma_bar:
                flag = 1
        else:
            i = perm[large]
            large += 1
            if d[i] <= sigma_bar:
                flag = 1
        # xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        k1 = k + 1
        if i != k1:  # Apply permutation Pi of paper
            t = d[k1]  # Interchange d[i] and d[k1]
            d[k1] = d[i]
            d[i] = t

            j = invperm[k1]  # Update perm arrays
            perm[j] = i
            invperm[i] = j

            # Interchange columns i and k+1 of the Q and P matrices
            I = np.array([k1, i])
            J = np.array([i, k1])
            Q[:, I] = Q[:, J]
            P[:, I] = P[:, J]

        # Deltas
        delta1 = d[k]
        delta2 = d[k1]
        sq_delta1 = delta1**2
        sq_delta2 = delta2**2
        if flag:
            c = 1.0
            s = 0.0
        else:
            c = np.sqrt((sigma_bar**2 - sq_delta2) / (sq_delta1 - sq_delta2))
            s = np.sqrt(1 - c**2)

        d[k1] = delta1 * delta2 / sigma_bar  # = y in paper
        z[k] = s * c * (sq_delta2 - sq_delta1) / sigma_bar  # = x in paper
        R[k, k] = sigma_bar

        if k > 0:
            R[0:k, k] = z[0:k] * c  # new column of R
            z[0:k] = -z[0:k] * s  # new column of Z

        # First Givens Rotation matrix
        G1 = np.array([[c, -s], [s, c]])

        J = np.array([k, k1])
        P[:, J] = P[:, J].dot(G1)  # apply G1 to P

        # Second Givens Rotation Matrix
        G2 = (1. / sigma_bar) * np.array([[c * delta1, -s * delta2],
                                          [s * delta2, c * delta1]])

        Q[:, J] = Q[:, J].dot(G2)  # apply G2 to Q

    R[p - 1, p - 1] = sigma_bar
    R[0:p - 1, p - 1] = z

    return Q, R, P
    
def downsample(s, n, phase=0):
    """Decrease sampling rate by integer factor n with included offset phase.
    """
    return s[phase::n]
  
def pol2cart(r,theta):
    return r * exp( 1j * theta )

def cart2pol(z):
    return ( abs(z), angle(z) )

def pow2db(x):
    """returns the corresponding decibel (dB) value for a power value x.

    The relationship between power and decibels is:

    .. math::    X_{dB} = 10 * \log_{10}(x)

    .. doctest::

        >>> from spectrum import pow2db
        >>> x = pow2db(0.1)
        >>> x
        -10.0
    """
    return 10 * np.log10(x)

def db2pow(xdb):
    """Convert decibels (dB) to power

    .. doctest::

        >>> from spectrum import db2pow
        >>> p = db2pow(-10)
        >>> p
        0.1

    .. seealso:: :func:`pow2db`
    """
    return 10.**(xdb/10.)

def expintn_noMaple(x,n): # Nt>11 && K*Nr>3  ZF analytic failed some value get NaN float/0 --> 'error'
    if n>=2:
        sums = 0
        for m in np.arange(0,(n-2)+1,1):
            if m ==[]: m =0
            sums = sums + (-1)**m * np.math.factorial(m) / x**(m+1)
            pass
        minuend = x**(n-1) * (-1)**(n+1) / np.math.factorial(n-1) * sc.exp1(x) # exp1 for matlab expint
        subtractor = x**(n-1) * (-1)**(n+1) / np.math.factorial(n-1) * np.exp(-x) * sums
        r = minuend - subtractor
    else:
        r = sc.exp1(x)
        pass
    return r

def CalNOMA_SC(Incov, H, W, NsUE='None'): # single carrier NOMA
    # Incov with { 'Rs','Ns', 'K'}
    # Wp (Nt, K*Ns) Tx precoder
    # Wc (K*Ns, Nr) Rx Combiner
    Rs = np.diag(Incov['Rs']) # vectorize covarance matrix
    if NsUE=='None':
        NsUE = 1*np.ones(np.shape(W)[1]) # Ns=1, single stream per user
        NsUE = Incov['Ns']*np.ones(Incov['K']) # Ns=1, single stream per user
        pass
    K =  len(NsUE)# of users
    Nr = int( np.shape(H)[0]/K ) # antenna per user, assuming equal
    Ns = int( sum(NsUE) ) # total data streams K*Ns
    rate = 0
    for iK in np.arange(0,K,1): # 0:K-1, K=quant
        st = int( sum(NsUE[0:(iK+1-1)])+1 ) -1  # sum(NsUE[1:(iK-1)])+1
        ed = int( sum(NsUE[0:iK+1]) ) -1   # sum(NsUE[1:iK])
        sted = np.arange(st,ed+1,1) # sted = st:ed
        
        ue_range = np.arange((Nr*((iK+1)-1)),(Nr*(iK+1)+1)-1,1) #(Nr*(ik-1)+1):(Nr*ik) np.arange(Nr*(iK-1)+1,(Nr*iK)+1,1)
        Hj = H[ ue_range,:] # user channel of i
        Wj = W[:, sted] # precoder for user i
        covj = np.diag(Rs[sted])
        Pj = (Hj@Wj)@covj@( (Hj@Wj).T.conj() ) # signal power term 1x1

        Wintf = W # precoder for interferers instead of user ith
        covtp = Rs
        Wintf = np.delete(Wintf, sted, axis=1) # Wintf[:, sted] =[] 
        covtp = np.delete(covtp, sted) # covtp[sted] = []
        cov_intf = np.diag(covtp)
        Pintf = (Hj@Wintf)@cov_intf@( (Hj@Wintf).T.conj() ) # interference power term 1x1
        rate = rate + np.log2(det( np.eye(Nr) + inv( np.eye(Nr) + Pintf ) @ Pj ))
    return rate


def CalRate(Incov, H, W, NsUE='None'):
    # Incov with { 'Rs','Ns', 'K', 'SNR'}
    # Wp (Nt, K*Ns) Tx precoder
    # Wc (K*Ns, Nr) Rx Combiner
    Rs = np.diag(Incov['Rs']) # vectorize covarance matrix
    if NsUE=='None':
        # NsUE = np.ones(np.shape(W)[1]) # Ns=1, single stream per user
        NsUE = Incov['Ns']*np.ones(Incov['K']) # Ns=1, single stream per user
        pass
    K =  len(NsUE)# of users
    Nr = int( np.shape(H)[0]/K ) # antenna per user, assuming equal
    Ns = int( sum(NsUE) ) # total data streams K*Ns
    rate = 0
    for iK in np.arange(0,K,1): # 0:K-1, K=quant
        st = int( sum(NsUE[0:(iK+1-1)])+1 ) -1  # sum(NsUE[1:(iK-1)])+1
        ed = int( sum(NsUE[0:iK+1]) ) -1   # sum(NsUE[1:iK])
        sted = np.arange(st,ed+1,1) # sted = st:ed
        
        ue_range = np.arange((Nr*((iK+1)-1)),(Nr*(iK+1)+1)-1,1) #(Nr*(ik-1)+1):(Nr*ik) np.arange(Nr*(iK-1)+1,(Nr*iK)+1,1)
        Hj = H[ ue_range,:] # user channel of i
        Wj = W[:, sted] # precoder for user i
        covj = np.diag(Rs[sted])
        Pj = (Hj@Wj)@covj@( (Hj@Wj).T.conj() ) # signal power term 1x1

        Wintf = W # precoder for interferers instead of user ith
        covtp = Rs
        Wintf = np.delete(Wintf, sted, axis=1) # Wintf[:, sted] =[] 
        covtp = np.delete(covtp, sted) # covtp[sted] = []
        cov_intf = np.diag(covtp)
        Pintf = (Hj@Wintf)@cov_intf@( (Hj@Wintf).T.conj() ) # interference power term 1x1
        Pnoise =  awgn(Pj,Incov['SNR'])
        rate = rate + np.log2(det( np.eye(Nr) + inv( np.eye(Nr) + Pintf ) @ Pj ))
    return rate

def CalRateM(Incov, H, Wp, Wc, NsUE='None'):
    # Incov with { 'Rs', 'Ns', 'K', 'SNR' }
    # Wp (Nt, K*Ns) Tx precoder
    # Wc (K*Ns, Nr) Rx Combiner
    Rs = np.diag(Incov['Rs']) # vectorize covarance matrix
    if NsUE=='None':
        NsUE = np.ones(np.shape(Wp)[1]) # Ns=1, single stream per user
        NsUE = Incov['Ns']*np.ones(Incov['K']) # Ns=1, single stream per user
        pass
    K =  len(NsUE) # of users
    Nr = int(np.shape(H)[0]/K ) # antenna per user, assuming equal
    Ns = int( sum(NsUE) ) # K*Ns total data streams
    rate = 0

    iK = 0
    for iK in np.arange(0,K,1): # 0:K-1:
        st = int( sum(NsUE[0:(iK+1-1)])+1 ) -1  # sum(NsUE[1:(iK-1)])+1
        ed = int( sum(NsUE[0:iK+1]) ) -1   # sum(NsUE[1:iK])
        sted = np.arange(st,ed+1,1) # sted = st:ed

        Cj = Wc[sted,:]

        ue_range = np.arange((Nr*((iK+1)-1)),(Nr*(iK+1)+1)-1,1) #(Nr*(ik-1)+1):(Nr*ik)
        Hj = H[ ue_range,:] # user channel of i
        Wj = Wp[:, sted] # precoder for user i
        Rj = np.diag(Rs[sted])
        Pj = (Cj@Hj@Wj)@ Rj @ ( (Cj@Hj@Wj).T.conj() )
        # Pj = (Hj@Wj)@ Rj @( (Hj@Wj).T.conj() ) # signal power term 1x1

        Wintf = Wp # precoder for interferers instead of user ith
        Wintf = np.delete(Wintf, sted, axis=1) # Wintf[:, sted] =[] 
        R_intf = Rs
        R_intf = np.delete(R_intf, sted) # covtp[sted] = []
        R_intf = np.diag(R_intf)
        Pintf =  Cj@  ( np.eye(Nr) + (Hj@Wintf)@ R_intf @ ( (Hj@Wintf).T.conj() ))  @ Cj.T.conj()
        Pnoise = awgn(Pj,Incov['SNR'])
        # Pintf = (Hj@Wintf)@R_intf@( (Hj@Wintf).T.conj() ) # interference power term 1x1
        rate = rate + np.log2(det(np.eye(int(NsUE[iK])) + pinv(Pintf)*Pj))
    return rate

def CalBDPrecoder(Incov,H,s='svd'): # no waterfilling
    """[summary]

    Args:
        Incov (dict): with {'K','Nr','Ns'}
        H (ndarray): MU-MIMO (K*Nr, Nt) array
        s (str, optional): [description]. Defaults to 'svd'.

    Returns:
        [type]: [description]
    """
    # Incov with {'K','Nr','Ns'}
    NR, Nt = np.shape(H)
    Ns = int(Incov['Ns'])
    K = int(Incov['K']) # of users
    Nr = int(NR/K) # antenna per user, assuming equal
    NsUE = Ns * np.ones(K) # default Ns stream per user
    # Fa = []; Ca = []
    F = np.zeros((Nt,K*Ns),dtype='cfloat') # (Nt, K*Ns) Tx precoder
    C = np.zeros((K*Ns,Nr),dtype='cfloat') # (K*Ns, Nr) Rx Combiner
    #***** Standard BD design based on paper "ZF methods for DL spatial multiplexing..." ***#
    for iK in np.arange(0,K,1): # 0:K-1, K=quant
        # ue_range ((iK-1)*Nr+1):(iK*Nr) # iK=0~ Nr=1~, iK+100
        ue_range = np.arange(Nr*(iK)+1-1,(iK+1)*Nr+1-1,1)
        Gj = H[ ue_range, :] # H of ith user select range
        G_tilde = H
        G_tilde = np.delete(G_tilde,np.s_[ue_range],axis=0) # Delete iK-user's channel
        U, S, VH = svd(G_tilde) # numpy svd is different from matlab
        V  = VH.T.conj()
        rv = matrix_rank(G_tilde)

        Htmp = Gj@V[:, np.arange(rv,Nt,1) ] # V(:, (rv + 1):Nt )   
        Lr = matrix_rank(Htmp) # max stream per UE
        Ut, St, VHt = svd(Htmp)
        Vt  = VHt.T.conj()
        # F = [F V(:, (rv + 1):Nt)*Vt(: , 1:Ns)]  #np.concatenate(F,Ftmp) # F = np.hstack([F,Ftmp])
        Ftmp = V[ : , np.arange(rv,Nt,1)]@Vt[: , np.arange(int(NsUE[iK]))]
        # C = [C;  Ut(:, 1:Ns)']
        Ctmp = Ut[:, np.arange(int(NsUE[iK]) ) ].T.conj()
        # if Fa ==[]: Fa = Ftmp; Ca = Ctmp # only single Nr antenna works
        # else: Fa = np.hstack([Fa,Ftmp]); Ca = np.vstack([Ca,Ctmp])
        k_range = np.arange((iK)*Ns,(iK+1)*Ns,1) # (iK-1)*Ns+1:iK*Ns) 
        F[:, k_range] = Ftmp
        C[ k_range, :] = Ctmp
        pass
    return F,C

def normalize_precoder(F,s='diag', P=1, K=1):
    """Normalized Precoder
    ----
    Args:
        F (ndarray): precoder u generate
        s (str, optional): [description]. Defaults to 'diag'. with single RF chain
        if select 'fro' normalized by frob norm, and input the P, K
        P (int, optional): [description]. Defaults to 1.
        K (int, optional): [description]. Defaults to 1.

    Returns:
        ndarray: F * np.sqrt(P/K)/ norm
    """
    # Paper On the Spectral and Energy Efficiency of
    # Full-Duplex Small Cell Wireless Systems with Massive MIMO
    if s=='diag': # pinv avoid singular error
        return F@pinv(np.sqrt(np.diag(np.diag(F.T.conj()@F))))
    if s=='fro': # Frobenius norm
        return F * np.sqrt(P/K)/ norm(F, 'fro')

def HybridQuant(B,W):
    """Quantize the beamforming phase with bit numbers
    ----
    Args:
        B (int): bit number to quantize
        W (cfloat): beamforming phase weight

    Returns:
        cfloat: Phase quantized with B bits
    """
    delta = 2*np.pi/(2**B); # quantization interval
    r = np.zeros( (np.shape(W)[0], np.shape(W)[1]), dtype='cfloat') # ininitialize quantized matrix
    for i1 in np.arange(np.shape(W)[0]):
        for i2 in np.arange(np.shape(W)[1]):
            ph = np.angle(W[i1, i2]) # ph in [-pi, pi]
            phq = np.floor(ph/delta)*delta +(np.mod(ph, delta) > (delta/2))*delta # quantized phase
            r[i1, i2] = np.exp(1j*phq)
    r = 1/np.sqrt(np.shape(W)[0]) * r
    return r

def threshold_bit(msg):
    """Threshold bit after hard decision [-1,1]
    -----
    Args:
        msg (int ndarray): bits after demodulation

    Returns:
        int ndarray: threhoding msg
    """
    msg[np.where(msg<0)] = 0; msg[np.where(msg>1)] = 1 # thresolding
    return msg

## Combining method for SIMO
def MRC(_y, H):
    '''Max Ratio Combining
    https://www.gaussianwaves.com/2020/01/receiver-diversity-maximum-ratio-combining-mrc/
    ---
    maxRatioCombine() is used when the receiver uses the information about the
    channel. Goal is to maximize SNR.
    Input: (Nr x N)
    Output: (1 x N)
    '''
    return(((H.T.conj())*_y)/(np.linalg.norm(H))**2)

def ERC(_y,H):
    '''Equal Ratio Combining
    ---
    equalRatioCombine() is used when the receiver uses no information about the
    channel. Goal is faster processing.
    Input: (Nr x N)
    Output: (1 x N)
    '''
    if np.array(H).ndim !=1:
        H = np.sum(H,1) # reduce to Nr x 1
    return np.sum(_y,0)/np.sum(H)

def SRC(_y, H):
    '''Select Ratio Combining
    ----
    selectRatioCombine() is used when the receiver uses the channel with highest
    strength. Goal is faster processing.
    Input: (Nr x N)
    Output: (1 x N)
    '''
    if np.array(H).ndim !=1:
        H = np.sum(H,1) # reduce to Nr x 1
    idx = np.argmax(H)
    return _y[idx,:]/H[idx]

def NOMA_SPC(N, alpha, modem):
    """NOMA superposition coding
    ---
    .. doctest::
        >>> SPC = NOMA_SPC(N, alpha, modem)
        >>> alpha=[0.8,0.15,0.05]
        >>> SPC = NOMA_SPC(N, alpha, modem)
        >>> x = SPC['x'];
    Args:
        N (int): num of sample
        alpha (ndarray): noma coef
        modem (Obj): modem of ModulationPY

    Returns:
        dict: {x, N, '0', '1'....}
        x: SPC signal
        N: samples num after modulation
        '0','1'..., ndarray: label of msg sent
    """
    res = {}
    for _ in np.arange(len(alpha)): # ue amout
        temp = np.random.binomial(n=1, p=0.5, size=(N, ))
        # temp = np.random.randint(0, 1+1, N) #Generate random binary message data for the users
        xmod = modem.modulate(temp)
        res[str(_)] = temp
        if _==0:
            x = np.sqrt(alpha[_])*xmod
        else:
            x += np.sqrt(alpha[_])*xmod
    res['x']=x
    # nSample= int(N/(2**0)) # bpsk
    # nSample= int(N/(2**1)) # qpsk/4QAM
    res['N'] = int(N/(2**(np.log2(modem.M)-1)))
    # x = np.sqrt(a1)*xmod1 + np.sqrt(a2)*xmod2 + np.sqrt(a3)*xmod3
    return res

def NOMA_channel(x, h, sigma2,BW=1e6, themo_noise=-174):
    """Channel Diversity uncoded

    Args:
        x (complex ndarray): SPC signal Nx1
        h (ndarray): Channel for UE [h1,h2,h3] array or list each h is channel matrix (must same dim)
        sigma2 ([type]): signal variance
        noise (str, optional): [description]. Defaults to 'default'.

    Returns:
        ndarray: UE x Nr x N, UE x PATH x Nr x N (MIMO)
    """
    # https://www.gaussianwaves.com/2014/08/characterizing-a-mimo-channel
    # np.tile([1,2,3], (4, 1)) # repeat dim
    # x = np.tile(x, (h.shape[1], 1)) # brocast as NtxN
    #BW = 20*10**6   #Bandwidth = 1 MHz
    No = themo_noise + 10*np.log10(BW)+7	# Noise power (dBm)
    no = (10**(-3))*10**(No/10) # db2pow(No) #Noise power (linear scale) 
    N = np.shape(x)[0]
    if h[0].ndim==1:
        y = [ np.sqrt(sigma2)*x*hi + np.sqrt(no)*(np.random.randn(N)+1j*np.random.randn(N))/np.sqrt(2) for hi in h ]
        # y = [ awgn(np.sqrt(sigma2)*x*hi, 10 * np.log10(sigma2*1000))  for hi in h ]
    else: # MIMO channel with uncoded select all path(antennas) in time slot
        # x = np.tile(x, (h[0].shape[1], 1)) # h[0] Nt x 1, X brocast as NtxN, 
        # y = np.array([[ hi[_]@x for _ in np.arange(np.shape(hi)[0]) ] for hi in h ])
        # [ h[_]@x for _ in np.arange(np.shape(h)[0]) ] #input h, Nr x Nt
        y = np.array( [ [ x[idx]*ichannel+np.sqrt(no)*(np.random.randn()+1j*np.random.randn())/np.sqrt(2) for ichannel in hi] for idx, hi in enumerate(h) ])# y1 = np.sqrt(sigma2)*x*h1 + n1	#At user 1 ...etc
    return y


def NOMA_SIC(r, h, sigma2, alpha, modem, noise='awgn'):
    """NOMA SIC function
    ---
    Args:
        r (ndarray): Nrx1 receive signal
        h (ndarray): 1d combined channel h
        sigma2 (float): noise variance (10**(-3))*10**(SNR_db/10) mW, sigma2=2*Nt/Nr*10^(-SNRdb/10)
        alpha (ndarray): noma coef
        modem (Obj): modem of ModulationPY
        noise (str, optional): [description]. Defaults to 'awgn'.

    Returns:
        dict: SIC and decode dict[str(1)]
    """
    modem.soft_decision = False
    # r = r/h # equalize
    # r = awgn(r, 10 * np.log10(sigma2*1000))
    dec = {}
    dec['rem'+str(0)] = r
    for n in np.arange(len(alpha)): # sic
        dec[str(n)] = threshold_bit( modem.demodulate(r).astype(int) )
        r = r - np.sqrt(alpha[n]*sigma2) * modem.modulate(dec[str(n)])
        dec['rem'+str(n+1)] = r
    return dec

def MMSE_SIC(r,H,sigma2,sigmas2,N,modem):
    """MMSE_SIC detector in MIMO
    ---
    Args:
        r (ndarray): receive signal y
        H (ndarray): channel matrix NrxNt
        sigma2 (float): noise variance sigma2=2*Nt/Nr*10^(-SNRdb/10)
        sigmas2 (flaot): signal variance ex: BPSK sigmas2=2
        N (int): [description]
        modem (Obj): modem of ModulationPY

    Returns:
        ndarray: MMSE-SIC detection bits
    """
    modOrd = modem.M
    modem.soft_decision = False
    # Create PSK modulator and demodulator System objects
    estMMSE = np.zeros(N*modOrd)
    orderVec = np.arange(1,N+1,1)
    k = N
    # Start MMSE nulling loop
    for n in np.arange(N):
        # Shrink H to remove the effect of the last decoded symbol
        # H = H[:, [1:k-1,k+1:end]]
        row,col = np.shape(H)
        select_range = np.ma.concatenate([ np.arange(0,(k+1)-1,1), np.arange((k+1),col,1)] )
        print(col, k)
        print(select_range)
        H = H[:, select_range ]
        # Shrink order vector correspondingly
        orderVec = orderVec[select_range] # orderVec(1, [1:k-1,k+1:end])
        # Select the next symbol to be decoded 
        # Same as inv(H.T.conj()*H + Sigma_n/Es *I ), but faster (H'*H + ((N-n+1)*sigma2/sigmas2)*eye(N-n+1)) \ eye(N-n+1) 
        G = inv(H.T.conj()@H + ((N-(n+1)+1)*sigma2/sigmas2)*np.eye(N-(n+1)+1))
        # [~,k] = min(np.diag(G))
        k = np.argmin(np.diag(np.real(G)), axis=0) # select smallest diagonal element in the covariance matrix
        symNum = orderVec[k]
        decBits = modem.demodulate([ G[k,:] @ H.T.conj() @ r ])
        estMMSE[2 * (symNum-1) + (np.arange(modOrd))] = decBits
        if n < N:
            r = r - H[:, k] * modem.modulate( threshold_bit( decBits.astype('int') ) )
    return estMMSE


# # rayleigh flat fading
# h= (np.random.randn(1,n) + 1j*np.random.randn(1,n)/np.sqrt(2)

# # rician input K factor
# hi = np.sqrt(K/(K+1))+np.sqrt(1/(2*(K+1)))*np.random.randn(1,n)
# hq = np.sqrt(1/(2*(K+1)))*np.random.randn(1,n)
# h = hi+hq # LOS

# # Nakagami-m Hoyt input q factor
# h = (1/(np.sqrt(1+q**2)))*( np.random.randn(1,n)+1j*np.random.randn(1,n) )

from numpy import sum,isrealobj,sqrt
from numpy.random import standard_normal
def awgn(s,SNRdB,L=1):
    """
    AWGN channel
    Add AWGN noise to input signal. The function adds AWGN noise vector to signal 's' to generate a resulting signal vector 'r' of specified SNR in dB. It also
    returns the noise vector 'n' that is added to the signal 's' and the power spectral density N0 of noise added
    Parameters:
        s : input/transmitted signal vector
        SNRdB : desired signal to noise ratio (expressed in dB) for the received signal
        L : oversampling factor (applicable for waveform simulation) default L = 1.
    Returns:
        r : received signal vector (r=s+n)
"""
    gamma = 10**(SNRdB/10) #SNR to linear scale
    if s.ndim==1:# if s is single dimensional vector
        P=L*sum(abs(s)**2)/len(s) #Actual power in the vector
    else: # multi-dimensional signals like MFSK
        P=L*sum(sum(abs(s)**2))/len(s) # if s is a matrix [MxN]
    N0=P/gamma # Find the noise spectral density
    if isrealobj(s):# check if input is real/complex object type
        n = sqrt(N0/2)*standard_normal(s.shape) # computed noise
    else:
        n = sqrt(N0/2)*(standard_normal(s.shape)+1j*standard_normal(s.shape))
    r = s + n # received signal
    return r

def awgn_measure(x, snr): # awgn channel with equalize
    """Add awgn noise SISO channel and  equalizing
    ----
    Args:
        x (ndarray): signal after modulation
        snr (float or int): SNR db

    Returns:
        ndarray: signal with noise and equalizing by y/h
    """
    n = len(x)
    y = np.zeros(n)+1j*np.zeros(n)
    h= np.random.randn(1,n) + 1j*np.random.randn(1,n)
    noise = np.random.randn(1,n) + 1j*np.random.randn(1,n)
    n_var = 10**(-snr/20) # power linear scale
    y=(h*x)+(noise)*np.sqrt(n_var)
    y=y/h
    return np.squeeze(y)


def awgnn(y,snr): # add awgn noise for receiver
    """Add awgn noise for receiver
    ---
    Args:
        y (1d ndarray): without noise
        snr (float or int): SNR db

    Returns:
        ndarray: signal with noise
    """
    n = len(y)
    n_var = 10**(-snr/20)
    # AWGN
    noise = np.random.randn(n) + 1j*np.random.randn(n)
    # # rayleigh
    # noise = (np.random.randn(n) + 1j*np.random.randn(n))/np.sqrt(2)
    
    # # rician K
    # K=3
    # ni = np.sqrt(K/(K+1))+np.sqrt(1/(2*(K+1)))*np.random.randn(n)
    # nq = np.sqrt(1/(2*(K+1)))*np.random.randn(n)
    # noise = ni+nq # LOS

    # # # Nakagami-m Hoyt input q factor
    # q=3
    # noise = (1/(np.sqrt(1+q**2)))*( np.random.randn(n)+1j*np.random.randn(n) )
    y = y + (noise)*np.sqrt(n_var)
    return np.squeeze(y)

# convert to plotly fig
from plotly.tools import mpl_to_plotly
def fig2html(fig,filename): # input matplotlib fig
    """convert 2d plot to plotly and save html 
    ---
    plotly==4.8.2

    Args:
        fig (matplotlib figure): [description]
        filename (str): [description]
    """
    plotly_fig = mpl_to_plotly(fig)
    plotly_fig.update_layout(template="none",showlegend=True,
        title_font_family='simsun',#"Times New Roman",
        font_family='simsun',#"Courier New",
        title_font_size=18, #title_font_color="red",
        font_size=14, #font_color="blue",
        # legend_font_size=15, legend_title_font_color="green",
        # hoverlabel_font_size=16, hoverlabel_font_color="blue",
        # hovermode="x unified",
        legend=dict(
            # yanchor="top",
            # y=0.99,
            yanchor="bottom",
            y=0.01,
            xanchor="left",
            x=0.01,
            # xanchor="right",
            # x=0.99,
            bgcolor = 'rgba(255,255,255,0.8)' # color rgb, and opacity
            ),
        xaxis={
            'title_font_size': 16,
            'tickfont_size': 12},
        yaxis={
            'title_font_size': 16,
            'tickfont_size': 12,
            # 'type': 'linear',
            'exponentformat': 'power',
            'showexponent': 'all'},
        )
    # type ( "-" | "linear" | "log" | "date" | "category" | "multicategory" ) 
    # exponentformat ( "none" | "e" | "E" | "power" | "SI" | "B" ) 
    plotly_fig.write_html(filename) # include_mathjax='cdn'
    # return plotly_fig.to_html()
