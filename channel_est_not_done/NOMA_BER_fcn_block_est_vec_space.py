import numpy as np
import matplotlib.pyplot as plt
from ModulationPy import PSKModem, QAMModem
from fun_block import NOMA_SPC,Interp1d,ray # block type allocation
from functions import NOMA_SIC,NOMA_channel,awgn,threshold_bit,PL_Hata,downsample
import scipy.linalg as la
import scipy.interpolate
import torch
from PL3gpp38901 import PL_RMa,PL_UMa,PL_UMi,PL_InH # load pathloss
import matplotlib
from scipy import signal

plt.rcParams['axes.unicode_minus'] = False # 負號問題
plt.rcParams['mathtext.fontset'] = 'custom' 
plt.rcParams['mathtext.it'] = 'STIXGeneral:italic' 
plt.rcParams['mathtext.bf'] = 'STIXGeneral:italic:bold' 
# matplotlib.rc('font', family='mingliu') # , weight='bold') # 設定新細明體
matplotlib.rc('font', family='PingFang HK') # , weight='bold') # 設定蘋方字體

# from matplotlib.font_manager import FontManager
# fm = FontManager()
# mat_fonts = set(f.name for f in fm.ttflist)
# print (mat_fonts)

Pt = np.arange(0,40+1,2) #Transmit power (dBm) 0:2:40
# Pt = np.arange(-40,43+1,5) #Transmit power (dBm) 0:2:40
pt = (10**(-3))*10**(Pt/10)  #db2pow(Pt) #Transmit power (linear scale)

BW = 10**6   #Bandwidth = 1 MHz
No = -174 + 10*np.log10(BW)	+7 #Noise power (dBm)
no = (10**(-3))*10**(No/10) #db2pow(No) #Noise power (linear scale) 
d1 = 500; d2 = 200; d3 = 70	#Distances 
a1 = 0.8; a2 = 0.15; a3 = 0.05	#Power allocation coefficients

#Create QPSKModulator and QPSKDemodulator objects
# modem = PSKModem(2）
modem = PSKModem(4) 
modem = QAMModem(4)
# modem.plot_const() # plot constellation
modem.soft_decision = False
# modem.demodulate( modem.modulate([0,1,1,1])).astype(int) # hard decision

N = 10**5 # for plot historgram need more samples
fftSize = 64
Blocks = 20
pilots_per = 8
N = 2*fftSize*Blocks
# nSample= int(N/(modem.N)) # bpsk
nSample= int(N/(modem.N))

hh1 = (np.random.randn(nSample) + 1j*np.random.randn(nSample))/np.sqrt(2)
hh2 = (np.random.randn(nSample) + 1j*np.random.randn(nSample))/np.sqrt(2)
hh3 = (np.random.randn(nSample) + 1j*np.random.randn(nSample))/np.sqrt(2)

# Fs = 983.04*int(10**6) # 2 BW
# Fs = 30.72*10**6
Fs = 2*BW
t = np.arange(0, 1, 1/Fs) # time vector. (start, stop, step)
z = ray(fftSize,Blocks,Fs,Fs/10) # ray(fftSize,Blocks,Fs,Fs/10)
# z = ray(512,20,30.72*10**6,30.72*10**5) # ray(fftSize,Blocks,Fs,Fs/10)
z = signal.resample(z,200*len(z))
plt.figure();plt.plot(20*np.log10(abs(z)),label='Channel');plt.legend()
plt.xlabel('Index')
plt.ylabel('Envelope(dB)')
plt.grid(True)
# MSK bps=log2(M)/2
# 位元速率 = 鮑率 x 一個訊號的位元數量 = 1000 x 4 = 4000 bps 
z = z[np.arange(nSample)]
hh1 = z[np.arange(nSample)]; hh2 = z[np.arange(nSample)]; hh3 = z[np.arange(nSample)]

#Do super position coding

alpha=[0.8,0.15,0.05]
SPC = NOMA_SPC(N, alpha, modem, Block_size = fftSize,pilot_per_block=pilots_per)
x = SPC['x'] # frequency domain signal

ber1 = [0]*len(Pt)
ber2 = [0]*len(Pt)
ber3 = [0]*len(Pt)
comb = np.zeros( (3,nSample),dtype='cfloat' )

for u in np.arange(len(Pt)):
    #  np.sqrt(d1**(-eta)) ~= 0dbm-Ploss
    Antenna_gain=-12
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'big')
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'small/medium')
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'suburban')
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'open')
    # PL_RMa(3.5,d1,70,1.5)
    # PL_UMa(3.5,d1,1.5)
    # PL_UMi(3.5,d1,1.5)
    # PL_InH(3.5,10)
    PL1 = PL_RMa(3.5,d1,70,1.5,condition='NLOS')
    PL2 = PL_RMa(3.5,d2,70,1.5,condition='NLOS')
    PL3 = PL_RMa(3.5,d3,70,1.5,condition='NLOS')
    G1 = np.sqrt((10**((Pt[u]-(PL1-Antenna_gain) )/10))/2)
    G2 = np.sqrt((10**((Pt[u]-(PL2-Antenna_gain) )/10))/2)
    G3 = np.sqrt((10**((Pt[u]-(PL3-Antenna_gain) )/10))/2)
    h1 = G1*hh1
    h2 = G2*hh2
    h3 = G3*hh3
    h = [h1,h2,h3] # SISO channel
    # using complex gaussian vec_space simulate match filter
    # https://www.gaussianwaves.com/2020/08/rician-flat-fading-channel-simulation/
    h = [h1,h2,h3] # SISO channel r = hx+n, mf: r*h.conj(), eq: mf/(h**2), y = h.conj()/(h**2)*r
    y = NOMA_channel(x, h, pt[u], BW,-174) # SISO Rx signal
    d = [d1,d2,d3]
    for _ in range(3):
        # add sfo, simulate cfo 
        # Sampling Frequency (Hz)
        Fs = 15.36e6 
        # Frequency Offset (Hz)
        delta_f = 500
        # y[_] = add_frequency_offset(y[_], Fs, delta_f)

        # Channel Estimation LS block type, 為了減少計算量，假設time domain的信號為。的frequency selective fading訊號
        if SPC['pilot']['state'] == True: # frequency selective situtaion
            # channelResponse = np.hstack([[1, 0],SPC['pilot']['spc_value']])
            pilots = y[_][ SPC['pilot']['ind'] ] #- np.sqrt(no)*((1+1j)/np.sqrt(2))*np.random.randn(len(y[_][ SPC['pilot']['ind'] ])) # get pilot index
            Hest_at_pilots = pilots  / SPC['pilot']['spc_value'] /( np.sqrt(pt[u]) + np.sqrt(no)*(np.random.randn(len(pilots)) + 1j*np.random.randn(len(pilots)))/np.sqrt(2) )  # use divide '/', this is freq domain

            # # Perform interpolation between the pilot carriers to get an estimate
            # # of the channel in the data carriers. Here, we interpolate absolute value and phase 
            # # separately
            # Hest_abs = scipy.interpolate.interp1d(SPC['pilot']['ind'], abs(Hest_at_pilots), kind='linear',fill_value="extrapolate")( np.arange(len(h[_])) ) # np.arange(len(y[_])) all index
            # Hest_abs = scipy.interpolate.interp1d(SPC['pilot']['ind'], abs(Hest_at_pilots), kind='cubic',fill_value="extrapolate")( np.arange(len(h[_])) ) # np.arange(len(y[_])) all index
            # Hest_phase = scipy.interpolate.interp1d( SPC['pilot']['ind'], np.angle(Hest_at_pilots), kind='linear',fill_value="extrapolate")( np.arange(len(h[_])) )
            # Hest_phase = scipy.interpolate.interp1d( SPC['pilot']['ind'], np.angle(Hest_at_pilots), kind='cubic',fill_value="extrapolate")( np.arange(len(h[_])) )
            # Interp1d()(x, y, xnew, yq_cpu)
            Hest_abs = Interp1d()(torch.tensor(SPC['pilot']['ind']), torch.Tensor(abs(Hest_at_pilots)), torch.tensor(np.arange(len(h[_]))) ) # np.arange(len(y[_])) all index
            Hest_phase = Interp1d()( torch.tensor(SPC['pilot']['ind']), torch.Tensor(np.angle(Hest_at_pilots)),torch.tensor(np.arange(len(h[_]))) )
            Hest = Hest_abs * np.exp(1j*Hest_phase)
            Hest = Hest.numpy().squeeze() # pytorch
            # Hest = wt*Hest
            if Pt[u]==40 and _==2:
                plt.figure();plt.plot(t[np.arange(len(Hest))],20*np.log10(abs(Hest)),label='Estimated channel via interpolation'); plt.plot(t[np.arange(len(Hest))],20*np.log10(abs(h[_])),label='Correct Channel',linewidth=5,alpha=0.2); plt.plot(t[ SPC['pilot']['ind'] ], 20*np.log10(abs(Hest_at_pilots)),'.r',label='Pilot estimates',alpha=0.6);plt.legend();
                plt.xlabel('Index')
                plt.ylabel('Envelope(dB)')
                plt.grid(True)
                plt.title('Rayleigh Fading')
                plt.figure();plt.plot(t[np.arange(len(Hest))],np.angle(Hest),label='Estimated channel via interpolation'); plt.plot(t[np.arange(len(Hest))],np.angle(h[_]),label='Correct Channel',linewidth=5,alpha=0.2); plt.plot(t[ SPC['pilot']['ind'] ], np.angle(Hest_at_pilots),'.r',label='Pilot estimates',alpha=0.6);plt.legend();
                plt.xlabel('Index')
                plt.ylabel('Phase angle')
                plt.grid(True)
                plt.title('Rayleigh Fading')
            pass
        # y[_] = awgn(y[_],Pt[u])
        # equalize flat fading model
        # k = y[_]/h[_]
        k = y[_]/Hest
        #vec space
        # k = y[_]* h[_].conj()/abs(h[_]**2)
        k = y[_]* Hest[_].conj()/abs(Hest[_]**2)
        # modulator additive AWGN noise
        # k = awgn(k,Pt[u])
        comb[_] = k # just a variable

    eq1 = comb[0]
    eq2 = comb[1]
    eq3 = comb[2]
    
    dec1 = NOMA_SIC(eq1, h1, pt[u], alpha, modem)
    dec2 = NOMA_SIC(eq2, h2, pt[u], alpha, modem)
    dec3 = NOMA_SIC(eq3, h3, pt[u], alpha, modem)
    ber1[u] = np.sum( dec1['0'] != SPC['0'])/N
    ber2[u] = np.sum( dec2['1'] != SPC['1'])/N
    ber3[u] = np.sum( dec3['2'] != SPC['2'])/N
    rem1 = dec1['rem0']
    rem2 = dec2['rem1']
    rem3 = dec3['rem2']

    # input signal SPC no need h
    decxx = NOMA_SIC(np.sqrt(pt[u])*x, h[0], pt[u], alpha, modem)
    remm1 = decxx['rem0']
    remm2 = decxx['rem1']
    remm3 = decxx['rem2']

    # compute abs avoid np.sqrt get -value less than 0
    # EVM = np.sqrt( ( (I-I')**2+(Q-Q')**2 ) /N )
    # EVM% = np.sqrt( ( (I-I')**2+(Q-Q')**2 ) /N )*100
    # EVM dB = 20*np.log10(EVM) 3% is ~= -30.45757490560675 dB
    EVM1 = np.sqrt( sum(abs( (rem1.real-remm1.real)**2 -(rem1.imag-remm1.imag)**2 )) /nSample )
    EVM2 = np.sqrt( sum(abs( (rem2.real-remm2.real)**2 -(rem2.imag-remm2.imag)**2 )) /nSample )
    EVM3 = np.sqrt( sum(abs( (rem3.real-remm3.real)**2 -(rem3.imag-remm3.imag)**2 )) /nSample )
    EVM1_dB = 20*np.log10(EVM1)
    EVM2_dB = 20*np.log10(EVM2)
    EVM3_dB = 20*np.log10(EVM3)
    
fig2 = plt.figure()
plt.semilogy(Pt, ber1,'-r',marker='o',label='User 1 (Weakest user) %dm'%d1)
plt.semilogy(Pt, ber2,'-g',marker='o',label='User 2 %dm'%d2)
plt.semilogy(Pt, ber3,'-b',marker='o',label='User 3 (Strongest user) %dm'%d3)
plt.title('NOMA multiple Users')
plt.xlabel('Transmit power (dBm)')
plt.ylabel('BER')
plt.legend()
plt.grid(True,which='both')
plt.tight_layout()


fig = plt.figure()
plt.title('Constellation')
plt.plot(np.real( rem1), np.imag( rem1  ), '.r', label = 'Dec1')
plt.plot(np.real( rem2 ), np.imag( rem2  ), '+g', label = 'Dec2')
plt.plot(np.real( rem3 ), np.imag( rem3  ), '1b', label = 'Dec3')
plt.xlabel(r'I')
plt.ylabel(r'Q')
plt.legend(loc='upper right')
plt.grid(True,which='both')
plt.tight_layout()

fig = plt.figure()
plt.title('Constellation SPC')
plt.plot(np.real( rem1), np.imag( rem1  ), 'rx', markersize=8,label = 'Dec1 EVM=%.3f'%(EVM1*100)+'%')
plt.plot(np.real( rem2 ), np.imag( rem2  ), 'g+', markersize=8, label = 'Dec2 EVM=%.3f'%(EVM2*100)+'%')
plt.plot(np.real( rem3 ), np.imag( rem3  ), 'b1', markersize=8, label = 'Dec3 EVM=%.3f'%(EVM3*100)+'%',alpha=0.6)
plt.xlabel(r'I')
plt.ylabel(r'Q')
plt.legend(loc='upper right')
plt.grid(True,which='both')
plt.tight_layout()

plt.show()

# from plotly.tools import mpl_to_plotly

# plotly_fig = mpl_to_plotly(fig)
# plotly_fig.update_layout(template="none")
# plotly_fig.write_html("constellation.html")

# plotly_fig = mpl_to_plotly(fig2)
# plotly_fig.update_layout(template="none")
# plotly_fig.write_html("BER.html")