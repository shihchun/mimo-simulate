# clc clear variables close all
import numpy as np
import matplotlib.pyplot as plt
from PL3gpp38901 import PL_RMa,PL_UMa,PL_UMi,PL_InH # load pathloss
import matplotlib
plt.rcParams['axes.unicode_minus'] = False # 負號問題
plt.rcParams['mathtext.fontset'] = 'custom' 
plt.rcParams['mathtext.it'] = 'STIXGeneral:italic' 
plt.rcParams['mathtext.bf'] = 'STIXGeneral:italic:bold' 
matplotlib.rc('font', family='mingliu') # , weight='bold') # 設定新細明體
# matplotlib.rc('font', family='PingFang HK') # , weight='bold') # 設定蘋方字體

# from matplotlib.font_manager import FontManager
# fm = FontManager()
# mat_fonts = set(f.name for f in fm.ttflist)
# print (mat_fonts)

#Distances
d1 = 1000; d2 = 200

#Power allocation coefficients
a1 = 0.75; a2 = 0.25

# N = 5*10**5
N = int(5*1e4)
N=3000
eta = 4 #Path loss exponent

#Transmit power
Pt = np.arange(0,40+1,2) #in dBm
pt = (10**(-3))*10**(Pt/10)  #db2pow(Pt) #linear scale

BW = 90*10**6  #bandwidth
#Noise power
No = -174 + 10*np.log10(BW)   #in dBm
no = (10**(-3))*10**(No/10) #db2pow(No)    #in linear scale

#Target rates
R1 = 1
R2 = 3

p1n = np.zeros(len(pt))
p2n = np.zeros(len(pt))
p1o = np.zeros(len(pt))
p2o = np.zeros(len(pt))

Rn = [0]*len(Pt)
Ro = [0]*len(Pt)
R1n_av = [0]*len(Pt)
R2n_av = [0]*len(Pt)
R1o_av = [0]*len(Pt)
R2o_av = [0]*len(Pt)
for u in np.arange(len(pt)):
    Antenna_gain=12
    PL1 = PL_RMa(3.5,d1,70,1.5,condition='NLOS')
    PL2 = PL_RMa(3.5,d2,70,1.5,condition='NLOS')
    G1 = np.sqrt((10**((Pt[u]-(PL1-Antenna_gain) )/10))/2)
    G2 = np.sqrt((10**((Pt[u]-(PL2-Antenna_gain) )/10))/2)
    #Rayleigh fading channels
    h11 = G1*(np.random.randn(N) + 1j*np.random.randn(N))/np.sqrt(2)
    h12 = G1*(np.random.randn(N) + 1j*np.random.randn(N))/np.sqrt(2)
    h21 = G2*(np.random.randn(N) + 1j*np.random.randn(N))/np.sqrt(2)
    h22 = G2*(np.random.randn(N) + 1j*np.random.randn(N))/np.sqrt(2)

    h1 = h11+h12
    h2 = h21+h22
    #Channel gains
    g1 = (abs(h1))**2
    g2 = (abs(h2))**2
    #Achievable rates for MIMO-NOMA
    R1n = np.log2(1 + pt[u]*a1*g1/(pt[u]*a2*g1 + no))
    R12n = np.log2(1 + pt[u]*a1*g2/(pt[u]*a2*g2 + no))
    R2n = np.log2(1 + pt[u]*a2*g2/no)

    #Achievable rates for MIMO-OMA
    R1o = 0.5*np.log2(1 + pt[u]*g1/no)
    R2o = 0.5*np.log2(1 + pt[u]*g2/no)

    #Sum rates
    Rn[u] = np.mean(R1n+R2n)    #MIMO-NOMA
    Ro[u] = np.mean(R1o+R2o)    #MIMO-OMA

    #Invidual user rates
    R1n_av[u] = np.mean(R1n)   #MIMO-NOMA USER 1 (WEAK)
    R2n_av[u] = np.mean(R2n)   #MIMO-NOMA USER 2 (STRONG)

    R1o_av[u] = np.mean(R1o)   #MIMO-OMA USER 1 (WEAK)
    R2o_av[u] = np.mean(R2o)   #MIMO-OMA USER 1 (WEAK)

    #Outage calculation
    for k in np.arange(N):
        #MIMO-NOMA USER 1 (WEAK)
        if R1n[k] < R1:
            p1n[u] = p1n[u]+1
        #MIMO-NOMA USER 2 (STRONG)
        if (R12n[k]<R1) or ((R12n[k]>R1) and (R2n[k] < R2)):
            p2n[u] = p2n[u]+1
        #MIMO-OMA USER 1 (WEAK)
        if R1o[k] < R1:
            p1o[u] = p1o[u]+1
        #MIMO-OMA USER 2 (STRONG)
        if R2o[k] < R2:
            p2o[u] = p2o[u]+1

fig = plt.figure()
plt.plot(Pt, Rn, '-*b',label='NOMA')
plt.plot(Pt, Ro, '-*r',label='OMA')
plt.xlabel('Transmit power (dBm)')
plt.ylabel('Achievable sum rates (bps/Hz)')
plt.title('Sum rate comparison')
plt.legend()
plt.grid(True,which='both')
plt.tight_layout()

fig = plt.figure()
plt.semilogy(Pt,p1n/N,'-*b',label='NOMA 弱用戶 %dm'%d1)
plt.semilogy(Pt,p2n/N,'-ob',label='NOMA 強用戶 %dm'%d2)
plt.semilogy(Pt,p1o/N,'-*r',label='OMA 弱用戶 %dm'%d1)
plt.semilogy(Pt,p2o/N,'-or',label='OMA 強用戶 %dm'%d2)
plt.xlabel('Transmit power (dBm)')
plt.ylabel('Outage probability')
plt.title('Outage comparison')
plt.legend()
plt.grid(True,which='both')
plt.tight_layout()

fig = plt.figure()
plt.plot(Pt,R1n_av,'-*b',label='NOMA 弱用戶 %dm'%d1)
plt.plot(Pt,R2n_av,'-ob',label='NOMA 強用戶 %dm'%d2)
plt.plot(Pt,R1o_av,'-*r',label='OMA 弱用戶 %dm'%d1)
plt.plot(Pt,R2o_av,'-or',label='OMA 強用戶 %dm'%d2)
plt.xlabel('Transmit power (dBm)')
plt.ylabel('Achievable rates (bps/Hz)')
plt.title('Individual user rates')
plt.legend()
plt.grid(True,which='both')
plt.tight_layout()

plt.show()