

# Dependency

pip install numpy, scipy, matplotlib
pip install ModulationPy # 0.1.8 version

# plotly for HTML plot, or toggle comment out or remove sthe functions.py line 712:756

pip install plotly==4.8.2
pip install matplotlib==3.2.2

```console
python -m pip install ....
cd path/to/code_dir_with_function.py
python code_name.py
```

# Env

## How to install the pip

If you not use anaconda, you need to install the pip manually

[Check here](https://pip.pypa.io/en/stable/installing/)

```
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py # download get-pip.py
python get-pip.py
python -m pip install -U pip # upgrade pip
```

## if you use Anaconda

it automantically add your conda.exe, python.exe, pip.exe to your system ENV path.

Just call it on your shell or terminal cmd.

Initial setup of anaconda

```console
conda update conda
conda init cmd.exe # or bash, zsh...etc
```

You all set for python env

# Matlab Engine installation

Open the terminal in the path of your matlab
check the API version on matlab doc

```console
D:\MATLAB\R2021a\extern\engines\python
python setup.py install
```

Check the API working fine

```
ipython
In [1]: import matlab.engine
In [2]: eng = matlab.engine.start_matlab()
In [3]: eng
Out[3]: <matlab.engine.matlabengine.MatlabEngine at 0x28be4655910>
```