import numpy as np
import matplotlib.pyplot as plt
from scipy.special import i0 # modified Bessel function
from scipy.special import j0 # modified Bessel function
import matplotlib
plt.rcParams['axes.unicode_minus'] = False # 負號問題
plt.rcParams['mathtext.fontset'] = 'custom' 
plt.rcParams['mathtext.it'] = 'STIXGeneral:italic' 
plt.rcParams['mathtext.bf'] = 'STIXGeneral:italic:bold' 
matplotlib.rc('font', family='mingliu')#, weight='bold') # 設定新細明體
# matplotlib.rc('font', family='simsum') # 設定新細明體 windows用

plt.figure()
s = 1 # sigma
omega = s**2 # fading power 衰落功率
x = np.linspace(0, 5, 1000)
mu=1

# # Gaussian
# normalPDF = 1/np.sqrt(2*np.pi*omega)* np.exp(-1/2*(x-mu)**2/omega)
# plt.plot(x, normalPDF,label=r'Gaussian')

# Rayleigh
mu=0
rayPDF = (2*(x-mu) / (omega) ) * np.exp(- ((x-mu)**2 / omega) )
# raypdf[np.where(x <= 0)] = 0
plt.plot(x, rayPDF,label='瑞雷PDF (萊斯 K=0, Hoyt q=1)')

# Rician Nakagami-n
mu=0
k = [0,1,3,5]
for K in k:
    ricePDF = 2*(1+K)/omega *(x-mu) * np.exp(-K-( (1+K)*((x-mu)**2)/omega ))*i0( 2*(x-mu)*np.sqrt( K*(1+K)/omega ) )
    plt.plot(x, ricePDF,label=r'萊斯PDF $K$=%d'%K)


# Hoyt Nakagami-q 
Q = [0.3,0.5,1]
mu=0
for q in Q:
    a = (1+q**2)/(q*omega)
    b = (1+q**2)**2*(x**2)/(4*(q**2)*omega)
    c = (1-q**4)*(x**2) /(4*(q**2)*omega)
    hoytPDF = a*x* np.exp(-b)* i0(c)
    # hoytPDF = (1+q**2)/(q*omega) * np.exp(-(1+q**2)**2*((x-mu)**2)/(4*(q**2)*omega) ) * i0( (1-q**4) /(4*(q**2)*omega)*((x-mu)**2) )
    plt.plot(x, hoytPDF,label=r'Hoyt PDF $q$=%.1f'%q)

plt.legend(loc='upper right')
plt.grid(True,which='both')
plt.tight_layout()


# 產生通道畫出直方圖 density plot
# sigma=1
from scipy.stats import norm
N = int(10e5)
x = np.linspace(norm.ppf(0.01),norm.ppf(0.99), 100)
# # Gaussian
# h = (np.random.randn(N) + 1j*np.random.randn(N))
# plt.hist(abs(h),bins='auto',density=True,label='Gaussian',alpha=0.6)
#  顯示出現次數，density=True,出現次數/N


# Rayleigh flat fading
mu=0
plt.figure()
rayPDF = (2*(x-mu) / (omega) ) * np.exp(- ((x-mu)**2 / omega) )
h_ray = (np.random.randn(N) + 1j*np.random.randn(N))/np.sqrt(2)
plt.hist(abs(h_ray),bins='auto',density=True,label='Rayleigh',alpha=0.6)
plt.plot(x, rayPDF,'--',label=r'瑞雷理論PDF (Rician $K$=0, Hoyt $q$=1)')
plt.xlim([0,3]); plt.ylim([0,1])
plt.legend(loc='upper right')
plt.grid(True,which='both')
plt.tight_layout()

plt.figure()
plt.hist(abs(h_ray),bins='auto',density=True,label=r'瑞雷通道',alpha=0.6)

# Rician input K factor
mu=0
k = [0,3,10,15] #dB
# k = [0,0.5,1,2,4]
for K in k:
    KK = K
    if K ==0:
        K =0
    else:
        K = 10 ** (K / 10) # convert scale
    hi = np.sqrt(K/(K+1))+ np.sqrt(1/(2*(K+1)))*np.random.randn(N)
    hq = np.sqrt(1/(2*(K+1)))*np.random.randn(N)
    h_rice = hi+1j*hq # LOS
    K = KK
    plt.hist(abs(h_rice),bins='auto',density=True,label=r'萊斯通道 $K$=%d dB'%K,alpha=0.6)
    if K ==0:
        K =0
    else:
        K = 10 ** (K / 10) # convert scale
    ricePDF = 2*(1+K)/omega *(x-mu) * np.exp(-K-( (1+K)*((x-mu)**2)/omega ))*i0( 2*(x-mu)*np.sqrt( K*(1+K)/omega ) )
    K = KK
    plt.plot(x, ricePDF,label=r'萊斯理論PDF $K$=%d dB'%K)

plt.plot(x, rayPDF,'--',label='瑞雷理論PDF(萊斯 $K$=0, Hoyt $q$=1)')
plt.xlim([0,3]); plt.ylim([0,3.5])
plt.legend(loc='upper right')
plt.grid(True,which='both')
plt.tight_layout()

plt.figure()
# # Nakagami-m Hoyt input q factor
q=3
Q = [0.1,0.3,1]
plt.hist(abs(h_ray),bins='auto',density=True,label=r'瑞雷理論PDF(萊斯 $K$=0, Hoyt $q$=1)',alpha=0.6)
for q in Q:
    h_q = (1/(np.sqrt(1+q**2)))*( np.random.randn(N)+1j*q*np.random.randn(N) )
    plt.hist(abs(h_q),bins='auto',density=True,label=r'Hoyt通道 $q$=%.1f'%q,alpha=0.6)

    a = (1+q**2)/(q*omega)
    b = ((1+q**2)**2)*(x**2)/(4*(q**2)*omega)
    c = (1-q**4)*(x**2) /(4*(q**2)*omega)
    hoytPDF = a*x* np.exp(-b)* i0(c)
    # hoytPDF = (1+q**2)/(q*omega) * np.exp(-(1+q**2)**2*((x-mu)**2)/(4*(q**2)*omega) ) * i0( (1-q**4) /(4*(q**2)*omega)*((x-mu)**2) )
    plt.plot(x, hoytPDF,label=r'Hoyt理論PDF $q$=%.1f'%q)

plt.plot(x, rayPDF,'--',label=r'瑞雷理論PDF(萊斯 $K$=0, Hoyt $q$=1)')
plt.xlim([0,3]); plt.ylim([0,1])
plt.legend(loc='upper right')
plt.grid(True,which='both')
plt.tight_layout()


plt.figure()
for sigma in [0.5,1,3,4]:
    omega = sigma**2
    rayPDF2 = (2*(x-mu) / (omega) ) * np.exp(- ((x-mu)**2 / omega) )
    h_ray = sigma*(np.random.randn(N) + 1j*np.random.randn(N))/np.sqrt(2)
    plt.hist(abs(h_ray),bins='auto',density=True,label='瑞雷通道 $\sigma$=%.1f'%sigma,alpha=0.6)
    plt.plot(x, rayPDF2,'--',label=r'理論瑞雷PDF $\sigma$=%.1f'%sigma)
plt.xlim([0,3]); plt.ylim([0,2])
plt.legend(loc='upper right')
plt.grid(True,which='both')
plt.tight_layout()


q = 0.1
h_ray = (np.random.randn(N) + 1j*np.random.randn(N))/np.sqrt(2)
h_q = (1/(np.sqrt(1+q**2)))*( np.random.randn(N)+1j*q*np.random.randn(N) )
k = 3 # 3dB
K = 10 ** (k / 10) # convert scale
hi = np.sqrt(K/(K+1))+ np.sqrt(1/(2*(K+1)))*np.random.randn(N)
hq = np.sqrt(1/(2*(K+1)))*np.random.randn(N)
h_rice = hi+1j*hq # LOS
plt.figure()
plt.plot(20*np.log10( abs(h_ray[0:1000]) ),label=r'瑞雷通道',alpha=0.4,marker = 'o', markersize=2)
plt.plot(20*np.log10(abs(h_rice[0:1000]) ),label=r'萊斯通道 $K$=%1.fdB'%k)
plt.plot(20*np.log10(abs(h_q[0:1000]) ),label=r'Hoyt通道 $q$=%.1f'%q,alpha=0.4)
plt.plot([0,1000], [0, 0], '--r',linewidth=2,label='無衰落',alpha=0.4) # 0 dB
plt.legend(loc='upper right')
plt.grid(True,which='both')
plt.xlabel('index')
plt.ylabel('Power(dB)')
plt.ylim([-30,10])
plt.tight_layout()

plt.figure()
plt.hist(abs(h_ray),bins='auto',density=True,label='瑞雷通道',alpha=0.6)
plt.hist(abs(h_rice),bins='auto',density=True,label='萊斯通道 $K$=3dB',alpha=0.6)
plt.hist(abs(h_q),bins='auto',density=True,label='Hoyt通道 $q=0.1$',alpha=0.6)
plt.legend(loc='upper right')
plt.grid(True,which='both')
plt.xlabel('Power (watt)')
plt.ylabel('Density')
plt.tight_layout()


plt.figure()
plt.plot(abs(h_ray[0:1000]) ,label=r'瑞雷通道',alpha=0.4,marker = 'o', markersize=2)
plt.plot(abs(h_rice[0:1000]) ,label=r'萊斯通道 $K$=%1.fdB'%k)
plt.plot(abs(h_q[0:1000]) ,label=r'Hoyt通道 $q$=%.1f'%q,alpha=0.4)
plt.legend(loc='upper right')
plt.grid(True,which='both')
plt.xlabel('index')
plt.ylabel('Power(watt)')
plt.tight_layout()
plt.show()
