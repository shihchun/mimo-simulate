# pip install scikit-commpy numpy matplotlib pandas torch sklearn
# torch version 1.6
import os
os.environ["KMP_DUPLICATE_LIB_OK"]="TRUE"
import warnings
warnings.filterwarnings('ignore') # "ignore", "default", "always", "error"(thought error)
from ModulationPy import PSKModem, QAMModem
modem = QAMModem(4)
modem.soft_decision = False

import torch
from torch.autograd import Variable
torch.set_printoptions(precision=2) ## Pytorch 精度print顯示 ---> 跟Numpy差不多
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
from complexLayers import ComplexBatchNorm1d, ComplexLinear
from complexFunctions import complex_relu

# model = DNN_NET(nSample,4)
class DNN_NET(nn.Module):
    def __init__(self, in_channels, modd):
        super(DNN_NET, self).__init__()
        self.nn1 = ComplexLinear(in_channels, modd*in_channels) # layer 1
        self.nn2 = ComplexLinear(modd*in_channels, modd*modd*in_channels) # layer 1
        self.nn3 = ComplexLinear(modd*modd*in_channels, modd*in_channels) # layer 1
        self.nn4 = ComplexLinear(modd*in_channels, in_channels) # layer 1

    def forward(self, x):
        x = self.nn1(x)
        x = complex_relu(x)
        x = self.nn2(x)
        x = complex_relu(x)
        x = self.nn3(x)
        x = complex_relu(x)
        x = self.nn4(x)
        x = complex_relu(x)
        return x

N = 5*10**5
# N = int(5*1e4)
N = 3000
Pt = np.arange(0,40+1,2) #Transmit power (dBm) 0:2:40
# Pt = np.arange(-40,40+1,5) #Transmit power (dBm) 0:2:40
pt = (10**(-3))*10**(Pt/10)  #db2pow(Pt) #Transmit power (linear scale)
BW = 10**6   #Bandwidth = 1 MHz
No = -174 + 10*np.log10(BW)	#Noise power (dBm)
# No = -164 + 10*np.log10(BW)	#Noise power (dBm)
no = (10**(-3))*10**(No/10) #db2pow(No) #Noise power (linear scale) 
# 10^(-Eb_N0_dB(ii)/20)*n    10**()
alpha = [0.8,0.15,0.05] #Power allocation coefficients
eta = 4	#Path loss exponent

# nSample= int(N/(2**0)) # bpsk
nSample= int(N/(2**1)) # qpsk/4QAM



from functions import NOMA_SPC,NOMA_SIC
# d = np.random.uniform(low=5,high=500,size=1) # distance is random
# h = np.sqrt(np.random.uniform(low=5,high=500,size=1)**(-eta))*(np.random.randn(nSample) + 1j*np.random.randn(nSample))/np.sqrt(2)


train_num =1000 #8000
test_num = 6000 #50000
NUM_EPOCHS = 1000 # mini batch :重複送入label次數 用dataloader 完成

from functions import NOMA_channel
def generate_channel(num,N,alpha,eta,sigma2,noise_pwr,modem):
    label_1 = np.random.randint(0, 1+1, (num,N)) # UE1
    label_2 = np.random.randint(0, 1+1, (num,N)) # UE2
    label_3 = np.random.randint(0, 1+1, (num,N)) # UE3
    
    nSample = int( N/( 2**(  int(np.log2(modem.M)/2) ) ) )
    xmod = np.zeros( (label_1.shape[0] ,nSample),dtype='cfloat' )
    for i in np.arange(label_1.shape[0]):
        xmod[i] = np.sqrt(alpha[0])*modem.modulate(label_1[i]) + np.sqrt(alpha[1])*modem.modulate(label_2[i]) + np.sqrt(alpha[2])*modem.modulate(label_3[i])

    eq_label = xmod*np.sqrt(noise_pwr) # constellation label -174dBm channel -80~-100dBm
    # np.sqrt(np.random.uniform(low=5,high=500,size=num)**(-eta)) # random distance loss channel
    # h = np.array([ np.sqrt(np.random.uniform(low=5,high=500,size=num)**(-eta))[:,None] * (np.random.randn(num,nSample) + 1j*np.random.randn(num,nSample))/np.sqrt(2) for _ in range(3) ])
    h = np.sqrt(np.random.uniform(low=5,high=500,size=num)**(-eta))[:,None] * (np.random.randn(num,nSample) + 1j*np.random.randn(num,nSample))/np.sqrt(2)
    
    # sigma2 = np.random.uniform(low=pt[0],high=pt[-1],size=1) # SNR power linear scale 1~10
    # y = []
    # y.append([ np.sqrt(sigma2)*xmod*hi + np.sqrt(no)*(np.random.randn(nSample) + 1j*np.random.randn(nSample))/np.sqrt(2)  for hi in h])
    # y = np.array(y).squeeze()
    y = np.sqrt(sigma2)*xmod*h + np.sqrt(no)*(np.random.randn(nSample) + 1j*np.random.randn(nSample))/np.sqrt(2)
    return {'SPC':xmod, 'Eq':eq_label, 'h':h, 'y': y, '0':label_1,'1':label_2,'2':label_3}


# # NOMA constellation Eq label with noise power no, y with noise power(with no +randn gaussian noise)
# res = generate_channel(train_num,N, alpha, eta, pt[15], no, modem) # pt[15] = signal noise power, no channel noise power
# # decxx = NOMA_SIC( res['y'][0][5]/res['h'][0][5], 1, pt[15], alpha, modem) # h not use insert 1
# decxx = NOMA_SIC( res['y'][5]/res['h'][5], 1, pt[15], alpha, modem) # h not use insert 1
# # res['Eq'][0] # Eq label 1
# # res['y'][0][5]/res['h'][0][5] # channel 1, label 5
# remm1 = decxx['rem0']
# remm2 = decxx['rem1']
# remm3 = decxx['rem2']
# ber1 = np.sum( decxx['0'] != res['0'][5])/N # ber1 label1 6
# ber2 = np.sum( decxx['1'] != res['1'][5])/N # ber2 label2 6
# ber3 = np.sum( decxx['2'] != res['2'][5])/N # ber3 label3 6
# fig = plt.figure()
# plt.title('Constellation SPC')
# plt.plot(np.real( remm1), np.imag( remm1  ), '.r', label = 'Dec1 BER=%f'%ber1)
# plt.plot(np.real( remm2 ), np.imag( remm2  ), '.g', label = 'Dec2')
# plt.plot(np.real( remm3 ), np.imag( remm3  ), '.b', label = 'Dec3')
# plt.xlabel(r'I')
# plt.ylabel(r'Q')
# plt.legend(loc='upper right')
# plt.grid(True,which='both')
# plt.tight_layout()
# plt.show()

# # label different snr
# import deepdish as dd
# print('dataset generation')
# dataset = {}
# for u in np.arange(len(Pt)): # pt[u] signal power in linear scale = sigma2
#     dataset[str(u)] = generate_channel(train_num, N, alpha, eta, pt[u], no, modem)
# dd.io.save('DNN_LABEL_SISO.h5', dataset, compression=None)
# print('done !')

# label Loading
import deepdish as dd
dataset2 = dd.io.load('DNN_LABEL_SISO.h5')

# train_data = [ x['y'] for x in dataset2]
y = np.array([ dataset2[x]['y'] for x in dataset2.keys()]) # (snr,labels, data)
y = y.reshape(-1, y.shape[-1]) # (label,data)

h = np.array([ dataset2[x]['h'] for x in dataset2.keys()]) # (snr,labels, data)
h = h.reshape(-1, h.shape[-1]) # (label,data)

Eq = np.array([ dataset2[x]['Eq'] for x in dataset2.keys()]) # (snr,labels, data)
Eq = Eq.reshape(-1, Eq.shape[-1]) # (label,data)

x1 = np.array([ dataset2[x]['0'] for x in dataset2.keys()]) # (snr,labels, data)
x1.reshape(-1, x1.shape[-1]) # (label,data)

x2 = np.array([ dataset2[x]['1'] for x in dataset2.keys()]) # (snr,labels, data)
x2 = x2.reshape(-1, x2.shape[-1]) # (label,data)

x3 = np.array([ dataset2[x]['2'] for x in dataset2.keys()]) # (snr,labels, data)
x3 = x3.reshape(-1, x3.shape[-1]) # (label,data)


# import torch.utils.data as Data # auto shuffle & multiprocessing
# from torch.optim import Adam,RMSprop

# BATCH_SIZE = 32
# NUM_EPOCHS = 100
# # dataset = Data.TensorDataset( train_data,  train_labels)
# # train_loader = Data.DataLoader(dataset = dataset, batch_size = 32, shuffle = True, num_workers = 2) # num_workers = 0 for each worker
# dataset = Data.TensorDataset( torch.tensor(y,dtype=torch.cfloat),  torch.tensor(h,dtype=torch.cfloat), torch.tensor(Eq,dtype=torch.cfloat) )
# train_loader = Data.DataLoader(dataset = dataset, batch_size = BATCH_SIZE, shuffle = True, num_workers = 0)
# model = DNN_NET(nSample,4)
# optimizer = Adam(model.parameters(),lr=0.001)
# loss_fn = nn.CrossEntropyLoss()

# for epoch in range(NUM_EPOCHS):
#     for step, (y, h, Eq) in enumerate(train_loader): # y, h, Eq
#         # print(x.shape,y.shape,z.shape)
#         yy = model(y)
#         equalized = yy/h
#         loss_fn(equalized, Eq) # denoise loss
#         optimizer.zero_grad() # clear gradients for this training step
#         loss.backward() # backpropagation, compute gradients
#         optimizer.step()
#         if step % 100 == 0:
#             print('Epoch: ', epoch, '| train loss: %.4f' % (loss.data.item()) )

