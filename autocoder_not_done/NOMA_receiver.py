#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import torch
from torch import nn
import torch.nn.functional as F
import numpy as np
import matplotlib.pyplot as plt
from ModulationPy import PSKModem, QAMModem
Pt = torch.arange(0,40+1,2, dtype=torch.float) # Transmit power (dBm) 0:2:40
Pt = torch.arange(0,2+1,0.5, dtype=torch.float) # Transmit power (dBm) 0:2:40
pt = (10**(-3))*10**(Pt/10)  # db2pow(Pt) #Transmit power (linear scale)
BW = torch.tensor(10**6, dtype=torch.float)   # Bandwidth = 1 MHz
No = -174 + 10*torch.log10(BW)	+7 # Noise power (dBm) Noise floor
no = (10**(-3))*10**(No/10) # db2pow(No) #Noise power (linear scale) 
d = [500,200,70]
alpha=[0.8,0.15,0.05]

modem = PSKModem(4)
modem = QAMModem(4)
modem.soft_decision = False

Blocks = 29
fftSize = 64
resample = 300
N = modem.N*fftSize*Blocks
nSample = N//modem.N
# input
x1 = torch.randint(0,1+1,(N,))
from fun_torch import NOMA_SPC, ray, Interp1d

# network

# z = [ray(fftSize,Blocks,Fs,Fs/10) for i in range(3)] # ray(fftSize,Blocks,Fs,Fs/10)
# from scipy import signal
# import matplotlib.pyplot as plt

# for _ in range(3):
#     z[_] = signal.resample(z[_],resample*len(z[_]))
#     # plt.figure();plt.plot(20*np.log10(abs(z[_])),label='Channel');plt.legend()
#     # plt.xlabel('Index')
#     # plt.ylabel('Envelope(dB)')
#     # plt.title('channel %d'%(_+1))
#     # plt.grid(True)
#     z[_] = z[_][np.arange(nSample)]
# SPC = NOMA_SPC(N, alpha, modem)
# x = SPC['x']
# xp =  SPC['pilot']['spc_value']

# for u in np.arange(len(Pt)):
#     pilots = y[_][ SPC['pilot']['ind'] ] #- np.sqrt(no)*((1+1j)/np.sqrt(2))*np.random.randn(len(y[_][ SPC['pilot']['ind'] ])) # get pilot index
#     Hest_at_pilots = pilots  / SPC['pilot']['spc_value'] /( np.sqrt(pt[u]) + np.sqrt(no)*(np.random.randn(len(pilots)) + 1j*np.random.randn(len(pilots)))/np.sqrt(2) )  # use divide '/', this is freq domain
#     Hest_abs = Interp1d()(torch.tensor(SPC['pilot']['ind']), torch.Tensor(abs(Hest_at_pilots)), torch.tensor(np.arange(len(h[_]))) ) # np.arange(len(y[_])) all index
#     Hest_phase = Interp1d()( torch.tensor(SPC['pilot']['ind']), torch.Tensor(np.angle(Hest_at_pilots)),torch.tensor(np.arange(len(h[_]))) )
#     Hest = Hest_abs * np.exp(1j*Hest_phase)  # Hest = Hest.numpy().squeeze() # pytorch


from complexLayers import ComplexLinear,ComplexReLU
from complexFunctions import complex_relu
from complex_loss import softmax # complex softmax

class ComplexNet(nn.Module): # Auto Encoder Network
    def __init__(self):
        super(ComplexNet, self).__init__()
        self.in_channels = in_channels
        self.enc1 = nn.Sequential(
            ComplexLinear(in_channels, in_channels),
            # nn.Tanh(),
            ComplexReLU(),
            ComplexLinear(in_channels, compressed_dim),
        )
        self.dec1 = nn.Sequential(
            ComplexLinear(compressed_dim, in_channels),
            ComplexReLU(),
            ComplexLinear(in_channels, in_channels)
        )
        self.dec2 = nn.Sequential(
            ComplexLinear(compressed_dim, in_channels),
            ComplexReLU(),
            ComplexLinear(in_channels, in_channels)
        )
        self.dec3 = nn.Sequential(
            ComplexLinear(compressed_dim, in_channels),
            ComplexReLU(),
            ComplexLinear(in_channels, in_channels)
        )
        
    def encode(self, x):
        x1=self.encoder1(x)
        # x1 = (self.in_channels ** 2) * (x1 / x1.norm(dim=-1)[:, None])
        x1 = (self.in_channels ** 2) * (x1 / torch.tensor(np.linalg.norm(x1.detach().numpy())))
        return x1
    def decode1(self, x):
        x1=self.encoder2(x)
        # x1 = (self.in_channels ** 2) * (x1 / x1.norm(dim=-1)[:, None])
        x1 = (self.in_channels ** 2) * (x1 / torch.tensor(np.linalg.norm(x1.detach().numpy())))
        return x1
    def decode2(self, x):
        return self.decoder1(x)
    def decode3(self, x):
        return self.decoder2(x)

    def equalize(self, x1, h):
        
        # x1 = (self.in_channels ** 0.5) * (x1 / x1.norm(dim=-1)[:, None])
        # # bit / channel_use
        # communication_rate = R
        # # Simulated Gaussian noise. Gaussian interference channel to achieve the sum-rate capacity
        # noise1 = Variable(torch.randn(*x1.size()) / ((2 * communication_rate * ebno) ** 0.5))
        return signal1,signal2

    def forward(self, x1,x2):
        x1 = self.encoder1(x1)
        x2 = self.encoder2(x2)

        # Normalization.
        # x1 = (self.in_channels **0.5) * (x1 / x1.norm(dim=-1)[:, None])
        x1 = (self.in_channels **0.5)  * (x1 / torch.tensor(np.linalg.norm(x1.detach().numpy())))
        # x2 = (self.in_channels **0.5) * (x2 / x2.norm(dim=-1)[:, None])
        x2 = (self.in_channels **0.5) * (x2 / torch.tensor(np.linalg.norm(x2.detach().numpy())))

        # 7dBW to SNR.
        training_signal_noise_ratio =  5.01187

        # bit / channel_use
        communication_rate = R

        # Simulated Gaussian noise.
        noise1 = Variable(torch.randn(*x1.size()) / ((2 * communication_rate * training_signal_noise_ratio) ** 0.5))
        noise2 = Variable(torch.randn(*x2.size()) / ((2 * communication_rate * training_signal_noise_ratio) ** 0.5))
        signal1=x1+noise1+x2
        signal2=x1+x2+noise2
        
        decode1 = self.decoder1(signal1)
        decode2 = self.decoder2(signal2)

        return decode1,decode2


batch_size = 64
n_train = 1000
n_test = 100
trans = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.5,), (1.0,))])
# train_set = datasets.MNIST('./data', train=True, transform=trans, download=True)
train_set = Subset(train_set, torch.arange(n_train))
# test_set = datasets.MNIST('./data', train=False, transform=trans, download=True)
test_set = Subset(test_set, torch.arange(n_test))

train_loader = torch.utils.data.DataLoader(train_set, batch_size= batch_size, shuffle=True)
test_loader = torch.utils.data.DataLoader(test_set, batch_size= batch_size, shuffle=True)

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
model = ComplexNet().to(device)
optimizer = torch.optim.SGD(model.parameters(), lr=5e-3, momentum=0.9)


# data is SPC dictionary channel, target is 
def train(model, device, train_loader, optimizer, epoch):
    model.train()
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target =data.to(device).type(torch.complex64), target.to(device)
        # print(data)
        optimizer.zero_grad()
        output = model.encode(data)
        # print(output)
        prb_output = softmax(output,dim=-1) # when traning dim [0 1], 0 is batchsize, -1 is heighest dim
        prb_target = softmax(target,dim=-1)
        loss = F.cross_entropy(output, prb_target) # insert softmax probility
        loss.backward()
        optimizer.step()
        if batch_idx % 100 == 0:
            print('Train\t Epoch: {:3} [{:6}/{:6} ({:3.0f}%)]\tLoss: {:.6f}'.format(
                epoch,
                batch_idx * len(data), 
                len(train_loader.dataset),
                100. * batch_idx / len(train_loader), 
                loss.item()
                )
            )
            
def test(model, device, test_loader, optimizer, epoch):
    model.eval()
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target =data.to(device).type(torch.complex64), target.to(device)
        output = model(data)
        prb_output = softmax(output,dim=-1) # when testing dim [0], -1 is heighest dim
        prb_target = softmax(target,dim=-1)
        loss = F.cross_entropy(output, prb_target) # insert softmax probility
        if batch_idx % 100 == 0:
            print('Test\t Epoch: {:3} [{:6}/{:6} ({:3.0f}%)]\tLoss: {:.6f}'.format(
                epoch,
                batch_idx * len(data), 
                len(train_loader.dataset),
                100. * batch_idx / len(train_loader), 
                loss.item())
            )
# Run training on 4 epochs
for epoch in range(4):
    train(model, device, train_loader, optimizer, epoch)
    test(model, device, test_loader, optimizer, epoch)
