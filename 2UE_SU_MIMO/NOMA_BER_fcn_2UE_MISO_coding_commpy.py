# Commpy Channel Coding it's very slow and seems not correct

import sys
sys.path[0] += '\\..' # import module from parant dir

import numpy as np
from numpy.linalg import inv,pinv
import matplotlib.pyplot as plt
from ModulationPy import PSKModem, QAMModem
from functions import NOMA_SIC,NOMA_SPC,NOMA_channel,awgn,threshold_bit,PL_Hata
import scipy.linalg as la
from PL3gpp38901 import PL_RMa,PL_UMa,PL_UMi,PL_InH # load pathloss
import matplotlib
# import logging
# logging.getLogger('matplotlib.font_manager').disabled = True
plt.rcParams['axes.unicode_minus'] = False # 負號問題
plt.rcParams['mathtext.fontset'] = 'custom' 
plt.rcParams['mathtext.it'] = 'STIXGeneral:italic' 
plt.rcParams['mathtext.bf'] = 'STIXGeneral:italic:bold' 
matplotlib.rc('font', family='mingliu') # , weight='bold') # 設定新細明體
matplotlib.rc('font', family='PingFang HK') # , weight='bold') # 設定蘋方字體

# from matplotlib.font_manager import FontManager
# fm = FontManager()
# mat_fonts = set(f.name for f in fm.ttflist)
# print (mat_fonts)

import commpy.channelcoding.convcode as cc # trellis encoder and viterbi decoder
M = 4 # modulation order (QPSK)
# k = np.log2(M) #number of bit per modulation symbol
# rate = 1/2
# No_code = 10*np.log10(1/(2.0*rate)) # rate = 1/2, No =0
# g_matrix = np.array([[5, 7]]) # generator branches
# trellis = cc.Trellis(np.array([M]), g_matrix) # Trellis structure
# punctureMatrix=np.array([[1,0,1],[1,1,0]])
# msg = np.random.randint(0, 1+1, 3000) # message
# enc = cc.conv_encode(msg, trellis,puncture_matrix=punctureMatrix)
# # enc = cc.conv_encode(msg, trellis)
# dec = cc.viterbi_decode(enc, trellis, decoding_type='hard')[:msg.size]
# L = 7 # constraint length
# m = np.array([L-1]) # number of delay elements
# tb_depth = 5*(m.sum() + 1) # traceback depth
# dec = cc.viterbi_decode(enc, trellis, tb_depth, decoding_type='hard')[:msg.size]

def NOMA_SPC_ch(N, alpha, modem, trellis, punctureMatrix=None):
    res = {}
    for _ in np.arange(len(alpha)): # ue amout
        temp = np.random.binomial(n=1, p=0.5, size=(N, ))
        enc = cc.conv_encode(temp, trellis, puncture_matrix=punctureMatrix)
        # temp = np.random.randint(0, 1+1, N) #Generate random binary message data for the users
        xmod = modem.modulate(enc)
        res[str(_)] = temp
        if _==0:
            x = np.sqrt(alpha[_])*xmod
        else:
            x += np.sqrt(alpha[_])*xmod
    res['x']=x
    res['N'] = int(N/modem.N)
    # x = np.sqrt(a1)*xmod1 + np.sqrt(a2)*xmod2 + np.sqrt(a3)*xmod3
    return res

#Create QPSKModulator and QPSKDemodulator objects
# modem = PSKModem(2）
modem = PSKModem(4) 
modem = QAMModem(4)
# modem.plot_const() # plot constellation
modem.soft_decision = False
# modem.demodulate( modem.modulate([0,1,1,1])).astype(int) # hard decision

N = 5*10**5
N = int(5*1e4)
N = 3000
Pt = np.arange(0,40+1,2) #Transmit power (dBm) 0:2:40
# Pt = np.arange(0,40+1,5) #Transmit power (dBm) 0:2:40
# Pt = np.arange(-40,50+1,3) #Transmit power (dBm) 0:2:40
Pt = np.arange(0,20+1,2) #Transmit power (dBm) 0:2:40
pt = (10**(-3))*10**(Pt/10)  #db2pow(Pt) #Transmit power (linear scale)

BW = 10**6   #Bandwidth = 1 MHz

d1 = 1000; d2 = 500	#Distances 
a1 = 0.75; a2 = 0.25   #Power allocation factors

nSample= int(N/modem.N) # qpsk/4QAM
Nt = 2
Nr = 1
hh1 = (np.random.randn(nSample,Nr,Nt) + 1j*np.random.randn(nSample,Nr,Nt))/np.sqrt(2)
hh2 = (np.random.randn(nSample,Nr,Nt) + 1j*np.random.randn(nSample,Nr,Nt))/np.sqrt(2)

#Do super position coding
alpha=[a1,a2]
SPC = NOMA_SPC(N, alpha, modem)
x = SPC['x']

ber1 = np.zeros((Nt,len(Pt)))
ber2 = np.zeros((Nt,len(Pt)))
ber3 = np.zeros((Nt,len(Pt)))
EVM1 = np.zeros((Nt,len(Pt)))
EVM2 = np.zeros((Nt,len(Pt)))
EVM1_dB = np.zeros((Nt,len(Pt)))
EVM2_dB = np.zeros((Nt,len(Pt)))
rem1 = np.zeros((Nt,nSample),dtype='cfloat')
rem2 = np.zeros((Nt,nSample),dtype='cfloat')
comb = np.zeros( (3,nSample,Nr,Nt),dtype='cfloat' )

# 2 User
r = np.zeros((2,nSample,Nr,1),dtype='cfloat') # Rx received Signal with Nr path
y = np.zeros((2,nSample,Nt,1),dtype='cfloat') # Equalized Signal with Nt path

# MRT
EVM1_mrt = np.zeros((len(Pt))); EVM2_mrt = np.zeros((len(Pt)))
y_mrt = np.zeros((2,nSample,1),dtype='cfloat')
ber1_mrt = np.zeros((len(Pt))); ber2_mrt = np.zeros((len(Pt)))

# channel coding
r1 = 1/2
No_r1 = 10*np.log10(1/(2.0*r1)) # in qpsk, No=0 when rate = 1/2
no_r1 = 1/(2.0*r1)
g_matrix = np.array([[5, 7]]) # generator branches
trellis = cc.Trellis(np.array([M]), g_matrix) # Trellis structure
# SPC_r1 = NOMA_SPC(N, alpha, modem)
SPC_r1 = NOMA_SPC_ch(N, alpha, modem, trellis)
x_r1 = SPC_r1['x']
nSample_r1 = len(x_r1)
hh1_r1 = (np.random.randn(nSample_r1,Nr,Nt) + 1j*np.random.randn(nSample_r1,Nr,Nt))/np.sqrt(2)
hh2_r2 = (np.random.randn(nSample_r1,Nr,Nt) + 1j*np.random.randn(nSample_r1,Nr,Nt))/np.sqrt(2)
r_r1 = np.zeros((2,nSample_r1,Nr,1),dtype='cfloat') # Rx received Signal with Nr path
y_r1 = np.zeros((2,nSample_r1,Nt,1),dtype='cfloat') # Equalized Signal with Nt path
ber1_r1 = np.zeros((Nt,len(Pt)))
ber2_r1 = np.zeros((Nt,len(Pt)))
EVM1_r1 = np.zeros((Nt,len(Pt)))
EVM2_r1 = np.zeros((Nt,len(Pt)))
EVM1_dB_r1 = np.zeros((Nt,len(Pt)))
EVM2_dB_r1 = np.zeros((Nt,len(Pt)))
rem1_r1 = np.zeros((Nt,nSample_r1),dtype='cfloat')
rem2_r1 = np.zeros((Nt,nSample_r1),dtype='cfloat')
# MRT
EVM1_mrt_r1 = np.zeros((len(Pt))); EVM2_mrt_r1 = np.zeros((len(Pt)))
y_mrt_r1 = np.zeros((2,nSample_r1,1),dtype='cfloat')
ber1_mrt_r1 = np.zeros((len(Pt))); ber2_mrt_r1 = np.zeros((len(Pt)))

for u in np.arange(len(Pt)):
    Antenna_gain=12
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'big')
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'small/medium')
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'suburban')
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'open')
    # PL_RMa(3.5,d1,70,1.5)
    # PL_UMa(3.5,d1,1.5)
    # PL_UMi(3.5,d1,1.5)
    # PL_InH(3.5,10)
    PL1 = PL_RMa(3.5,d1,70,1.5,condition='NLOS')
    PL2 = PL_RMa(3.5,d2,70,1.5,condition='NLOS')
    G1 = np.sqrt((10**((Pt[u]-(PL1-Antenna_gain) )/10))/2)
    G2 = np.sqrt((10**((Pt[u]-(PL2-Antenna_gain) )/10))/2)
    h1 = G1*hh1
    h2 = G2*hh2
    h = [h1,h2] # MIMO channel Nr*Nt assume each path have same pathloss with a distance

    No = -174 + 10*np.log10(BW)+7	#  Noise power (dBm)
    no = (10**(-3))*10**(No/10) # db2pow(No) # Noise power (linear scale)

    # input signal SPC no need h get codebook
    decxx = NOMA_SIC(np.sqrt(pt[u]/Nt)*x, h[0], pt[u]/Nt, alpha, modem)
    remm1 = decxx['rem0']
    remm2 = decxx['rem1']
    # input signal SPC no need h get codebook
    decxx = NOMA_SIC(np.sqrt(pt[u]/Nt)*x_r1, h[0], pt[u]/Nt, alpha, modem)
    remm1_r1 = decxx['rem0']
    remm2_r1 = decxx['rem1']
    # code_book for non-normalized signal perform ML detection
    code_ue1 = np.unique(remm1, axis=0) # remove repeat element
    code_ue2 = np.unique(remm2, axis=0)
    def ML_detect(y,code_book):
        # using non-normalized constellation as code_book
        z = [code_book[np.argmin(np.abs(code_book - x))] for x in np.nditer(y)]
        return np.array(np.asmatrix(z).reshape(y.shape))

    for ichannel in np.arange(nSample):
        # y = xh+n
        # y = NOMA_channel(x, h, pt[u],BW,-174) # Channel with noise
        
        d = [d1,d2]
        for _ in range(2):
            n = np.sqrt(no)*(np.random.randn(Nr,1) + 1j*np.random.randn(Nr,1))/np.sqrt(2)

            ### SVD
            U, S, V_H = np.linalg.svd(h[_][ichannel])
            W = V_H.conj().T / np.sqrt(Nt)
            G_H = np.diag(1. / S).dot(U.conj().T) * np.sqrt(Nt)

            r[_][ichannel] = np.sqrt(pt[u]/Nt) * ( h[_][ichannel]@ W@np.tile(x[ichannel],(h[_][ichannel].shape[1],1)) ) + n # nSample, Nr, 1
            # awgn additive noise channel
            # r[_][ichannel] = awgn( np.sqrt(pt[u]) * ( h[_][ichannel]@ np.tile(x[ichannel],(h[_][ichannel].shape[1],1)) )+ n, Pt[u]) # nSample, Nr, 1

            Cx = (pt[u]/no)*np.identity(Nt) # all antennas receiving same data np.var(x)
            Cz = np.identity(Nr) # Covariance matrix of noise. Currently assuming uncorrelated across antennas.
            if Nt==Nr:
                # print('Nt==Nr')
                Hh = h[_][ichannel].conj().T
                tM = Hh@(inv(Cz)@h[_][ichannel]) + inv(Cx)
                EqMMSE = inv(tM)@Hh
                EqZF = inv(h[_][ichannel])
            else:
                Hh = h[_][ichannel].conj().T
                tM = Hh@(pinv(Cz)@h[_][ichannel]) + pinv(Cx)
                EqMMSE = pinv(tM)@Hh
                EqZF = pinv(h[_][ichannel])

            # y[_] = awgn(y[_],Pt[u]) # add noise already
            # precoding matrix for channel matrix used at vector space
            # wt =  h[_][ichannel].T.conj() @ pinv( h[_][ichannel]@h[_][ichannel].T.conj() )

            # h[_][ichannel] = wt@h[_][ichannel]
            # wt = EqZF
            # wt = EqMMSE

            wt =  (h[_][ichannel]@W).T.conj() @ pinv( h[_][ichannel]@W@(h[_][ichannel]@W).T.conj() )
            y[_][ichannel] = ( wt @ r[_][ichannel] ) # nSample, Nt, 1
            # y[_][ichannel] = ML_detect(y[_][ichannel],code_ue1)
            # modulator additive AWGN noise after equalize
            # y[_][ichannel] = awgn( wt @ r[_][ichannel],Pt[u] ) # awgn after Equalizer
            W_mrt = np.exp(-1j * np.angle( h[_][ichannel]) ).T / np.sqrt(Nt)
            xx = W_mrt*np.tile(x[ichannel],(h[_][ichannel].shape[1],1)) # Element-wise multiplication (use broadcast)
            r[_][ichannel] = np.sqrt(pt[u]/Nt) * ( h[_][ichannel]@ xx ) + n # nSample, Nr, 1
            G_H = np.sqrt(Nt) / np.sum( np.abs(h[_][ichannel]) )
            y_mrt[_][ichannel] = G_H*r[_][ichannel] # Element-wise multiplication (use broadcast)
            # maybe you can do sth
            # k = y[_][ichannel] #/h[_][ichannel] # h[_]
            # comb[_] = y[_]

    for ichannel in np.arange(nSample_r1):
        # y = xh+n
        # y = NOMA_channel(x, h, pt[u],BW,-174) # Channel with noise
        h_r1 = [G1*hh1_r1, G2*hh2_r2] # share same pathloss
        d = [d1,d2]
        for _ in range(2):
            n = np.sqrt(no)*(np.random.randn(Nr,1) + 1j*np.random.randn(Nr,1))/np.sqrt(2)
            ### SVD
            U, S, V_H = np.linalg.svd(h_r1[_][ichannel])
            W = V_H.conj().T / np.sqrt(Nt)
            G_H = np.diag(1. / S).dot(U.conj().T) * np.sqrt(Nt)
            r_r1[_][ichannel] = np.sqrt(pt[u]/Nt) * ( h_r1[_][ichannel]@ W@np.tile(x_r1[ichannel],(h_r1[_][ichannel].shape[1],1)) ) + n # nSample, Nr, 1
            wt =  (h_r1[_][ichannel]@W).T.conj() @ pinv( h_r1[_][ichannel]@W@(h_r1[_][ichannel]@W).T.conj() )
            y_r1[_][ichannel] = ( wt @ r_r1[_][ichannel] ) # nSample, Nt, 1

            W_mrt = np.exp(-1j * np.angle( h_r1[_][ichannel]) ).T / np.sqrt(Nt)
            xx = W_mrt*np.tile(x_r1[ichannel],(h_r1[_][ichannel].shape[1],1)) # Element-wise multiplication (use broadcast)
            r_r1[_][ichannel] = np.sqrt(pt[u]/Nt) * ( h_r1[_][ichannel]@ xx ) + n # nSample, Nr, 1
            G_H = np.sqrt(Nt) / np.sum( np.abs(h_r1[_][ichannel]) )
            y_mrt_r1[_][ichannel] = G_H*r_r1[_][ichannel] # Element-wise multiplication (use broadcast)



    for path in np.arange(Nt):
        dec1 = NOMA_SIC( y[0][:,path].squeeze(), h[0], pt[u]/Nt, alpha, modem) # y[0][:,Nt] #select Nt path after equalize
        dec2 = NOMA_SIC( y[1][:,path].squeeze(), h[0], pt[u]/Nt, alpha, modem)
        
        # dec1 = NOMA_SIC(eq1, h[0], pt[u], alpha, modem) # h not use in this fcn
        # dec2 = NOMA_SIC(eq2, h[1], pt[u], alpha, modem)

        ber1[path][u] = np.sum( dec1['0'] != SPC['0'])/N
        ber2[path][u] = np.sum( dec2['1'] != SPC['1'])/N
        rem1[path] = dec1['rem0']
        rem2[path] = dec2['rem1']

        # compute abs avoid np.sqrt get -value less than 0
        # EVM = np.sqrt( ( (I-I')**2+(Q-Q')**2 ) /N )
        # EVM% = np.sqrt( ( (I-I')**2+(Q-Q')**2 ) /N )*100
        # EVM dB = 20*np.log10(EVM) 3% is ~= -30.45757490560675 dB

        EVM1[path] = np.sqrt( sum(abs( (rem1[path].real-remm1.real)**2 -(rem1[path].imag-remm1.imag)**2 )) /nSample )
        EVM2[path] = np.sqrt( sum(abs( (rem2[path].real-remm2.real)**2 -(rem2[path].imag-remm2.imag)**2 )) /nSample )
        EVM1_dB[path] = 20*np.log10(EVM1[path])
        EVM2_dB[path] = 20*np.log10(EVM2[path])

        # Channel Coding Rate = r1
        dec1 = NOMA_SIC( y_r1[0][:,path].squeeze(), h[0], pt[u]/Nt, alpha, modem) # y[0][:,Nt] #select Nt path after equalize
        dec2 = NOMA_SIC( y_r1[1][:,path].squeeze(), h[0], pt[u]/Nt, alpha, modem)
        # viterbi decoding
        decc1 = cc.viterbi_decode(dec1['0'], trellis, decoding_type='hard')[:SPC_r1['0'].size]
        decc2 = cc.viterbi_decode(dec1['1'], trellis, decoding_type='hard')[:SPC_r1['0'].size]
        ber1_r1[path][u] = np.sum( decc1 != SPC_r1['0']) / N
        ber2_r1[path][u] = np.sum( decc2 != SPC_r1['1']) / N
        rem1_r1[path] = dec1['rem0']
        rem2_r1[path] = dec2['rem1']

        EVM1_r1[path] = np.sqrt( sum(abs( (rem1_r1[path].real-remm1_r1.real)**2 -(rem1_r1[path].imag-remm1_r1.imag)**2 )) /nSample_r1 )
        EVM2_r1[path] = np.sqrt( sum(abs( (rem2_r1[path].real-remm2_r1.real)**2 -(rem2_r1[path].imag-remm2_r1.imag)**2 )) /nSample_r1 )
        EVM1_dB_r1[path] = 20*np.log10(EVM1_r1[path])
        EVM2_dB_r1[path] = 20*np.log10(EVM2_r1[path])

        if path == np.arange(Nt)[-1]:
            dec1 = NOMA_SIC( y_mrt[0].squeeze(), h[0], pt[u]/Nt, alpha, modem)
            dec2 = NOMA_SIC( y_mrt[1].squeeze(), h[0], pt[u]/Nt, alpha, modem)
            ber1_mrt[u] = np.sum( dec1['0'] != SPC['0'])/N
            ber2_mrt[u] = np.sum( dec2['1'] != SPC['1'])/N
            rem1_mrt = dec1['rem0']
            rem2_mrt = dec2['rem1']
            EVM1_mrt = np.sqrt( sum(abs( (rem1_mrt.real-remm1.real)**2 -(rem1_mrt.imag-remm1.imag)**2 )) /nSample )
            EVM2_mrt = np.sqrt( sum(abs( (rem2_mrt.real-remm2.real)**2 -(rem2_mrt.imag-remm2.imag)**2 )) /nSample )

            dec1 = NOMA_SIC( y_mrt_r1[0].squeeze(), h[0], pt[u]/Nt, alpha, modem)
            dec2 = NOMA_SIC( y_mrt_r1[1].squeeze(), h[0], pt[u]/Nt, alpha, modem)
            # viterbi decoding
            decc1 = cc.viterbi_decode(dec1['0'], trellis, decoding_type='hard')[:SPC_r1['0'].size]
            decc2 = cc.viterbi_decode(dec1['1'], trellis, decoding_type='hard')[:SPC_r1['0'].size]
            ber1_mrt_r1[u] = np.sum( decc1 != SPC_r1['0'])/N
            ber2_mrt_r1[u] = np.sum( decc2 != SPC_r1['1'])/N
            rem1_mrt_r1 = dec1['rem0']
            rem2_mrt_r1 = dec2['rem1']
            EVM1_mrt_r1 = np.sqrt( sum(abs( (rem1_mrt_r1.real-remm1_r1.real)**2 -(rem1_mrt_r1.imag-remm1_r1.imag)**2 )) /nSample_r1 )
            EVM2_mrt_r1 = np.sqrt( sum(abs( (rem2_mrt_r1.real-remm2_r1.real)**2 -(rem2_mrt_r1.imag-remm2_r1.imag)**2 )) /nSample_r1 )
        # print(EVM1)


for path in np.arange(Nr): # after equalizer have Nt path vectors
    # fig2 = plt.figure()
    plt.semilogy(Pt, ber1[path],'r-',marker='o',label='SVD-ZF 弱用戶 %dm 路徑：%d'%(d1,path+1))
    plt.semilogy(Pt, ber2[path],'g--',marker='o',label='SVD-ZF 強用戶 %dm 路徑：%d'%(d2,path+1))

    plt.semilogy(Pt, ber1_r1[path],'c-',marker='o',label='SVD-ZF R=1/2 弱用戶 %dm 路徑：%d'%(d1,path+1))
    plt.semilogy(Pt, ber2_r1[path],'y--',marker='o',label='SVD-ZF R=1/2 強用戶 %dm 路徑：%d'%(d2,path+1))

    if path == np.arange(Nr)[-1]:
        plt.semilogy(Pt, ber1_mrt,'-',marker='v',label='MRT %dx%d 弱用戶 %dm'%(Nt,Nr,d1),linewidth=2,alpha=0.6)
        plt.semilogy(Pt, ber2_mrt,'--',marker='v',label='MRT %dx%d 強用戶 %dm'%(Nt,Nr,d2),linewidth=2,alpha=0.6)

        plt.semilogy(Pt, ber1_mrt_r1,'-',marker='v',label='MRT %dx%d 弱用戶 %dm'%(Nt,Nr,d1),linewidth=2,alpha=0.6)
        plt.semilogy(Pt, ber2_mrt_r1,'--',marker='v',label='MRT %dx%d 強用戶 %dm'%(Nt,Nr,d2),linewidth=2,alpha=0.6)


plt.title('NOMA兩個使用者BER')
plt.xlabel('Transmit power (dBm)')
plt.ylabel('BER')
plt.legend()
plt.grid(True,which='both')
plt.tight_layout()

for path in np.arange(Nr): # after equalizer have Nt path vectors
    fig = plt.figure()
    plt.title('Constellation Pt = %ddB, 路徑: %d'%(Pt[u],path+1))
    plt.plot(np.real( rem1[path]), np.imag( rem1[path]  ), 'rx', label = 'Dec1, EVM=%.3f'%(EVM1[path][u]*100)+'%')
    plt.plot(np.real( rem2[path] ), np.imag( rem2[path]  ), 'g+', label = 'Dec2 EVM=%.3f'%(EVM2[path][u]*100)+'%')
    plt.xlabel(r'I')
    plt.ylabel(r'Q')
    plt.legend(loc='upper right')
    plt.grid(True,which='both')
    plt.tight_layout()

for path in np.arange(Nr): # after equalizer have Nt path vectors
    fig = plt.figure()
    plt.title('Rate = 1/2, Constellation Pt = %ddB, 路徑: %d'%(Pt[u],path+1))
    plt.plot(np.real( rem1_r1[path]), np.imag( rem1_r1[path]  ), 'rx', label = 'Dec1, EVM=%.3f'%(EVM1[path][u]*100)+'%')
    plt.plot(np.real( rem2_r1[path] ), np.imag( rem2_r1[path]  ), 'g+', label = 'Dec2 EVM=%.3f'%(EVM2[path][u]*100)+'%')
    plt.xlabel(r'I')
    plt.ylabel(r'Q')
    plt.legend(loc='upper right')
    plt.grid(True,which='both')
    plt.tight_layout()

## MRT
fig = plt.figure()
plt.title('Constellation Pt = %ddB'%(Pt[u]))
plt.plot(np.real( rem1_mrt), np.imag( rem1_mrt ), 'rx', label = 'MRC, EVM=%.3f'%(EVM1_mrt*100)+'%')
plt.plot(np.real( rem2_mrt), np.imag( rem2_mrt ), 'g+', label = 'MRC, EVM=%.3f'%(EVM2_mrt*100)+'%')
plt.xlabel(r'I')
plt.ylabel(r'Q')
plt.legend(loc='upper right')
plt.grid(True,which='both')
plt.tight_layout()

## MRT
fig = plt.figure()
plt.title('Rate = 1/2 Constellation Pt = %ddB'%(Pt[u]))
plt.plot(np.real( rem1_mrt_r1), np.imag( rem1_mrt_r1 ), 'rx', label = 'MRC, EVM=%.3f'%(EVM1_mrt_r1*100)+'%')
plt.plot(np.real( rem2_mrt_r1), np.imag( rem2_mrt_r1 ), 'g+', label = 'MRC, EVM=%.3f'%(EVM2_mrt_r1*100)+'%')
plt.xlabel(r'I')
plt.ylabel(r'Q')
plt.legend(loc='upper right')
plt.grid(True,which='both')
plt.tight_layout()

fig = plt.figure()
plt.title('Constellation SPC')
plt.plot(np.real( remm1), np.imag( remm1  ), 'rx', label = 'Dec1')
plt.plot(np.real( remm2 ), np.imag( remm2  ), 'g+', label = 'Dec2')
plt.xlabel(r'I')
plt.ylabel(r'Q')
plt.legend(loc='upper right')
plt.grid(True,which='both')
plt.tight_layout()

plt.show()

# from plotly.tools import mpl_to_plotly

# plotly_fig = mpl_to_plotly(fig)
# plotly_fig.update_layout(template="none")
# plotly_fig.write_html("constellation.html")

# plotly_fig = mpl_to_plotly(fig2)
# plotly_fig.update_layout(template="none")
# plotly_fig.write_html("BER.html")