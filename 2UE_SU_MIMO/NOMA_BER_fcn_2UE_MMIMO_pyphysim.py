import sys
sys.path[0] += '\\..' # import module from parant dir
# massive mimo not done yet
# rate and interference term, stream allocation, Block diagonal, custering... etc
# ref to https://github.com/whni/hybrid-bd-source/blob/master/massiveBD_Rayleigh.m#L43
# https://github.com/darcamo/pyphysim/blob/master/pyphysim/comm/blockdiagonalization.py
## TODO
# 1. check Ns allocation shape, and other things
# 2. check Clustering and channel shape ..etc
# 3. hybrid Beam or beam sterring fix the phase shift when using MIMO Nt>Nr

import numpy as np
from numpy.linalg import inv,pinv
import matplotlib.pyplot as plt
from ModulationPy import PSKModem, QAMModem
from functions import NOMA_SIC,NOMA_SPC,NOMA_channel,awgn,threshold_bit,PL_Hata
import scipy.linalg as la
from PL3gpp38901 import PL_RMa,PL_UMa,PL_UMi,PL_InH # load pathloss
import matplotlib
# import logging
# logging.getLogger('matplotlib.font_manager').disabled = True
plt.rcParams['axes.unicode_minus'] = False # 負號問題
plt.rcParams['mathtext.fontset'] = 'custom' 
plt.rcParams['mathtext.it'] = 'STIXGeneral:italic' 
plt.rcParams['mathtext.bf'] = 'STIXGeneral:italic:bold' 
matplotlib.rc('font', family='mingliu') # , weight='bold') # 設定新細明體
# matplotlib.rc('font', family='PingFang HK') # , weight='bold') # 設定蘋方字體

# from matplotlib.font_manager import FontManager
# fm = FontManager()
# mat_fonts = set(f.name for f in fm.ttflist)
# print (mat_fonts)

import pyphysim.comm.blockdiagonalization as bd # pip install pyphysim # slice channel is useful

#Create QPSKModulator and QPSKDemodulator objects
# modem = PSKModem(2）
modem = PSKModem(4) 
modem = QAMModem(4)
# modem.plot_const() # plot constellation
modem.soft_decision = False
# modem.demodulate( modem.modulate([0,1,1,1])).astype(int) # hard decision

N = 5*10**5
N = int(5*1e4)
N = 3000
Pt = np.arange(0,40+1,2) #Transmit power (dBm) 0:2:40
# Pt = np.arange(0,40+1,5) #Transmit power (dBm) 0:2:40
# Pt = np.arange(-40,50+1,3) #Transmit power (dBm) 0:2:40
# Pt = np.arange(0,20+1,2) #Transmit power (dBm) 0:2:40
# Pt = np.arange(0,25+1,2) #Transmit power (dBm) 0:2:40
Pt = np.linspace(0,40+1,10)
pt = (10**(-3))*10**(Pt/10)  #db2pow(Pt) #Transmit power (linear scale)
# pt = 10**(Pt/10)  #db2pow(Pt) #Transmit power (linear scale) mWatt

BW = 10**6   #Bandwidth = 1 MHz

d1 = 1000; d2 = 500	#Distances 
a1 = 0.75; a2 = 0.25   #Power allocation factors

nSample= int(N/modem.N) # qpsk/4QAM
Nt = 8
Nr = 4

# assume it have same channel different Pathloss with 2UE
Ks = 1 # cluster number
UE = 2 # user number NOMA can allocate per cluster
Ns = 1 # streams per user
K = int(UE*Ks*Ns) # total allocated column
Lr = 4 # chains per user
Lt = Lr*K # chains per user

#Do super position coding
alpha=[a1,a2]
SPC = NOMA_SPC(N, alpha, modem)
x = SPC['x']

ber1 = np.zeros((K,Nt,len(Pt)))
ber2 = np.zeros((K,Nt,len(Pt)))
ber3 = np.zeros((K,Nt,len(Pt)))
EVM1 = np.zeros((K,Nt,len(Pt)))
EVM2 = np.zeros((K,Nt,len(Pt)))
EVM1_dB = np.zeros((K,Nt,len(Pt)))
EVM2_dB = np.zeros((K,Nt,len(Pt)))
rem1 = np.zeros((K,Nt,nSample),dtype='cfloat')
rem2 = np.zeros((K,Nt,nSample),dtype='cfloat')
comb = np.zeros( (3,nSample,Nr,Nt),dtype='cfloat' )

# 2 User
r = np.zeros((2,nSample,Nr,1),dtype='cfloat') # Rx received Signal with Nr path
y = np.zeros((2,nSample,Nt,1),dtype='cfloat') # Equalized Signal with Nt path

No = -174 + 10*np.log10(BW)+7	#  Noise floor power (dBm)
no = (10**(-3))*10**(No/10) # db2pow(No) # Noise power (linear scale)
BD_obj = bd.BlockDiagonalizer(K, pt[0], no) # (num_users, bs_power, noise_var)
from functions import CalBDPrecoder,normalize_precoder

# send same channel now
H = (np.random.randn(K*Nr, Nt) + 1j*np.random.randn(K*Nr, Nt)) / np.sqrt(2)
for u in np.arange(len(Pt)):
    # for BD precoder need
    # Rs = {'K':K,'Nr':Nr,'Ns':1}
    # for calculate rate need
    # covariance matrix of power and input collections dictionary
    # PTxdBm=10*np.log10(PTx*1000) # power to dB
    Rs = { 'Rss': (pt[u]/(K*Ns)),'Rss_dB':10*np.log10((pt[u]/(K*Ns))*1000),'Rs':(pt[u]/(K*Ns))*np.eye(K*Ns),'Nt':Nt,'Nr': Nr,'Ns':Ns,'K':K,'SNR':Pt[u]} 
    
    No = -174 + 10*np.log10(BW)+7	#  Noise floor power (dBm)
    no = (10**(-3))*10**(No/10) # db2pow(No) # Noise power (linear scale)

    # input signal SPC no need h get codebook
    decxx = NOMA_SIC(np.sqrt(Rs.copy()['Rss'])*x, 1, Rs.copy()['Rss'], alpha, modem)
    remm1 = decxx['rem0']
    remm2 = decxx['rem1']
    # code_book for non-normalized signal perform ML detection
    code_ue1 = np.unique(remm1, axis=0) # remove repeat element
    code_ue2 = np.unique(remm2, axis=0)
    def ML_detect(y,code_book):
        # using non-normalized constellation as code_book
        z = [code_book[np.argmin(np.abs(code_book - x))] for x in np.nditer(y)]
        return np.array(np.asmatrix(z).reshape(y.shape))
    Antenna_gain=12
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'big')
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'small/medium')
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'suburban')
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'open')
    # PL_RMa(3.5,d1,70,1.5)
    # PL_UMa(3.5,d1,1.5)
    # PL_UMi(3.5,d1,1.5)
    # PL_InH(3.5,10)
    d = [d1,d2]
    d = np.stack([d]*Ks)
    
    # bit = 3; d = np.random.randint(10*10**bit,(1000)*10**bit+1,(Ks,2) )/(10**bit) # 10m ~1000m
    # d = np.sort(d,axis=1); d = np.flip(d,axis=1) # sort andreverse d1>d2

    PL1 = PL_RMa(3.5,d[0][0],70,1.5,condition='NLOS')
    PL2 = PL_RMa(3.5,d[0][1],70,1.5,condition='NLOS')
    G1 = np.sqrt((10**((Rs.copy()['Rss_dB']-(PL1-Antenna_gain) )/10))/2)
    G2 = np.sqrt((10**((Rs.copy()['Rss_dB']-(PL2-Antenna_gain) )/10))/2)

    for ichannel in np.arange(nSample):
        # H = (np.random.randn(K*Nr, Nt) + 1j*np.random.randn(K*Nr, Nt)) / np.sqrt(2)
        # wt,c = CalBDPrecoder(Rs,H) # all with BD combiner
        wt = pinv(H.T.conj()@H)@H.T.conj()
        F = 1/np.sqrt(Nt)*np.exp(1j*np.angle(H)).T.conj()
        wt = F@wt # aggregate precoder
        wt = normalize_precoder(wt,'fro',K*Ns)
        HH = H@wt
        # # HybridQuant(1,F) # with 1 bit Quantized hybrid beamformer 
        h1 = G1 * BD_obj._get_sub_channel(HH, [0])
        h2 = G2 * BD_obj._get_sub_channel(HH, [1])
        h = [h1,h2] # MIMO channel Nr*Nt assume each path have same pathloss with a distance
        for _ in range(2): # allocated K user, cluster = K/UE
            n = np.sqrt(no)*(np.random.randn(Nr,1) + 1j*np.random.randn(Nr,1))/np.sqrt(2)
            r[_][ichannel] = np.sqrt(Rs.copy()['Rss']) * ( h[_]@ np.tile(x[ichannel],(h[_].shape[1],1)) ) + n # nSample, Nr, 1
            # awgn additive noise channel
            # r[_][ichannel] = awgn( np.sqrt(pt[u]) * ( h[_]@ np.tile(x[ichannel],(h[_].shape[1],1)) )+ n, Pt[u]) # nSample, Nr, 1
            if Nt==Nr:
                # print('Nt==Nr')
                EqZF = inv(h[_])
            else:
                EqZF = pinv(h[_])

            # y[_] = awgn(y[_],Pt[u]) # add noise already
            # precoding matrix for channel matrix used at vector space
            wt =  h[_].T.conj() @ pinv( h[_]@h[_].T.conj() )
            # h[_] = wt@h[_]
            # wt = EqZF
            # wt = EqMMSE
            y[_][ichannel] = ( wt @ r[_][ichannel] ) # nSample, Nt, 1
            # y[_][ichannel] = ML_detect(y[_][ichannel],code_ue1)
            # modulator additive AWGN noise after equalize
            # y[_][ichannel] = awgn( wt @ r[_][ichannel],Pt[u] ) # awgn after Equalizer

    for cluster in np.arange(int(K/UE)):
        for path in [0]:#np.arange(Nt):
            dec1 = NOMA_SIC( y[0][:,path].squeeze(), h[0], Rs.copy()['Rss'], alpha, modem) # y[0][:,Nt] #select Nt path after equalize
            dec2 = NOMA_SIC( y[1][:,path].squeeze(), h[0], Rs.copy()['Rss'], alpha, modem)

            ber1[cluster][path][u] = np.sum( dec1['0'] != SPC['0'])/N
            ber2[cluster][path][u] = np.sum( dec2['1'] != SPC['1'])/N
            rem1[cluster][path] = dec1['rem0']
            rem2[cluster][path] = dec2['rem1']

            # compute abs avoid np.sqrt get -value less than 0
            # EVM = np.sqrt( ( (I-I')**2+(Q-Q')**2 ) /N )
            # EVM% = np.sqrt( ( (I-I')**2+(Q-Q')**2 ) /N )*100
            # EVM dB = 20*np.log10(EVM) 3% is ~= -30.45757490560675 dB

            EVM1[cluster][path] = np.sqrt( sum(abs( (rem1[cluster][path].real-remm1.real)**2 -(rem1[cluster][path].imag-remm1.imag)**2 )) /nSample )
            EVM2[cluster][path] = np.sqrt( sum(abs( (rem2[cluster][path].real-remm2.real)**2 -(rem2[cluster][path].imag-remm2.imag)**2 )) /nSample )
            EVM1_dB[cluster][path] = 20*np.log10(EVM1[cluster][path])
            EVM2_dB[cluster][path] = 20*np.log10(EVM2[cluster][path])

            # print(EVM1)


fig2 = plt.figure()
for cluster in np.arange(int(K/UE)):
    for path in [0]:#np.arange(Nt):
        plt.semilogy(Pt, ber1[cluster][path],'-',marker='o',label='弱用戶 %dm 路徑：%d 叢集：%d'%(d[cluster][0],path+1,cluster+1))
        plt.semilogy(Pt, ber2[cluster][path],'--',marker='o',label='強用戶 %dm 路徑：%d 叢集：%d'%(d[cluster][1],path+1,cluster+1))
        plt.title('NOMA兩個使用者BER')
        plt.xlabel('Transmit power (dBm)')
        plt.ylabel('BER')
        plt.legend()
        plt.grid(True,which='both')
        plt.tight_layout()


for cluster in np.arange(int(K/UE)):
    fig = plt.figure()
    for path in [0]:#np.arange(Nt):
        plt.title('Constellation Pt = %ddB, 路徑: %d 叢集：%d'%(Pt[u],path+1,cluster+1))
        plt.plot(np.real( rem1[cluster][path]), np.imag( rem1[cluster][path]  ), 'rx', label = 'Dec1, EVM=%.3f'%(EVM1[cluster][path][u]*100)+'%')
        plt.plot(np.real( rem2[cluster][path] ), np.imag( rem2[cluster][path]  ), 'g+', label = 'Dec2 EVM=%.3f'%(EVM2[cluster][path][u]*100)+'%')
        plt.xlabel(r'I')
        plt.ylabel(r'Q')
        plt.legend(loc='upper right')
        plt.grid(True,which='both')
        plt.tight_layout()

fig = plt.figure()
plt.title('Constellation SPC')
plt.plot(np.real( remm1), np.imag( remm1  ), 'rx', label = 'Dec1')
plt.plot(np.real( remm2 ), np.imag( remm2  ), 'g+', label = 'Dec2')
plt.xlabel(r'I')
plt.ylabel(r'Q')
plt.legend(loc='upper right')
plt.grid(True,which='both')
plt.tight_layout()

plt.show()

# from plotly.tools import mpl_to_plotly

# plotly_fig = mpl_to_plotly(fig)
# plotly_fig.update_layout(template="none")
# plotly_fig.write_html("constellation.html")

# plotly_fig = mpl_to_plotly(fig2)
# plotly_fig.update_layout(template="none")
# plotly_fig.write_html("BER.html")
