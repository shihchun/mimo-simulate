import sys
sys.path[0] += '\\..' # import module from parant dir

import numpy as np
from numpy.linalg import inv,pinv
import matplotlib.pyplot as plt
from ModulationPy import PSKModem, QAMModem
from functions import NOMA_SIC,NOMA_SPC,NOMA_channel,awgn,threshold_bit,PL_Hata
import scipy.linalg as la
from PL3gpp38901 import PL_RMa,PL_UMa,PL_UMi,PL_InH # load pathloss
from functions import MRC,ERC,SRC
import matplotlib
# import logging
# logging.getLogger('matplotlib.font_manager').disabled = True
plt.rcParams['axes.unicode_minus'] = False # 負號問題
plt.rcParams['mathtext.fontset'] = 'custom' 
plt.rcParams['mathtext.it'] = 'STIXGeneral:italic' 
plt.rcParams['mathtext.bf'] = 'STIXGeneral:italic:bold' 
matplotlib.rc('font', family='mingliu') # , weight='bold') # 設定新細明體
# matplotlib.rc('font', family='PingFang HK') # , weight='bold') # 設定蘋方字體

# from matplotlib.font_manager import FontManager
# fm = FontManager()
# mat_fonts = set(f.name for f in fm.ttflist)
# print (mat_fonts)
import matlab.engine # API for matlab python==3.7, matlab==2019a
print('=== Matlab is opening ===')
eng = matlab.engine.start_matlab()
print('=== Matlab Started ! ===')
# https://www.mathworks.com/help/comm/ref/poly2trellis.html
# trellis = eng.poly2trellis(3.0, matlab.double([6,7])) # rate = 1/2
# trellis = eng.poly2trellis(matlab.double([5,4]), matlab.double([[23,35,0],[0,5,13]])) # rate = 2/3
# trellis = eng.poly2trellis(5.0, matlab.double([37,33]),37.0) # rate = 1/2 feedback code
# data = np.random.binomial(n=1, p=0.5, size=(3004, ))
# codedData = eng.convenc(matlab.double(data.tolist()),trellis)
# codedData = np.array(codedData).astype('int').squeeze()
# codedData = matlab.int64(data.astype('int').tolist())
tbdepth = 5.0
# decodedData = eng.vitdec(codedData,trellis,tbdepth,'trunc','hard');

def NOMA_SPC_ch(N, alpha, modem, trellis):
    res = {}
    for _ in np.arange(len(alpha)): # ue amout
        temp = np.random.binomial(n=1, p=0.5, size=(N, ))
        enc = eng.convenc(matlab.double(temp.tolist()),trellis)
        enc = np.array(enc).astype('int').squeeze()
        # temp = np.random.randint(0, 1+1, N) #Generate random binary message data for the users
        xmod = modem.modulate(enc)
        res[str(_)] = temp
        if _==0:
            x = np.sqrt(alpha[_])*xmod
        else:
            x += np.sqrt(alpha[_])*xmod
    res['x']=x
    res['N'] = int(N/modem.N)
    # x = np.sqrt(a1)*xmod1 + np.sqrt(a2)*xmod2 + np.sqrt(a3)*xmod3
    return res

#Create QPSKModulator and QPSKDemodulator objects
# modem = PSKModem(2）
modem = PSKModem(4) 
modem = QAMModem(4)
# modem.plot_const() # plot constellation
modem.soft_decision = False
# modem.demodulate( modem.modulate([0,1,1,1])).astype(int) # hard decision

N = 5*10**5
# N = int(5*1e4)
N = int(4*1e6)
# N = 3000
Pt = np.arange(0,40+1,2) #Transmit power (dBm) 0:2:40
# Pt = np.arange(0,40+1,5) #Transmit power (dBm) 0:2:40
# Pt = np.arange(-40,50+1,3) #Transmit power (dBm) 0:2:40
# Pt = np.arange(0,30+1,3) #Transmit power (dBm) 0:2:40
pt = (10**(-3))*10**(Pt/10)  #db2pow(Pt) #Transmit power (linear scale)

BW = 10**6   #Bandwidth = 1 MHz

d1 = 1000; d2 = 200	#Distances 
a1 = 0.75; a2 = 0.25   #Power allocation factors

nSample= int(N/modem.N) # qpsk/4QAM
Nt = 1
Nr = 2

#Do super position coding
alpha=[a1,a2]
SPC = NOMA_SPC(N, alpha, modem)
x = SPC['x']

ber1 = np.zeros((Nt,len(Pt)))
ber2 = np.zeros((Nt,len(Pt)))
ber3 = np.zeros((Nt,len(Pt)))
EVM1 = np.zeros((Nt,len(Pt)))
EVM2 = np.zeros((Nt,len(Pt)))
EVM1_dB = np.zeros((Nt,len(Pt)))
EVM2_dB = np.zeros((Nt,len(Pt)))
rem1 = np.zeros((Nt,nSample),dtype='cfloat')
rem2 = np.zeros((Nt,nSample),dtype='cfloat')
comb = np.zeros( (3,nSample,Nr,Nt),dtype='cfloat' )

# 2 User
r = np.zeros((2,nSample,Nr,1),dtype='cfloat') # Rx received Signal with Nr path
y = np.zeros((2,nSample,Nt,1),dtype='cfloat') # Equalized Signal with Nt path

EVM1_mrc = np.zeros((len(Pt))); EVM2_mrc = np.zeros((len(Pt)))
EVM1_erc = np.zeros((len(Pt))); EVM2_erc = np.zeros((len(Pt)));
EVM1_src = np.zeros((len(Pt))); EVM2_src = np.zeros((len(Pt)))
y_mrc = np.zeros((2,nSample,1),dtype='cfloat')
ber1_mrc = np.zeros((len(Pt))); ber2_mrc = np.zeros((len(Pt)))
y_erc = np.zeros((2,nSample,1),dtype='cfloat') 
ber1_erc = np.zeros((len(Pt))); ber2_erc = np.zeros((len(Pt)))
y_src = np.zeros((2,nSample,1),dtype='cfloat') 
ber1_src = np.zeros((len(Pt))); ber2_src = np.zeros((len(Pt)))

# channel coding
r1 = 1/2
No_r1 = 10*np.log10(1/(2.0*r1)) # in qpsk, No=0 when rate = 1/2
no_r1 = 1/(2.0*r1)
g_matrix = np.array([[5, 7]]) # generator branches

# trellis_r1 = eng.poly2trellis(3.0, matlab.double([5,7])) # rate = 1/2
# trellis_r1 = eng.poly2trellis(matlab.double([5,4]), matlab.double([[23,35,0],[0,5,13]])) # rate = 2/3
trellis_r1 = eng.poly2trellis(5.0, matlab.double([37,33]),37.0) # rate = 1/2 feedback code
dfree = eng.distspec(trellis_r1,4)['dfree']
Gasy = 10*np.log10(dfree*r1)
CR = 1/2
# SPC_r1 = NOMA_SPC(N, alpha, modem)
SPC_r1 = NOMA_SPC_ch(N, alpha, modem, trellis_r1)
x_r1 = SPC_r1['x']
nSample_r1 = len(x_r1)
r_r1 = np.zeros((2,nSample_r1,Nr,1),dtype='cfloat') # Rx received Signal with Nr path
y_r1 = np.zeros((2,nSample_r1,Nt,1),dtype='cfloat') # Equalized Signal with Nt path
ber1_r1 = np.zeros((Nt,len(Pt)))
ber2_r1 = np.zeros((Nt,len(Pt)))
EVM1_r1 = np.zeros((Nt,len(Pt)))
EVM2_r1 = np.zeros((Nt,len(Pt)))
EVM1_dB_r1 = np.zeros((Nt,len(Pt)))
EVM2_dB_r1 = np.zeros((Nt,len(Pt)))
rem1_r1 = np.zeros((Nt,nSample_r1),dtype='cfloat')
rem2_r1 = np.zeros((Nt,nSample_r1),dtype='cfloat')

# SIMO combining
EVM1_mrc_r1 = np.zeros((len(Pt))); EVM2_mrc_r1 = np.zeros((len(Pt)))
EVM1_erc_r1 = np.zeros((len(Pt))); EVM2_erc_r1 = np.zeros((len(Pt)));
EVM1_src_r1 = np.zeros((len(Pt))); EVM2_src_r1 = np.zeros((len(Pt)))
y_mrc_r1 = np.zeros((2,nSample_r1,1),dtype='cfloat')
ber1_mrc_r1 = np.zeros((len(Pt))); ber2_mrc_r1 = np.zeros((len(Pt)))
y_erc_r1 = np.zeros((2,nSample_r1,1),dtype='cfloat') 
ber1_erc_r1 = np.zeros((len(Pt))); ber2_erc_r1 = np.zeros((len(Pt)))
y_src_r1 = np.zeros((2,nSample_r1,1),dtype='cfloat') 
ber1_src_r1 = np.zeros((len(Pt))); ber2_src_r1 = np.zeros((len(Pt)))

for u in np.arange(len(Pt)):
    print(Pt[u])
    Antenna_gain=12
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'big')
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'small/medium')
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'suburban')
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'open')
    # PL_RMa(3.5,d1,70,1.5)
    # PL_UMa(3.5,d1,1.5)
    # PL_UMi(3.5,d1,1.5)
    # PL_InH(3.5,10)
    PL1 = PL_RMa(3.5,d1,70,1.5,condition='NLOS')
    PL2 = PL_RMa(3.5,d2,70,1.5,condition='NLOS')
    G1 = np.sqrt((10**((Pt[u]-(PL1-Antenna_gain) )/10))/2)
    G2 = np.sqrt((10**((Pt[u]-(PL2-Antenna_gain) )/10))/2)

    No = -174 + 10*np.log10(BW)+7	#  Noise power (dBm)
    no = (10**(-3))*10**(No/10) # db2pow(No) # Noise power (linear scale)

    # input signal SPC no need h get codebook
    decxx = NOMA_SIC(np.sqrt(pt[u]/Nt)*x,[0,1,2], pt[u]/Nt, alpha, modem)
    remm1 = decxx['rem0']
    remm2 = decxx['rem1']
    # input signal SPC no need h get codebook
    decxx = NOMA_SIC(np.sqrt(pt[u]/Nt)*x_r1,[0,1,2], pt[u]/Nt, alpha, modem)
    remm1_r1 = decxx['rem0']
    remm2_r1 = decxx['rem1']
    # code_book for non-normalized signal perform ML detection
    code_ue1 = np.unique(remm1, axis=0) # remove repeat element
    code_ue2 = np.unique(remm2, axis=0)
    def ML_detect(y,code_book):
        # using non-normalized constellation as code_book
        z = [code_book[np.argmin(np.abs(code_book - x))] for x in np.nditer(y)]
        return np.array(np.asmatrix(z).reshape(y.shape))

    for ichannel in np.arange(nSample):
        hh1 = (np.random.randn(Nr,Nt) + 1j*np.random.randn(Nr,Nt))/np.sqrt(2)
        hh2 = (np.random.randn(Nr,Nt) + 1j*np.random.randn(Nr,Nt))/np.sqrt(2)
        h1 = G1*hh1
        h2 = G2*hh2
        h = [h1,h2] # MIMO channel Nr*Nt assume each path have same pathloss with a distance
        d = [d1,d2]
        for _ in range(2):
            n = np.sqrt(no)*(np.random.randn(Nr,1) + 1j*np.random.randn(Nr,1))/np.sqrt(2)
            r[_][ichannel] = np.sqrt(pt[u]/Nt) * ( h[_]@np.tile(x[ichannel],(h[_].shape[1],1)) ) + n # nSample, Nr, 1
            wt =  h[_].T.conj() @ pinv( h[_]@h[_].T.conj()+no/pt[u])
            # Cx = (pt[u]/no)*np.identity(Nt) # all antennas receiving same data np.var(x)
            # Cz = np.identity(Nr) # Covariance matrix of noise. Currently assuming uncorrelated across antennas.
            # if Nt==Nr:
            #     # print('Nt==Nr')
            #     Hh = h[_].conj().T
            #     tM = Hh@(inv(Cz)@h[_]) + inv(Cx)
            #     EqMMSE = inv(tM)@Hh
            #     EqZF = inv(h[_])
            # else:
            #     Hh = h[_].conj().T
            #     tM = Hh@(pinv(Cz)@h[_]) + pinv(Cx)
            #     EqMMSE = pinv(tM)@Hh
            #     EqZF = pinv(h[_])
            # wt = EqZF
            y[_][ichannel] = ( wt @ r[_][ichannel] ) # nSample, Nt, 1
            # y[_][ichannel] = ML_detect(y[_][ichannel],code_ue1)

            y_mrc[_][ichannel] = np.sum((h[_].T.conj()@ r[_][ichannel])/(h[_].T.conj()@h[_]))
            # y_mrc[_][ichannel] = MRC(r[_][ichannel],h[_])
            y_erc[_][ichannel] = ERC(r[_][ichannel],h[_])
            y_src[_][ichannel] = SRC(r[_][ichannel],h[_])

    G1 = np.sqrt((10**((Pt[u]-(PL1-Antenna_gain-Gasy) )/10))/2)
    G2 = np.sqrt((10**((Pt[u]-(PL2-Antenna_gain-Gasy) )/10))/2)
    for ichannel in np.arange(nSample_r1):
        hh1_r1 = (np.random.randn(Nr,Nt) + 1j*np.random.randn(Nr,Nt))/np.sqrt(2)
        hh2_r1 = (np.random.randn(Nr,Nt) + 1j*np.random.randn(Nr,Nt))/np.sqrt(2)
        h_r1 = [G1*hh1_r1, G2*hh2_r1] # share same pathloss
        d = [d1,d2]
        for _ in range(2):
            n = np.sqrt(no)*(np.random.randn(Nr,1) + 1j*np.random.randn(Nr,1))/np.sqrt(2)
            r_r1[_][ichannel] = np.sqrt(pt[u]/Nt) * ( h_r1[_]@np.tile(x_r1[ichannel],(h_r1[_].shape[1],1)) ) + n # nSample, Nr, 1
            wt =  h_r1[_].T.conj() @ pinv( h_r1[_]@h_r1[_].T.conj()+no/pt[u])
            # Cx = (pt[u]/no)*np.identity(Nt) # all antennas receiving same data np.var(x)
            # Cz = np.identity(Nr) # Covariance matrix of noise. Currently assuming uncorrelated across antennas.
            # if Nt==Nr:
            #     # print('Nt==Nr')
            #     Hh = h_r1[_].conj().T
            #     tM = Hh@(inv(Cz)@h_r1[_]) + inv(Cx)
            #     EqMMSE = inv(tM)@Hh
            #     EqZF = inv(h_r1[_])
            # else:
            #     Hh = h_r1[_].conj().T
            #     tM = Hh@(pinv(Cz)@h_r1[_]) + pinv(Cx)
            #     EqMMSE = pinv(tM)@Hh
            #     EqZF = pinv(h_r1[_])
            # wt = EqZF
            y_r1[_][ichannel] = ( wt @ r_r1[_][ichannel] ) # nSample, Nt, 1

            y_mrc_r1[_][ichannel] = np.sum((h_r1[_].T.conj()@ r_r1[_][ichannel])/(h_r1[_].T.conj()@h_r1[_]))
            # y_mrc_r1[_][ichannel] = MRC(r_r1[_][ichannel],h_r1[_])
            y_erc_r1[_][ichannel] = ERC(r_r1[_][ichannel],h_r1[_])
            y_src_r1[_][ichannel] = SRC(r_r1[_][ichannel],h_r1[_])

    for path in np.arange(Nt):
        dec1 = NOMA_SIC( y[0][:,path].squeeze(),[0,1,2], pt[u]/Nt, alpha, modem) # y[0][:,Nt] #select Nt path after equalize
        dec2 = NOMA_SIC( y[1][:,path].squeeze(),[0,1,2], pt[u]/Nt, alpha, modem)

        ber1[path][u] = np.sum( dec1['0'] != SPC['0'])/N
        ber2[path][u] = np.sum( dec2['1'] != SPC['1'])/N
        rem1[path] = dec1['rem0']
        rem2[path] = dec2['rem1']

        EVM1[path] = np.sqrt( sum(abs( (rem1[path].real-remm1.real)**2 -(rem1[path].imag-remm1.imag)**2 )) /nSample )
        EVM2[path] = np.sqrt( sum(abs( (rem2[path].real-remm2.real)**2 -(rem2[path].imag-remm2.imag)**2 )) /nSample )
        EVM1_dB[path] = 20*np.log10(EVM1[path])
        EVM2_dB[path] = 20*np.log10(EVM2[path])

        # Channel Coding Rate = r1
        dec1 = NOMA_SIC( y_r1[0][:,path].squeeze(),[0,1,2], pt[u]/Nt, alpha, modem) # y[0][:,Nt] #select Nt path after equalize
        dec2 = NOMA_SIC( y_r1[1][:,path].squeeze(),[0,1,2], pt[u]/Nt, alpha, modem)
        # viterbi decoding
        decc1 = matlab.int64(dec1['0'].astype('int').tolist())
        decc1 = eng.vitdec(decc1,trellis_r1,tbdepth,'trunc','hard')
        decc1 = np.array(decc1).astype('int').squeeze()

        decc2 = matlab.int64(dec2['1'].astype('int').tolist())
        decc2 = eng.vitdec(decc2,trellis_r1,tbdepth,'trunc','hard')
        decc2 = np.array(decc2).astype('int').squeeze()

        ber1_r1[path][u] = np.sum( decc1 != SPC_r1['0']) / N
        ber2_r1[path][u] = np.sum( decc2 != SPC_r1['1']) / N
        rem1_r1[path] = dec1['rem0']
        rem2_r1[path] = dec2['rem1']

        EVM1_r1[path] = np.sqrt( sum(abs( (rem1_r1[path].real-remm1_r1.real)**2 -(rem1_r1[path].imag-remm1_r1.imag)**2 )) /nSample_r1 )
        EVM2_r1[path] = np.sqrt( sum(abs( (rem2_r1[path].real-remm2_r1.real)**2 -(rem2_r1[path].imag-remm2_r1.imag)**2 )) /nSample_r1 )
        EVM1_dB_r1[path] = 20*np.log10(EVM1_r1[path])
        EVM2_dB_r1[path] = 20*np.log10(EVM2_r1[path])

        if path == np.arange(Nt)[-1]:
            ###   MRC  ###
            dec1 = NOMA_SIC( y_mrc[0].squeeze(),[0,1,2], pt[u]/Nt, alpha, modem)
            dec2 = NOMA_SIC( y_mrc[1].squeeze(),[0,1,2], pt[u]/Nt, alpha, modem)
            ber1_mrc[u] = np.sum( dec1['0'] != SPC['0'])/N
            ber2_mrc[u] = np.sum( dec2['1'] != SPC['1'])/N
            rem1_mrc = dec1['rem0']
            rem2_mrc = dec2['rem1']
            EVM1_mrc = np.sqrt( sum(abs( (rem1_mrc.real-remm1.real)**2 -(rem1_mrc.imag-remm1.imag)**2 )) /nSample )
            EVM2_mrc = np.sqrt( sum(abs( (rem2_mrc.real-remm1.real)**2 -(rem2_mrc.imag-remm1.imag)**2 )) /nSample )

            dec1 = NOMA_SIC( y_mrc_r1[0].squeeze(),[0,1,2], pt[u]/Nt, alpha, modem)
            dec2 = NOMA_SIC( y_mrc_r1[1].squeeze(),[0,1,2], pt[u]/Nt, alpha, modem)
            # viterbi decoding
            decc1 = matlab.int64(dec1['0'].astype('int').tolist())
            decc1 = eng.vitdec(decc1,trellis_r1,tbdepth,'trunc','hard')
            decc1 = np.array(decc1).astype('int').squeeze()

            decc2 = matlab.int64(dec2['1'].astype('int').tolist())
            decc2 = eng.vitdec(decc2,trellis_r1,tbdepth,'trunc','hard')
            decc2 = np.array(decc2).astype('int').squeeze()

            ber1_mrc_r1[u] = np.sum( decc1 != SPC_r1['0'])/N
            ber2_mrc_r1[u] = np.sum( decc2 != SPC_r1['1'])/N
            rem1_mrc_r1 = dec1['rem0']
            rem2_mrc_r1 = dec2['rem1']
            EVM1_mrc_r1 = np.sqrt( sum(abs( (rem1_mrc_r1.real-remm1_r1.real)**2 -(rem1_mrc_r1.imag-remm1_r1.imag)**2 )) /nSample_r1 )
            EVM2_mrc_r1 = np.sqrt( sum(abs( (rem2_mrc_r1.real-remm1_r1.real)**2 -(rem2_mrc_r1.imag-remm1_r1.imag)**2 )) /nSample_r1 )

            dec1 = NOMA_SIC( y_erc[0].squeeze(),[0,1,2], pt[u]/Nt, alpha, modem)
            dec2 = NOMA_SIC( y_erc[1].squeeze(),[0,1,2], pt[u]/Nt, alpha, modem)
            ber1_erc[u] = np.sum( dec1['0'] != SPC['0'])/N
            ber2_erc[u] = np.sum( dec2['1'] != SPC['1'])/N
            rem1_erc = dec1['rem0']
            rem2_erc = dec2['rem1']
            EVM1_erc = np.sqrt( sum(abs( (rem1_erc.real-remm1.real)**2 -(rem1_erc.imag-remm1.imag)**2 )) /nSample )
            EVM2_erc = np.sqrt( sum(abs( (rem2_erc.real-remm1.real)**2 -(rem2_erc.imag-remm1.imag)**2 )) /nSample )

            ###   ERC  ###
            dec1 = NOMA_SIC( y_erc[0].squeeze(),[0,1,2], pt[u]/Nt, alpha, modem)
            dec2 = NOMA_SIC( y_erc[1].squeeze(),[0,1,2], pt[u]/Nt, alpha, modem)
            ber1_erc[u] = np.sum( dec1['0'] != SPC['0'])/N
            ber2_erc[u] = np.sum( dec2['1'] != SPC['1'])/N
            rem1_erc = dec1['rem0']
            rem2_erc = dec2['rem1']
            EVM1_erc = np.sqrt( sum(abs( (rem1_erc.real-remm1.real)**2 -(rem1_erc.imag-remm1.imag)**2 )) /nSample )
            EVM2_erc = np.sqrt( sum(abs( (rem2_erc.real-remm1.real)**2 -(rem2_erc.imag-remm1.imag)**2 )) /nSample )

            dec1 = NOMA_SIC( y_erc_r1[0].squeeze(),[0,1,2], pt[u]/Nt, alpha, modem)
            dec2 = NOMA_SIC( y_erc_r1[1].squeeze(),[0,1,2], pt[u]/Nt, alpha, modem)
            # viterbi decoding
            decc1 = matlab.int64(dec1['0'].astype('int').tolist())
            decc1 = eng.vitdec(decc1,trellis_r1,tbdepth,'trunc','hard')
            decc1 = np.array(decc1).astype('int').squeeze()

            decc2 = matlab.int64(dec2['1'].astype('int').tolist())
            decc2 = eng.vitdec(decc2,trellis_r1,tbdepth,'trunc','hard')
            decc2 = np.array(decc2).astype('int').squeeze()

            ber1_erc_r1[u] = np.sum( decc1 != SPC_r1['0'])/N
            ber2_erc_r1[u] = np.sum( decc2 != SPC_r1['1'])/N
            rem1_erc_r1 = dec1['rem0']
            rem2_erc_r1 = dec2['rem1']
            EVM1_erc_r1 = np.sqrt( sum(abs( (rem1_erc_r1.real-remm1_r1.real)**2 -(rem1_erc_r1.imag-remm1_r1.imag)**2 )) /nSample_r1 )
            EVM2_erc_r1 = np.sqrt( sum(abs( (rem2_erc_r1.real-remm1_r1.real)**2 -(rem2_erc_r1.imag-remm1_r1.imag)**2 )) /nSample_r1 )

            dec1 = NOMA_SIC( y_erc[0].squeeze(),[0,1,2], pt[u]/Nt, alpha, modem)
            dec2 = NOMA_SIC( y_erc[1].squeeze(),[0,1,2], pt[u]/Nt, alpha, modem)
            ber1_erc[u] = np.sum( dec1['0'] != SPC['0'])/N
            ber2_erc[u] = np.sum( dec2['1'] != SPC['1'])/N
            rem1_erc = dec1['rem0']
            rem2_erc = dec2['rem1']
            EVM1_erc = np.sqrt( sum(abs( (rem1_erc.real-remm1.real)**2 -(rem1_erc.imag-remm1.imag)**2 )) /nSample )
            EVM2_erc = np.sqrt( sum(abs( (rem2_erc.real-remm2.real)**2 -(rem2_erc.imag-remm2.imag)**2 )) /nSample )
            ###   SRC  ###
            dec1 = NOMA_SIC( y_src[0].squeeze(),[0,1,2], pt[u]/Nt, alpha, modem)
            dec2 = NOMA_SIC( y_src[1].squeeze(),[0,1,2], pt[u]/Nt, alpha, modem)
            ber1_src[u] = np.sum( dec1['0'] != SPC['0'])/N
            ber2_src[u] = np.sum( dec2['1'] != SPC['1'])/N
            rem1_src = dec1['rem0']
            rem2_src = dec2['rem1']
            EVM1_src = np.sqrt( sum(abs( (rem1_src.real-remm1.real)**2 -(rem1_src.imag-remm1.imag)**2 )) /nSample )
            EVM2_src = np.sqrt( sum(abs( (rem2_src.real-remm2.real)**2 -(rem2_src.imag-remm2.imag)**2 )) /nSample )

            dec1 = NOMA_SIC( y_src_r1[0].squeeze(),[0,1,2], pt[u]/Nt, alpha, modem)
            dec2 = NOMA_SIC( y_src_r1[1].squeeze(),[0,1,2], pt[u]/Nt, alpha, modem)
            # viterbi decoding
            decc1 = matlab.int64(dec1['0'].astype('int').tolist())
            decc1 = eng.vitdec(decc1,trellis_r1,tbdepth,'trunc','hard')
            decc1 = np.array(decc1).astype('int').squeeze()

            decc2 = matlab.int64(dec2['1'].astype('int').tolist())
            decc2 = eng.vitdec(decc2,trellis_r1,tbdepth,'trunc','hard')
            decc2 = np.array(decc2).astype('int').squeeze()

            ber1_src_r1[u] = np.sum( decc1 != SPC_r1['0'])/N
            ber2_src_r1[u] = np.sum( decc2 != SPC_r1['1'])/N
            rem1_src_r1 = dec1['rem0']
            rem2_src_r1 = dec2['rem1']
            EVM1_src_r1 = np.sqrt( sum(abs( (rem1_src_r1.real-remm1_r1.real)**2 -(rem1_src_r1.imag-remm1_r1.imag)**2 )) /nSample_r1 )
            EVM2_src_r1 = np.sqrt( sum(abs( (rem2_src_r1.real-remm2_r1.real)**2 -(rem2_src_r1.imag-remm2_r1.imag)**2 )) /nSample_r1 )

            dec1 = NOMA_SIC( y_src[0].squeeze(),[0,1,2], pt[u]/Nt, alpha, modem)
            dec2 = NOMA_SIC( y_src[1].squeeze(),[0,1,2], pt[u]/Nt, alpha, modem)
            ber1_src[u] = np.sum( dec1['0'] != SPC['0'])/N
            ber2_src[u] = np.sum( dec2['1'] != SPC['1'])/N
            rem1_src = dec1['rem0']
            rem2_src = dec2['rem1']
            EVM1_src = np.sqrt( sum(abs( (rem1_src.real-remm1.real)**2 -(rem1_src.imag-remm1.imag)**2 )) /nSample )
            EVM2_src = np.sqrt( sum(abs( (rem2_src.real-remm2.real)**2 -(rem2_src.imag-remm2.imag)**2 )) /nSample )


for path in np.arange(Nt): # after equalizer have Nt path vectors
    # fig2 = plt.figure()
    plt.semilogy(Pt, ber1[path],'r-',marker='o',label='MMSE 弱用戶 %dm 路徑：%d'%(d1,path+1))
    plt.semilogy(Pt, ber2[path],'g--',marker='o',label='MMSE 強用戶 %dm 路徑：%d'%(d2,path+1))

    plt.semilogy(Pt, ber1_r1[path],'b-',marker='o',label='MMSE R=%.3f 弱用戶 %dm 路徑：%d'%(CR,d1,path+1),markerfacecolor='none')
    plt.semilogy(Pt, ber2_r1[path],'k--',marker='o',label='MMSE R=%.3f 強用戶 %dm 路徑：%d'%(CR,d2,path+1),markerfacecolor='none')

    if path == np.arange(Nt)[-1]:
        plt.semilogy(Pt, ber1_mrc,'r-',marker='v',label='MRC %dx%d 弱用戶 %dm'%(Nt,Nr,d1))
        plt.semilogy(Pt, ber2_mrc,'g--',marker='v',label='MRC %dx%d 強用戶 %dm'%(Nt,Nr,d2))

        plt.semilogy(Pt, ber1_mrc_r1,'b-',marker='v',label='MRC R=%.3f %dx%d 弱用戶 %dm'%(CR,Nt,Nr,d1),markerfacecolor='none')
        plt.semilogy(Pt, ber2_mrc_r1,'k--',marker='v',label='MRC R=%.3f %dx%d 強用戶 %dm'%(CR,Nt,Nr,d2),markerfacecolor='none')

        plt.semilogy(Pt, ber1_erc,'r-',marker='s',label='erc %dx%d 弱用戶 %dm'%(Nt,Nr,d1))
        plt.semilogy(Pt, ber2_erc,'g--',marker='s',label='erc %dx%d 強用戶 %dm'%(Nt,Nr,d2))

        plt.semilogy(Pt, ber1_erc_r1,'b-',marker='s',label='erc R=%.3f %dx%d 弱用戶 %dm'%(CR,Nt,Nr,d1),markerfacecolor='none')
        plt.semilogy(Pt, ber2_erc_r1,'k--',marker='s',label='erc R=%.3f %dx%d 強用戶 %dm'%(CR,Nt,Nr,d2),markerfacecolor='none')

        plt.semilogy(Pt, ber1_src,'r-',marker='^',label='src %dx%d 弱用戶 %dm'%(Nt,Nr,d1))
        plt.semilogy(Pt, ber2_src,'g--',marker='^',label='src %dx%d 強用戶 %dm'%(Nt,Nr,d2))

        plt.semilogy(Pt, ber1_src_r1,'b-',marker='^',label='src R=%.3f %dx%d 弱用戶 %dm'%(CR,Nt,Nr,d1),markerfacecolor='none')
        plt.semilogy(Pt, ber2_src_r1,'k--',marker='^',label='src R=%.3f %dx%d 強用戶 %dm'%(CR,Nt,Nr,d2),markerfacecolor='none')

plt.title('NOMA兩個使用者BER')
plt.xlabel('Transmit power (dBm)')
plt.ylabel('BER')
plt.legend()
plt.grid(True,which='both')
plt.tight_layout()

# for path in np.arange(Nt): # after equalizer have Nt path vectors
#     fig = plt.figure()
#     plt.title('Constellation Pt = %ddB, 路徑: %d'%(Pt[u],path+1))
#     plt.plot(np.real( rem1[path]), np.imag( rem1[path]  ), 'ro', label = 'Dec1, EVM=%.3f'%(EVM1[path][u]*100)+'%',markerfacecolor='none')
#     plt.plot(np.real( rem2[path] ), np.imag( rem2[path]  ), 'gv', label = 'Dec2 EVM=%.3f'%(EVM2[path][u]*100)+'%',markerfacecolor='none')
#     plt.xlabel(r'I')
#     plt.ylabel(r'Q')
#     plt.legend(loc='upper right')
#     plt.grid(True,which='both')
#     plt.tight_layout()

# for path in np.arange(Nt): # after equalizer have Nt path vectors
#     fig = plt.figure()
#     plt.title('Rate = %.3f, Constellation Pt = %ddB, 路徑: %d'%(CR,Pt[u],path+1))
#     plt.plot(np.real( rem1_r1[path]), np.imag( rem1_r1[path]  ), 'ro', label = 'Dec1, EVM=%.3f'%(EVM1[path][u]*100)+'%',markerfacecolor='none')
#     plt.plot(np.real( rem2_r1[path] ), np.imag( rem2_r1[path]  ), 'gv', label = 'Dec2 EVM=%.3f'%(EVM2[path][u]*100)+'%',markerfacecolor='none')
#     plt.xlabel(r'I')
#     plt.ylabel(r'Q')
#     plt.legend(loc='upper right')
#     plt.grid(True,which='both')
#     plt.tight_layout()

# ##  MRC ###
# fig = plt.figure()
# plt.title('Constellation Pt = %ddB'%(Pt[u]))
# plt.plot(np.real( rem1_mrc), np.imag( rem1_mrc ), 'ro', label = 'MRC, EVM=%.3f'%(EVM1_mrc*100)+'%',markerfacecolor='none')
# plt.plot(np.real( rem2_mrc), np.imag( rem2_mrc ), 'gv', label = 'MRC, EVM=%.3f'%(EVM2_mrc*100)+'%',markerfacecolor='none')
# plt.xlabel(r'I')
# plt.ylabel(r'Q')
# plt.legend(loc='upper right')
# plt.grid(True,which='both')
# plt.tight_layout()

# fig = plt.figure()
# plt.title('Rate = %.3f, Constellation Pt = %ddB'%(CR,Pt[u]))
# plt.plot(np.real( rem1_mrc_r1), np.imag( rem1_mrc_r1 ), 'ro', label = 'MRC, EVM=%.3f'%(EVM1_mrc_r1*100)+'%',markerfacecolor='none')
# plt.plot(np.real( rem2_mrc_r1), np.imag( rem2_mrc_r1 ), 'gv', label = 'MRC, EVM=%.3f'%(EVM2_mrc_r1*100)+'%',markerfacecolor='none')
# plt.xlabel(r'I')
# plt.ylabel(r'Q')
# plt.legend(loc='upper right')
# plt.grid(True,which='both')
# plt.tight_layout()
# ##  ERC ###
# fig = plt.figure()
# plt.title('Constellation Pt = %ddB'%(Pt[u]))
# plt.plot(np.real( rem1_erc), np.imag( rem1_erc ), 'ro', label = 'ERC, EVM=%.3f'%(EVM1_erc*100)+'%',markerfacecolor='none')
# plt.plot(np.real( rem2_erc), np.imag( rem2_erc ), 'gv', label = 'ERC, EVM=%.3f'%(EVM2_erc*100)+'%',markerfacecolor='none')
# plt.xlabel(r'I')
# plt.ylabel(r'Q')
# plt.legend(loc='upper right')
# plt.grid(True,which='both')
# plt.tight_layout()

# fig = plt.figure()
# plt.title('Rate = %.3f, Constellation Pt = %ddB'%(CR,Pt[u]))
# plt.plot(np.real( rem1_erc_r1), np.imag( rem1_erc_r1 ), 'ro', label = 'ERC, EVM=%.3f'%(EVM1_erc_r1*100)+'%',markerfacecolor='none')
# plt.plot(np.real( rem2_erc_r1), np.imag( rem2_erc_r1 ), 'gv', label = 'ERC, EVM=%.3f'%(EVM2_erc_r1*100)+'%',markerfacecolor='none')
# plt.xlabel(r'I')
# plt.ylabel(r'Q')
# plt.legend(loc='upper right')
# plt.grid(True,which='both')
# plt.tight_layout()
# ##  SRC ###
# fig = plt.figure()
# plt.title('Constellation Pt = %ddB'%(Pt[u]))
# plt.plot(np.real( rem1_src), np.imag( rem1_src ), 'ro', label = 'SRC, EVM=%.3f'%(EVM1_src*100)+'%',markerfacecolor='none')
# plt.plot(np.real( rem2_src), np.imag( rem2_src ), 'gv', label = 'SRC, EVM=%.3f'%(EVM2_src*100)+'%',markerfacecolor='none')
# plt.xlabel(r'I')
# plt.ylabel(r'Q')
# plt.legend(loc='upper right')
# plt.grid(True,which='both')
# plt.tight_layout()

# fig = plt.figure()
# plt.title('Rate = %.3f, Constellation Pt = %ddB'%(CR,Pt[u]))
# plt.plot(np.real( rem1_src_r1), np.imag( rem1_src_r1 ), 'ro', label = 'SRC, EVM=%.3f'%(EVM1_src_r1*100)+'%',markerfacecolor='none')
# plt.plot(np.real( rem2_src_r1), np.imag( rem2_src_r1 ), 'gv', label = 'SRC, EVM=%.3f'%(EVM2_src_r1*100)+'%',markerfacecolor='none')
# plt.xlabel(r'I')
# plt.ylabel(r'Q')
# plt.legend(loc='upper right')
# plt.grid(True,which='both')
# plt.tight_layout()

# # codebook
# fig = plt.figure()
# plt.title('Constellation SPC')
# plt.plot(np.real( remm1), np.imag( remm1  ), 'ro', label = 'Dec1',markerfacecolor='none')
# plt.plot(np.real( remm2 ), np.imag( remm2  ), 'gv', label = 'Dec2',markerfacecolor='none')
# plt.xlabel(r'I')
# plt.ylabel(r'Q')
# plt.legend(loc='upper right')
# plt.grid(True,which='both')
# plt.tight_layout()

plt.show()

# from plotly.tools import mpl_to_plotly

# plotly_fig = mpl_to_plotly(fig)
# plotly_fig.update_layout(template="none")
# plotly_fig.write_html("constellation.html")

# plotly_fig = mpl_to_plotly(fig2)
# plotly_fig.update_layout(template="none")
# plotly_fig.write_html("BER.html")