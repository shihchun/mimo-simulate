# closed loop MIMO
import sys
sys.path[0] += '\\..' # import module from parant dir

import numpy as np
from numpy.linalg import inv,pinv
import matplotlib.pyplot as plt
from ModulationPy import PSKModem, QAMModem
from functions import NOMA_SIC,NOMA_SPC,NOMA_channel,awgn,threshold_bit,PL_Hata,gmd
import scipy.linalg as la
from PL3gpp38901 import PL_RMa,PL_UMa,PL_UMi,PL_InH # load pathloss
import matplotlib
# import logging
# logging.getLogger('matplotlib.font_manager').disabled = True
plt.rcParams['axes.unicode_minus'] = False # 負號問題
plt.rcParams['mathtext.fontset'] = 'custom' 
plt.rcParams['mathtext.it'] = 'STIXGeneral:italic' 
plt.rcParams['mathtext.bf'] = 'STIXGeneral:italic:bold' 
matplotlib.rc('font', family='mingliu') # , weight='bold') # 設定新細明體
# matplotlib.rc('font', family='PingFang HK') # , weight='bold') # 設定蘋方字體

# from matplotlib.font_manager import FontManager
# fm = FontManager()
# mat_fonts = set(f.name for f in fm.ttflist)
# print (mat_fonts)

#Create QPSKModulator and QPSKDemodulator objects
# modem = PSKModem(2）
modem = PSKModem(4) 
modem = QAMModem(4)
# modem.plot_const() # plot constellation
modem.soft_decision = False
# modem.demodulate( modem.modulate([0,1,1,1])).astype(int) # hard decision

N = 5*10**5
N = int(2*1e6)
# N = 30
Pt = np.arange(0,40+1,2) #Transmit power (dBm) 0:2:40
# Pt = np.arange(0,40+1,5) #Transmit power (dBm) 0:2:40
# Pt = np.arange(-40,50+1,3) #Transmit power (dBm) 0:2:40
# Pt = np.arange(0,20+1,2) #Transmit power (dBm) 0:2:40
pt = (10**(-3))*10**(Pt/10)  #db2pow(Pt) #Transmit power (linear scale)

BW = 10**6   #Bandwidth = 1 MHz

d1 = 1000; d2 = 500	#Distances 
a1 = 0.75; a2 = 0.25   #Power allocation factors

nSample= int(N/modem.N) # qpsk/4QAM
Nt = 4
Nr = 2

#Do super position coding
alpha=[a1,a2]
SPC = NOMA_SPC(N, alpha, modem)
x = SPC['x']

ber1 = np.zeros((Nt,len(Pt)))
ber2 = np.zeros((Nt,len(Pt)))
ber3 = np.zeros((Nt,len(Pt)))
EVM1 = np.zeros((Nt,len(Pt)))
EVM2 = np.zeros((Nt,len(Pt)))
EVM1_dB = np.zeros((Nt,len(Pt)))
EVM2_dB = np.zeros((Nt,len(Pt)))
rem1 = np.zeros((Nt,nSample),dtype='cfloat')
rem2 = np.zeros((Nt,nSample),dtype='cfloat')
comb = np.zeros( (3,nSample,Nr,Nt),dtype='cfloat' )

# 2 User
r = np.zeros((2,nSample,Nr,1),dtype='cfloat') # Rx received Signal with Nr path
y = np.zeros((2,nSample,Nt,1),dtype='cfloat') # Equalized Signal with Nt path

def KLT(a):
    val,vec = np.linalg.eig(np.cov(a))
    klt = np.dot(vec,a)
    return klt,vec,val

for u in np.arange(len(Pt)):
    print(Pt[u])
    Antenna_gain=12
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'big')
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'small/medium')
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'suburban')
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'open')
    # PL_RMa(3.5,d1,70,1.5)
    # PL_UMa(3.5,d1,1.5)
    # PL_UMi(3.5,d1,1.5)
    # PL_InH(3.5,10)
    PL1 = PL_RMa(3.5,d1,70,1.5,condition='NLOS')
    PL2 = PL_RMa(3.5,d2,70,1.5,condition='NLOS')
    G1 = np.sqrt((10**((Pt[u]-(PL1-Antenna_gain) )/10))/2)
    G2 = np.sqrt((10**((Pt[u]-(PL2-Antenna_gain) )/10))/2)

    No = -174 + 10*np.log10(BW)+7	#  Noise power (dBm)
    no = (10**(-3))*10**(No/10) # db2pow(No) # Noise power (linear scale)

    # input signal SPC no need h get codebook
    decxx = NOMA_SIC(np.sqrt(pt[u]/Nt)*x, [0,1,2], pt[u]/Nt, alpha, modem)
    remm1 = decxx['rem0']
    remm2 = decxx['rem1']
    # code_book for non-normalized signal perform ML detection
    code_ue1 = np.unique(remm1, axis=0) # remove repeat element
    code_ue2 = np.unique(remm2, axis=0)
    def ML_detect(y,code_book):
        # using non-normalized constellation as code_book
        z = [code_book[np.argmin(np.abs(code_book - x))] for x in np.nditer(y)]
        return np.array(np.asmatrix(z).reshape(y.shape))

    for ichannel in np.arange(nSample):
        hh1 = (np.random.randn(Nr,Nt) + 1j*np.random.randn(Nr,Nt))/np.sqrt(2)
        hh2 = (np.random.randn(Nr,Nt) + 1j*np.random.randn(Nr,Nt))/np.sqrt(2)
        h1 = G1*hh1
        h2 = G2*hh2
        h = [h1,h2] # MIMO channel Nr*Nt assume each path have same pathloss with a distance
        # y = xh+n
        # y = NOMA_channel(x, h, pt[u],BW,-174) # Channel with noise
        
        d = [d1,d2]
        for _ in range(2):
            # MTRC sqrt(P)* hh@hh.T +n --> STBC Alamouti precoder and combiner
            # SVD & GMD MIMO signal precoding
            # https://github.com/darcamo/pyphysim/blob/master/pyphysim/mimo/mimo.py
            ### SVD
            # U, S, V_H = np.linalg.svd(channel)
            # W = V_H.conj().T / math.sqrt(Nt)
            # G_H = np.diag(1. / S).dot(U.conj().T) * np.sqrt(Nt)
            ### GMD
            # Q, R, P = gmd(U, S, V_H)
            # W = P / np.sqrt(Nt)
            # G_H = channel_eq = Q.dot(R)

            ### SVD
            U, S, V_H = np.linalg.svd(h[_])
            W = V_H.conj().T / np.sqrt(Nt)
            # G_H = np.diag(1. / S).dot(U.conj().T) * np.sqrt(Nt)

            n = np.sqrt(no)*(np.random.randn(Nr,1) + 1j*np.random.randn(Nr,1))/np.sqrt(2)
            r[_][ichannel] = np.sqrt(pt[u]/Nt) * ( h[_]@ W@np.tile(x[ichannel],(h[_].shape[1],1)) ) + n # nSample, Nr, 1
            wt =  (h[_]@W).T.conj() @ pinv( h[_]@W@(h[_]@W).T.conj() )
            if Nt==Nr:
                if Pt[u] == Pt[-1]:
                    print("use G_H")
                G_H = np.diag(1. / S).dot(U.conj().T) * np.sqrt(Nt)
                y[_][ichannel] = (G_H @ r[_][ichannel] ) # nSample, Nt, 1
            else:
                y[_][ichannel] = (wt @ r[_][ichannel] ) # nSample, Nt, 1
    
    for path in np.arange(Nt):
        dec1 = NOMA_SIC( y[0][:,path].squeeze(), [0,1,2], pt[u]/Nt, alpha, modem) # y[0][:,Nt] #select Nt path after equalize
        dec2 = NOMA_SIC( y[1][:,path].squeeze(), [0,1,2], pt[u]/Nt, alpha, modem)

        ber1[path][u] = np.sum( dec1['0'] != SPC['0'])/N
        ber2[path][u] = np.sum( dec2['1'] != SPC['1'])/N
        rem1[path] = dec1['rem0']
        rem2[path] = dec2['rem1']

        EVM1[path] = np.sqrt( sum(abs( (rem1[path].real-remm1.real)**2 -(rem1[path].imag-remm1.imag)**2 )) /nSample )
        EVM2[path] = np.sqrt( sum(abs( (rem2[path].real-remm2.real)**2 -(rem2[path].imag-remm2.imag)**2 )) /nSample )
        EVM1_dB[path] = 20*np.log10(EVM1[path])
        EVM2_dB[path] = 20*np.log10(EVM2[path])

marks= ['o','v','>','s','<','*','d','p']
markes_face = ['full','none','right','left','top','bottom']
for path in np.arange(Nt):
    ind_ma_face = path//8
    ind_ma = np.mod(path,8)
    # fig2 = plt.figure()
    plt.semilogy(Pt, ber1[path],'r-',marker=marks[ind_ma],label='SVD-ZF %dx%d 弱用戶 %dm 路徑：%d'%(Nt,Nr,d1,path+1),fillstyle=markes_face[ind_ma_face])
    plt.semilogy(Pt, ber2[path],'g--',marker=marks[ind_ma],label='SVD-ZF %dx%d 強用戶 %dm 路徑：%d'%(Nt,Nr,d2,path+1),fillstyle=markes_face[ind_ma_face])
    plt.title('NOMA兩個使用者BER')
    plt.xlabel('Transmit power (dBm)')
    plt.ylabel('BER')
    plt.legend()
    plt.grid(True,which='both')
    plt.tight_layout()

# for path in np.arange(Nt):
#     fig = plt.figure()
#     plt.title('Constellation Pt = %ddB, 路徑: %d'%(Pt[u],path+1))
#     plt.plot(np.real( rem1[path]), np.imag( rem1[path]  ), 'ro', label = 'Dec1, EVM=%.3f'%(EVM1[path][u]*100)+'%',markerfacecolor='none')
#     plt.plot(np.real( rem2[path] ), np.imag( rem2[path]  ), 'gv', label = 'Dec2 EVM=%.3f'%(EVM2[path][u]*100)+'%',markerfacecolor='none')
#     # plt.plot(np.real( rem1), np.imag( rem1  ), 'rx', label = 'Dec1')
#     # plt.plot(np.real( rem2 ), np.imag( rem2  ), 'g+', label = 'Dec2')
#     plt.xlabel(r'I')
#     plt.ylabel(r'Q')
#     plt.legend(loc='upper right')
#     plt.grid(True,which='both')
#     plt.tight_layout()

fig = plt.figure()
plt.title('Constellation SPC')
plt.plot(np.real( remm1), np.imag( remm1  ), 'ro', label = 'Dec1',markerfacecolor='none')
plt.plot(np.real( remm2 ), np.imag( remm2  ), 'gv', label = 'Dec2',markerfacecolor='none')
plt.xlabel(r'I')
plt.ylabel(r'Q')
plt.legend(loc='upper right')
plt.grid(True,which='both')
plt.tight_layout()

plt.show()

# from plotly.tools import mpl_to_plotly

# plotly_fig = mpl_to_plotly(fig)
# plotly_fig.update_layout(template="none")
# plotly_fig.write_html("constellation.html")

# plotly_fig = mpl_to_plotly(fig2)
# plotly_fig.update_layout(template="none")
# plotly_fig.write_html("BER.html")