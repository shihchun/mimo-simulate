# not done yet
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import ticker
from ModulationPy import PSKModem, QAMModem
np.set_printoptions(precision=16)
from functions import *
# N = 5*10**5
N = int(5*1e4)
N = 3000
Pt = np.arange(0,40+1,2) #Transmit power (dBm) 0:2:40
# Pt = np.arange(-40,40+1,5) #Transmit power (dBm) 0:2:40
pt = (10**(-3))*10**(Pt/10)  #db2pow(Pt) #Transmit power (linear scale)

BW = 10**6   #Bandwidth = 1 MHz
No = -174 + 10*np.log10(BW)	#Noise power (dBm)
no = (10**(-3))*10**(No/10) #db2pow(No) #Noise power (linear scale) 
# 10^(-Eb_N0_dB(ii)/20)*n    10**()
d1 = 500; d2 = 200; d3 = 70	#Distances 
a1 = 0.8; a2 = 0.15; a3 = 0.05	#Power allocation coefficients

eta = 4	#Path loss exponent
nSample= int(N/(2**1)) # qpsk/4QAM int(N/(2**(np.log2(modem.M)-1)))
#Generate Rayleigh fading channel for the three users
# MU-MIMO channel shape H(Nr*K,Nt), than equalized nSample channel
# H = (np.random.randn(Nr*K,Nt) + 1j*np.random.randn(Nr*K,Nt))/np.sqrt(2) # with nSample H

# #########  example of slice channel  #########
print('Channel Loading ...')

Nt = 16
Nr = 4
K = 4      # User number
channNum = nSample  #For each user

h = np.zeros((K,nSample,Nr,Nt),dtype='cfloat') 
# h = np.zeros((K,nSample,Nr,Nr),dtype='cfloat') # square
genH = np.zeros((Nr, Nt, channNum*K),dtype='cfloat') # NrxNt channel every UE channel
for ch in np.arange(channNum*K):
    genH[:, :, ch] = (np.random.randn(Nr, Nt) + 1j*np.random.randn(Nr, Nt)) / np.sqrt(2)
Hcell = np.zeros((K,Nr,Nt),dtype='cfloat'); Hcelleq = {}; wt = {}; wte = {} # python dictionary, matlab cell array

for ichannel in np.arange(channNum): # run channNum times
    H = np.zeros( (K*Nr, Nt) ,dtype='cfloat') # MU-MIMO channel Nr*K x Nt
    for k in np.arange(K): # get the kth users (Nr,Nt) matrix, MU-MIMO (Nr*K,Nt) --> function H2Cell return Hcell, H
        Hcell[k] = genH[:, :, ichannel+1 + channNum * (k+1-1) -1] # genH(:, :, ichannel + channNum * (k-1));
        H[(k+1-1)*Nr + np.arange(1,Nr+1,1)-1,:] = Hcell[k] # porb # H((k-1)*Nr + (1:Nr), 1:Nt) = Hcell{k};
        pass

    F = 1/np.sqrt(Nt)*np.exp(1j*np.angle(H)).T.conj()
    FQPR = HybridQuant(1, F) # analog RF Quant # get slower 多這個loop會很慢
    H = H@FQPR # hybrid beamforming
    # precoding MU-MIMO channel (Transmission precoding) Nr*K, Nt
    # Rs = {'Rs':(P/(K*Ns))*np.eye(K*Ns)} 
    # wt,c = CalBDPrecoder(Rs,H) # all with BD combiner
    wtt = H.T.conj()@pinv(H@H.T.conj()) # wt@H Hcell
    H = wtt@H
    
    for k in np.arange(K):
        # precoding MIMO ith cell channel (MIMO receiver Precoding) Nr, Nt cell matrix
        # wt[str(k)] = normalize_precoder ( Hcell[k].T.conj()@pinv(Hcell[k]@Hcell[k].T.conj()) ) # UE1 with ZF
        wt[str(k)] = Hcell[k].T.conj()@pinv(Hcell[k]@Hcell[k].T.conj()) 
    
    h[0][ichannel] = np.sqrt(d1**(-eta))* Hcell[0]
    # h[0][ichannel] = np.sqrt(d1**(-eta))* Hcell[0][0:2,0:2] # square
    h[1][ichannel] = np.sqrt(d2**(-eta))* Hcell[1]
    # h[1][ichannel] = np.sqrt(d2**(-eta))* Hcell[1][0:2,0:2]
    h[2][ichannel] = np.sqrt(d3**(-eta))* Hcell[2]
    # h[2][ichannel] = np.sqrt(d3**(-eta))* Hcell[2][0:2,0:2]
    h[3][ichannel] = np.sqrt(d3**(-eta))* Hcell[3]

print('generation done !')
##############################################
modem = PSKModem(4)
modem = QAMModem(4)
# modem.plot_const() # plot constellation
modem.soft_decision = False
# modem.demodulate( modem.modulate([0,1,1,1])).astype(int) # hard decision

#Do super position coding
alpha=[0.8,0.15,0.05]
SPC = NOMA_SPC(N, alpha, modem)
x = SPC['x']

ber1 = [0]*len(Pt)
ber2 = [0]*len(Pt)
ber3 = [0]*len(Pt)
ww = np.zeros((K,nSample,Nt,Nr),dtype='cfloat')
# ww = np.zeros((K,nSample,Nr,Nr),dtype='cfloat') # square

r = np.zeros((K,nSample,Nr,1),dtype='cfloat')
y = np.zeros((K,nSample,Nt,1),dtype='cfloat')
# y = np.zeros((K,nSample,Nr,1),dtype='cfloat') # square
for u in np.arange(len(Pt)):
    # select square matrix 2x2
    for _ in range(K): # ZF equalize weight
        for ichannel in range(channNum):
            ww[_][ichannel] = h[_][ichannel].T.conj()@pinv(h[_][ichannel]@h[_][ichannel].T.conj())
            # ww[_][ichannel] = normalize_precoder(ww[_][ichannel])
            # ww[_][ichannel] = normalize_precoder(ww[_][ichannel],'fro',pt[u])
            n = np.sqrt(no)*(np.random.randn(Nr,1) + 1j*np.random.randn(Nr,1))/np.sqrt(2)
            r[_][ichannel] = np.sqrt(pt[u]) * ( h[_][ichannel]@ np.tile(x[ichannel],(h[_][ichannel].shape[1],1)) )+ n # Rx 1500, Nr, 1
            y[_][ichannel] = ( ww[_][ichannel] @ r[_][ichannel] ) # Eq 1500, Nt, 1

    # combining 
    comb=[]
    for _ in range(3):
        # comb.append(MRC(y[_],h[_])) 
        comb.append(y[_])

    # Tx transmitted Nt path y[0].shape
    # Rx receive Nr path r[0].shape
    # Equalize Nt path y[0].shape
    dec1 = NOMA_SIC( comb[0][:,0].squeeze(), h[0], pt[u], alpha, modem)
    dec2 = NOMA_SIC( comb[1][:,0].squeeze(), h[1], pt[u], alpha, modem)
    dec3 = NOMA_SIC( comb[2][:,0].squeeze(), h[2], pt[u], alpha, modem)

    # dec1 = NOMA_SIC( awgn(comb[0][:,0].squeeze(),Pt[u]), h[0], pt[u], alpha, modem) # channel path 0 of Nt paths
    # dec2 = NOMA_SIC( awgn(comb[1][:,0].squeeze(),Pt[u]), h[1], pt[u], alpha, modem)
    # dec3 = NOMA_SIC( awgn(comb[2][:,0].squeeze(),Pt[u]), h[2], pt[u], alpha, modem)
    
    rem1 = dec1['rem0']
    rem2 = dec1['rem1']
    rem3 = dec3['rem2']

    # input signal SPC no need h with feature scaling
    decxx = NOMA_SIC(np.sqrt(pt[u])*x, h[0], pt[u], alpha, modem)
    remm1 = decxx['rem0']
    remm2 = decxx['rem1']
    remm3 = decxx['rem2']
    #BER calculation
    ber1[u] = np.sum( dec1['0'] != SPC['0'])/N
    ber2[u] = np.sum( dec2['1'] != SPC['1'])/N
    ber3[u] = np.sum( dec3['2'] != SPC['2'])/N
    if u==16:
        # 檢查 demodulaotor 解碼能力
        berr1 = np.sum( decxx['0'] != SPC['0'])/N
        berr2 = np.sum( decxx['1'] != SPC['1'])/N
        berr3 = np.sum( decxx['2'] != SPC['2'])/N
        print(berr1,berr2,berr3)

fig = plt.figure()
plt.semilogy(Pt, ber1,'-',marker='v',label='User 1 (Weakest user) %dm'%d1)
plt.semilogy(Pt, ber2,'-',marker='v',label='User 2 %dm'%d2)
plt.semilogy(Pt, ber3,'-',marker='v',label='User 3 (Strongest user) %dm'%d3)
plt.title('NOMA multiple Users')
plt.xlabel('Transmit power (dBm)')
plt.ylabel('BER')
plt.grid(True,which='both')
plt.tight_layout()
plt.legend()

fig = plt.figure()
plt.title('Constellation')
plt.plot(np.real( rem1), np.imag( rem1  ), '.r', label = 'Dec1')
plt.plot(np.real( rem2 ), np.imag( rem2  ), '.g', label = 'Dec2')
plt.plot(np.real( rem3 ), np.imag( rem3  ), '.b', label = 'Dec3')
plt.xlabel(r'I')
plt.ylabel(r'Q')
plt.legend(loc='upper right')
plt.grid(True,which='both')
plt.tight_layout()

fig = plt.figure()
plt.title('Constellation SPC')
plt.plot(np.real( remm1), np.imag( remm1  ), '.r', label = 'Dec1')
plt.plot(np.real( remm2 ), np.imag( remm2  ), '.g', label = 'Dec2')
plt.plot(np.real( remm3 ), np.imag( remm3  ), '.b', label = 'Dec3')
plt.xlabel(r'I')
plt.ylabel(r'Q')
plt.legend(loc='upper right')
plt.grid(True,which='both')
plt.tight_layout()

plt.show()

# fig2html(fig,'temp-plot.html')