import numpy as np
# if pass value to function a(distance_array.copy()) is better to pass the copy things
# https://www.etsi.org/deliver/etsi_tr/138900_138999/138901/14.03.00_60/tr_138901v140300p.pdf
# prop using table 7.4.2-1
# set scenario -> assign NLOS/LOS -> Calculate Pathloss --> antenna/channel modeling
def PL_38901(Incov):
  """3GPP TR 38.901 14.3.0 Release 14 Outdoor Path Loss model
  PL_RMa, PL_UMa, PL_UMi, PL_InH
  Args:
      Incov (dict): get Incov by insert the

  Returns:
      [float]: Pathloss of 3GPP TR 38.901 14.3.0 Release 14 Outdoor Path Loss model
  """
  if Incov.copy()['Scenario']=='RMa' or Incov.copy()['Scenario']=='UMa' or Incov.copy()['Scenario']=='UMi':
    Incov['d_3d'] = np.sqrt(Incov.copy()['d_2d']**2+(Incov.copy()['h_bs']-Incov.copy()['h_ut'] )**2)
    # print(Incov.copy()['d_3d'])
  if Incov.copy()['shad'] == False:
      Incov['sigma'] =0
  if Incov.copy()['fc']>100 or Incov.copy()['fc']<0.5:
      raise ValueError("Center Frequency not in 0.5-100[GHz]")

  # calulate d_bp break point coef
  if Incov.copy()['Scenario']=='RMa':
    Incov['d_bp'] = 2*np.pi*Incov.copy()['h_bs']*Incov.copy()['h_ut']*(Incov.copy()['fc']*10**8)/(299792458)
    Incov['d_bp'] = 4*Incov.copy()['h_bs']*Incov.copy()['h_ut']*(Incov.copy()['fc']*10**8)/(299792458)
  if Incov.copy()['Scenario']=='UMa':
    g = Incov.copy()['d_2d']
    if g<=18: g=0
    if g>=18: g = (5/4)*(g/100)**3*np.exp(-g/150)
    if Incov.copy()['h_ut'] <13: Incov['he']=1
    else: Incov['he'] = 1/(1+((Incov.copy()['h_ut']-13)**1.5/10)*g)
    
    Incov['h_e_bs']=Incov.copy()['h_bs']-Incov.copy()['he']; Incov['h_e_ut']=Incov.copy()['h_ut']-Incov.copy()['he']
    Incov['d_bp'] = 2*np.pi* Incov.copy()['h_e_bs']*Incov.copy()['h_e_ut']*(Incov.copy()['fc']*10**8)/(299792458)
    Incov['d_bp'] = 4* Incov.copy()['h_e_bs']*Incov.copy()['h_e_ut']*(Incov.copy()['fc']*10**8)/(299792458)
  if Incov.copy()['Scenario']=='UMi':
    Incov['he']=1
    Incov['h_e_bs']=Incov.copy()['h_bs']-Incov.copy()['he']; Incov['h_e_ut']=Incov.copy()['h_ut']-Incov.copy()['he']
    Incov['d_bp'] = 2*np.pi* Incov.copy()['h_e_bs']*Incov.copy()['h_e_ut']*(Incov.copy()['fc']*10**8)/(299792458)
    Incov['d_bp'] = 4* Incov.copy()['h_e_bs']*Incov.copy()['h_e_ut']*(Incov.copy()['fc']*10**8)/(299792458)


  if Incov.copy()['Scenario']=='RMa':
    if Incov.copy()['d_2d']<=10: prop=1
    else: prop = np.exp(-1*((Incov.copy()['d_2d']-10)/1000))
    if Incov.copy()['w_avg']>50 or Incov.copy()['w_avg']<5 and Incov.copy()['h_avg']>50 and Incov.copy()['h_avg']<5:
      raise ValueError("RMa w_avg and h_avg not in 5-50[m]")
    if Incov.copy()['h_ut']>10 or Incov.copy()['h_ut']<1:
      raise ValueError("RMa h_ut not in 1-10[m]")
    if Incov.copy()['h_bs']>150 or Incov.copy()['h_bs']<10:
      raise ValueError("RMa h_bs not in 10-150[m]")
    if Incov.copy()['d_2d'] >10000 or Incov.copy()['d_2d'] <10:
      raise ValueError("RMa d_2d not in 10-10,000[m]")
    if Incov.copy()['shad'] == True and Incov.copy()['condition']=='LOS':
      Incov['sigma'] = [4,6][np.random.randint(0,1+1)]
    if Incov.copy()['shad'] == True and Incov.copy()['condition']=='NLOS':
      Incov['sigma'] =8
    # 10m-d_bp
    Incov['PL_RMA_LOS'] = 20*np.log10(40*np.pi*Incov.copy()['d_3d']*Incov.copy()['fc']/3)+\
      min(0.03*Incov.copy()['h_avg']**(1.72),10)*np.log10(Incov.copy()['d_3d'])-\
      min(0.044*Incov.copy()['h_avg']**(1.72),14.77) + 0.002*np.log10(Incov.copy()['h_avg'])*Incov.copy()['d_3d']
    
    Incov['PL_RMA_LOS_bp'] = 20*np.log10(40*np.pi*Incov.copy()['d_bp']*Incov.copy()['fc']/3)+\
        min(0.03*Incov.copy()['h_avg']**(1.72),10)*np.log10(Incov.copy()['d_bp'])-\
        min(0.044*Incov.copy()['h_avg']**(1.72),14.77) + 0.002*np.log10(Incov.copy()['h_avg'])*Incov.copy()['d_bp']
    if Incov.copy()['d_bp'] >= Incov.copy()['d_2d'] and Incov.copy()['d_2d']<=10000: # d_bp-10km
      Incov['PL_RMA_LOS'] = Incov.copy()['PL_RMA_LOS_bp'] + 40*np.log10(Incov.copy()['d_3d']/Incov.copy()['d_bp'])
    
    if Incov.copy()['condition']=='LOS':  PL = Incov.copy()['PL_RMA_LOS']
    Incov['PL_RMA_NLOS'] = 161.04-7.1*np.log10(Incov.copy()['w_avg'])+7.5*np.log10(Incov.copy()['h_avg'])-\
        (24.37-3.7*(Incov.copy()['h_avg']/Incov.copy()['h_bs'])**2)*np.log10(Incov.copy()['h_bs'])+\
          (43.42-3.11*np.log10(Incov.copy()['h_bs']))*(np.log10(Incov.copy()['d_3d'])-3)+\
            20*np.log10(Incov.copy()['fc'])-(3.2*(np.log10(11.75*Incov.copy()['h_ut'])**2-4.97))
    if Incov.copy()['condition']=='NLOS':
      PL = max(Incov.copy()['PL_RMA_NLOS'],Incov.copy()['PL_RMA_LOS'])
    


    # PL_RMA_LOS(d_bp <= d_2d & d_2d <=10000) = PL_RMA_LOS(d_bp <= d_2d & d_2d <=10000) + 40*log10(d_3d(d_bp <= d_2d & d_2d <=10000)/Incov.copy()['d_bp'])
    

  if Incov.copy()['Scenario']=='UMa':
    prop=0
    if Incov.copy()['d_2d']<=18: prop=1
    if prop==0 and Incov.copy()['d_2d']<=13:
      prop = (18/Incov.copy()['d_2d'] + np.exp(-1*((Incov.copy()['d_2d'])/63))*(1-(18/Incov.copy()['d_2d'])))
    if prop==0 and Incov.copy()['d_2d']>13 and Incov.copy()['d_2d']<=23:
      A = (18/Incov.copy()['d_2d'] + np.exp(-1*((Incov.copy()['d_2d'])/63))*(1-(18/Incov.copy()['d_2d']))) # First part of the equation [] square brackets
      B = (1+((Incov.copy()['h_ut']-13)/10)**(1.5)*(5/4)*(Incov.copy()['d_2d']/100)**3*np.exp(-1*(Incov.copy()['d_2d']/150))); # Second part, round brackets
      prop = A*B
    if Incov.copy()['h_ut']>22.5 or Incov.copy()['h_ut']<1.5:
      raise ValueError("UMa h_ut not in 1.5-22.5[m] (Urban)")
    if Incov.copy()['d_2d'] >10000 or Incov.copy()['d_2d'] <10:
      raise ValueError("UMa d_2d not in 10-10,000[m]")
    if Incov.copy()['h_bs']!=25 and Incov.copy()['Scenario']=='UMa':
      Incov.copy()['h_bs'] = 25; #print('h_bs = 25 only in UMa: Urban macro you insert %d'%Incov.copy()['h_bs'])
    if Incov.copy()['shad'] ==True and Incov.copy()['condition']=='LOS':
      Incov['sigma'] =4
    if Incov.copy()['shad'] ==True and Incov.copy()['condition']=='NLOS':
      Incov['sigma'] =6 
    # if PL == PL_optional and Incov.copy()['shad'] ==True: Incov.copy()['sigma']  = 7.8
    # 10m-d_bp
    Incov['PL_UMA_LOS'] = 28.0+22*np.log10(Incov.copy()['d_3d']) + 20*np.log10(Incov.copy()['fc'])
    if Incov.copy()['d_bp'] >= Incov.copy()['d_2d'] and Incov.copy()['d_2d']<=5000: # d_bp-5km
      Incov['PL_UMA_LOS'] = 28.0+40*np.log10(Incov.copy()['d_3d']) + 20*np.log10(Incov.copy()['fc'])-\
        9*np.log10((Incov.copy()['d_bp'])**2+(Incov.copy()['h_bs']-Incov.copy()['h_ut'])**2)
    if Incov.copy()['condition']=='LOS':  PL = Incov.copy()['PL_UMA_LOS']
    Incov['PL_UMA_NLOS'] = 13.54+39.08*np.log10(Incov.copy()['d_3d'])+20*np.log10(Incov['fc'])-\
      0.6*(Incov['h_ut']-1.5)
    if Incov.copy()['condition']=='NLOS':
      PL = max(Incov['PL_UMA_LOS'],Incov['PL_UMA_NLOS'])
    # PL_UMA_LOS(d_bp <= d_2d & d_2d <=5000) = 28+40*log10(d_3d(d_bp <= d_2d & d_2d <=5000)) + 20*log10(f_c)-9*log10((d_bp)^2+(h_bs-h_ut)^2)

  if Incov.copy()['Scenario']=='UMi': # Street Canyon
    if Incov.copy()['d_2d']<=18: prop=1
    else: prop = 18/Incov.copy()['d_2d']+ np.exp(-1*((Incov.copy()['d_2d'])/36))*(1-(18/Incov.copy()['d_2d']))
    if Incov.copy()['h_ut']>22.5 or Incov.copy()['h_ut']<1.5:
      raise ValueError("h_ut not in 1.5-22.5[m] (Urban)")
    if Incov.copy()['h_bs']!=10 and Incov.copy()['Scenario']=='UMi':
      Incov.copy()['h_bs'] = 10; print('h_bs = 10 only in UMi: Urban micro')
    if Incov.copy()['shad'] ==True and Incov.copy()['condition']=='LOS':
      Incov['sigma'] =4
    if Incov.copy()['shad'] ==True and Incov.copy()['condition']=='NLOS':
      Incov['sigma'] =7.82
    
    # Calculate LOS Pathloss
    # Incov.copy()['d_bp_hat']=2*pi*h_bs*h_ut*fc/c fc in GHz, c is light speed in p.27 note 1
    Incov.copy()['d_bp_hat'] = 2*np.pi*(Incov.copy()['h_bs']-1)*(Incov.copy()['h_ut']-1)*Incov.copy()['fc']*10**8/(299792458) # UMi he=1
    if Incov.copy()['d_2d']>=10 and Incov.copy()['d_2d'] <= Incov.copy()['d_bp_hat']:
      PL = 32.4+21*np.log(Incov.copy()['d_3d'])+20*np.log10(Incov.copy()['fc'])

    if Incov.copy()['d_2d']>=Incov.copy()['d_bp_hat'] and Incov.copy()['d_2d'] <= 5*10**3:
      PL = 32.4+40*np.log(Incov.copy()['d_3d'])+20*np.log10(Incov.copy()['fc'])- \
        9.5*np.log10((Incov.copy()['d_bp_hat']**2)+(Incov.copy()['h_bs']-Incov.copy()['h_ut'])**2)

    if Incov.copy()['condition']=='NLOS':
      PL_NLOS = 35.3*np.log10(Incov.copy()['d_3d'])+22.4+21.3*np.log10(Incov.copy()['fc'])-0.3*(Incov.copy()['h_ut']-1.5)
      PL_optional=3.24+20*np.log10(Incov.copy()['fc'])+31.9*np.log10(Incov.copy()['d_3d'])
      PL = max(PL,PL_NLOS,PL_optional)
      if PL == PL_optional and Incov.copy()['shad'] ==True: Incov.copy()['sigma']  = 8.2

  if Incov.copy()['Scenario']=='InH': # InH - Office
    if Incov.copy()['d_3d']>150 or Incov.copy()['d_3d']<1:
      raise ValueError("InH d_3d=%f not in 1-150[m]"%Incov.copy()['d_3d'])
    if Incov.copy()['shad'] ==True and Incov.copy()['condition']=='LOS':
      Incov['sigma'] =3
    if Incov.copy()['shad'] ==True and Incov.copy()['condition']=='NLOS':
      Incov['sigma'] =8.03
    
    PL = 32.4+17.3*np.log10(Incov.copy()['d_3d'])+20*np.log10(Incov.copy()['fc'])
    if Incov.copy()['condition']=='NLOS':
      PL_NLOS = 38.3*np.log10(Incov.copy()['d_3d'])+17.30+24.9*np.log10(Incov.copy()['fc'])
      PL_optional=3.24+20*np.log10(Incov.copy()['fc'])+31.9*np.log10(Incov.copy()['d_3d'])
      PL = max(PL,PL_NLOS,PL_optional)
      if PL == PL_optional and Incov.copy()['shad'] ==True: Incov.copy()['sigma']  = 8.29

  PL = PL + Incov.copy()['sigma'] *np.random.randn()
  return PL

def PL_RMa(fc,d_2d,h_bs=35,h_ut=1.5,shad=False,h_avg=20,w_avg=10,condition='LOS'):
  """RMa Path Loss model
  ---
  3GPP TR 38.901 14.3.0 Release 14 Outdoor Path Loss model
  w_avg=18m,8m is taiwan 1 lan road
  h_avg=20m, h_bs=35m, h_ut=1.5m use 38901 default value

  .. doctest::
    >>> from PL3gpp38901 import PL_RMa
    >>> PL_RMa(0.5,50) # fc and distance
    >>> PL_RMa(0.5,50,condition='NLOS',shad=True) # add shadowing
    >>> PL_RMa(0.5,50,shad=4) # Incov.copy()['sigma']  = 4 shadowing
    >>> PL_RMa(0.5,50,35,1.5,False)
    >>> PL_RMa(0.5,50,shad=6) # Incov.copy()['sigma']  = 6 shadowing

  Args:
      condition (str): 'LOS' or 'NLOS'
      fc (float): center frequency in 0.5-100 [GHz] 
      d_2d (float): 2d distance in meters - also as matrix
      d_3d (float): 3d_distance in meters 1-150[m]
      h_bs (float): height of tx in meters 'RMa': 10-150[m]
      h_ut (float): height of rx in meters 'RMa': 1-10[m]
      h_avg (float): average height of buildings 'RMa': 5-50 [m]
      w_avg (float): average width of roads 'RMa': 5-50[m]
      shad (boolen): shadow fading or not
  Returns:
      [float]: Path Loss of 3GPP TR 38.901 14.3.0 Release 14 Outdoor Path Loss model
  """  
  if type(d_2d)==np.ndarray:
    return [ PL_38901({'Scenario':'RMa','fc': fc,'condition':condition,'d_2d':d,'h_bs':h_bs,'h_ut':h_ut,\
      'h_avg':h_avg,'w_avg':w_avg,'shad':shad}) for d in d_2d]
  else:
    return PL_38901({'Scenario':'RMa','fc': fc,'condition':condition,'d_2d':d_2d,'h_bs':h_bs,'h_ut':h_ut,\
      'h_avg':h_avg,'w_avg':w_avg,'shad':shad})


def PL_UMa(fc,d_2d,h_ut=1.5,h_bs=25,shad=False,condition='LOS',optional=False):
  """UMa Path Loss model
  ---
  3GPP TR 38.901 14.3.0 Release 14 Outdoor Path Loss model
  h_ut 1.5-22.5[m], h_bs=25m
  h_ut=1.5m use 38901 default value
  .. doctest::

    >>> from PL3gpp38901 import PL_UMa
    >>> PL_UMa(0.5,50) # fc and distance
    >>> PL_UMa(0.5,50,condition='NLOS',shad=True) # add shadowing
    >>> PL_UMa(0.5,50,22.5)

  Args:
      condition (str): 'LOS' or 'NLOS'
      fc (float): center frequency in 0.5-100 [GHz] 
      d_2d (float): 2d distance in meters - also as matrix
      h_bs (float): height of tx in meters 'RMa': 10-150[m]
      h_ut (float): height of rx in meters 'RMa': 1-10[m]
      shad (boolen): shadow fading or not
      optional(boolen): use optional loss
  Returns:
      [float]: Path Loss of 3GPP TR 38.901 14.3.0 Release 14 Outdoor Path Loss model
  """
  if type(d_2d)!=float and type(d_2d)!=int:
    return [ PL_38901({'Scenario':'UMa','fc': fc,'condition':condition,'d_2d':d,'h_bs':h_bs,'h_ut':h_ut,\
    'shad':shad,'optional': optional}) for d in d_2d]
  else:
    return PL_38901({'Scenario':'UMa','fc': fc,'condition':condition,'d_2d':d_2d,'h_bs':h_bs,'h_ut':h_ut,\
      'shad':shad})

def PL_UMi(fc,d_2d,h_ut=1.5,h_bs=10,shad=False,h_avg=20,w_avg=18,condition='LOS',optional=False):
  """UMa Path Loss model
  ---
  3GPP TR 38.901 14.3.0 Release 14 Outdoor Path Loss model
  h_ut 1.5-22.5[m], h_bs=10m
  h_ut=1.5m use 38901 default value
  .. doctest::

    >>> from PL3gpp38901 import PL_UMa
    >>> PL_UMa(0.5,50) # fc and distance
    >>> PL_UMa(0.5,50,condition='NLOS',shad=True) # add shadowing
    >>> PL_UMa(0.5,50,22.5)

  Args:
      condition (str): 'LOS' or 'NLOS'
      fc (float): center frequency in 0.5-100 [GHz] 
      d_2d (float): 2d distance in meters - also as matrix
      h_bs (float): height of tx in meters 'RMa': 10-150[m]
      h_ut (float): height of rx in meters 'RMa': 1-10[m]
      shad (boolen): shadow fading or not
      optional(boolen): use optional loss
  Returns:
      [float]: Path Loss of 3GPP TR 38.901 14.3.0 Release 14 Outdoor Path Loss model
  """
  if type(d_2d)!=float and type(d_2d)!=int:
    return [ PL_38901({'Scenario':'UMa','fc': fc,'condition':condition,'d_2d':d,'h_bs':h_bs,'h_ut':h_ut,\
      'shad':shad}) for d in d_2d]
  else:
    return PL_38901({'Scenario':'UMa','fc': fc,'condition':condition,'d_2d':d_2d,'h_bs':h_bs,'h_ut':h_ut,\
      'shad':shad})

def PL_InH(fc,d_3d,shad=False,condition='LOS'):
  """UMa Path Loss model
  ---
  3GPP TR 38.901 14.3.0 Release 14 Outdoor Path Loss model
  d_3d = 1-150[m]
  # for InH and freq blow 6GHz use 6 GHz
  .. doctest::
    >>> import PL_38901
    >>> import PL_InH
    >>> PL_UMa(0.5,50) # fc and distance
    >>> PL_UMa(0.5,50,condition='NLOS',shad=True) # add shadowing

  Args:
      condition (str): 'LOS' or 'NLOS'
      fc (float): center frequency in 0.5-100 [GHz] 
      d_3d (float): 3d_distance in meters 1-150[m]
      h_bs (float): height of tx in meters  10-150[m]
      h_ut (float): height of rx in meters  1-10[m]
      shad (boolen): shadow fading or not
  Returns:
      [float]: Path Loss of 3GPP TR 38.901 14.3.0 Release 14 Outdoor Path Loss model
  """
  if fc<=6:
    fc=6
  if type(d_3d)!=float and type(d_3d)!=int:
    return [ PL_38901({'Scenario':'InH','fc': fc,'condition':condition,'d_3d':d,\
    'shad':shad}) for d in d_3d]
  else:
    return PL_38901({'Scenario':'InH','fc': fc,'condition':condition,'d_3d':d_3d,\
      'shad':shad})