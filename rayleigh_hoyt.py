import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
#  This function generates normalized rayleigh samples based on Inverse DFT 
# ref from https://in.mathworks.com/matlabcentral/fileexchange/24318-rayleigh-fading-channel-generation
# RayleighFading_generation(1024,10,30.72*10**6)
# kM not use currently
def ray( fftSize,numBlocks,fs,kM):
    fd = fs/10
    numSamples=fftSize*numBlocks; # total number of samples
    fM=fd/fs       #n ormalized doppler shift
    NfM=fftSize*fM
    kM=np.floor(NfM) # maximum freq of doppler filter in FFT samples
    doppFilter = np.hstack([0,
        1/np.sqrt(2*np.sqrt(1-( ((np.arange(1,kM-1+1,1))/NfM)**2 ))),
        np.sqrt((kM/2)*((np.pi/2)-np.arctan((kM-1)/np.sqrt(2*kM-1)))),
        np.zeros(int(fftSize-2*kM-1)),
        np.sqrt( (kM/2)*((np.pi/2)-np.arctan((kM-1)/np.sqrt(2*kM-1))) ),
        1/np.sqrt(2*np.sqrt(1-((( np.arange(kM-1,1-1,-1) )/NfM)**2)))
        ]).T
    sigmaG=np.sqrt((2*2/(fftSize**2))*np.sum(doppFilter**2))
    gSamplesI=np.random.randn(numSamples,2)  # i.i.d gaussian input samples (in phase)
    gSamplesQ=np.random.randn(numSamples,2)  # i.i.d gaussian input samples (quadrature phase)
    gSamplesI=(1/sigmaG)*(gSamplesI[:,0]+1j*gSamplesI[:,1])
    gSamplesQ=(1/sigmaG)*(gSamplesQ[:,0]+1j*gSamplesQ[:,1])
    # filtering
    filterSamples= np.kron(np.ones(numBlocks),doppFilter)
    filterSamples[np.where(filterSamples==0)]=1
    gSamplesI = gSamplesI*filterSamples
    gSamplesQ = gSamplesQ*filterSamples
    freqSignal = gSamplesI-1j*gSamplesQ # conj()
    freqSignal = freqSignal.reshape( fftSize,numBlocks ) 
    # outSignal = ifft(freqSignal,fftSize) #python is different from matlab
    # outSignal = fft[0]+ [2*fft[1:fftSize/2]] 
    outSignal = np.fft.ifft(freqSignal,fftSize).flatten(order='C')*2
    outSignal = outSignal[np.arange(int(fftSize*numBlocks))]
    return outSignal

def hoyt( fftSize,numBlocks,fs,kM,q=0.5):
    # q=0.5;
    sfq = (1+q**2)/(2*q**2)
    fd = fs/10
    numSamples=fftSize*numBlocks; # total number of samples
    fM=fd/fs       #n ormalized doppler shift
    NfM=fftSize*fM
    kM=np.floor(NfM) # maximum freq of doppler filter in FFT samples
    doppFilter = np.hstack([0,
        1/np.sqrt(2*np.sqrt(1-( ((np.arange(1,kM-1+1,1))/NfM)**2 ))),
        np.sqrt((kM/2)*((np.pi/2)-np.arctan((kM-1)/np.sqrt(2*kM-1)))),
        np.zeros(int(fftSize-2*kM-1)),
        np.sqrt( (kM/2)*((np.pi/2)-np.arctan((kM-1)/np.sqrt(2*kM-1))) ),
        1/np.sqrt(2*np.sqrt(1-((( np.arange(kM-1,1-1,-1) )/NfM)**2)))
        ]).T
    sigmaG=np.sqrt((2*2/(fftSize**2))*sum(doppFilter**2))
    # sigmaG=sqrt((2*2/(fftSize.^2))*sum(doppFilter.^2));
    gSamplesI=np.random.randn(numSamples,2)  # i.i.d gaussian input samples (in phase)
    gSamplesQ=np.random.randn(numSamples,2)  # i.i.d gaussian input samples (quadrature phase)
    gSamplesI=(1/sigmaG)*(gSamplesI[:,0]+1j*gSamplesI[:,1])
    gSamplesQ=(1/sigmaG)*(gSamplesQ[:,0]+1j*gSamplesQ[:,1])
    # filtering
    # filterSamples= kron(ones(numBlocks),doppFilter)
    filterSamples= np.kron(np.ones(numBlocks),doppFilter)
    filterSamples[np.where(filterSamples==0)]=1
    gSamplesI = gSamplesI*filterSamples
    gSamplesQ = q*gSamplesQ*filterSamples
    freqSignal = gSamplesI-1j*gSamplesQ
    freqSignal = freqSignal.reshape( fftSize,numBlocks )
    # outSignal = ifft(freqSignal,fftSize) #python is different from matlab
    # outSignal = fft[0]+ [2*fft[1:fftSize/2]] 
    outSignal = np.fft.ifft(freqSignal,fftSize).flatten(order='C')*2
    outSignal = outSignal[np.arange(int(fftSize*numBlocks))]
    return outSignal

q = 0.5
# 不管輸入多少好像都沒有變化都跟 0.5 差不多 
# 不過分佈可以模擬出跟q=0.5相近的情況，比Rayleigh衰落更多的情況
y = ray(512,20,30.72*10**6,30.72*10**5)
print(np.shape(y))
# import scipy.io as sio
# mat = sio.loadmat('rayleigh.mat')
# mat = sio.loadmat('C:/Users/geek/Desktop/論文/程式/rayleigh.mat')
# yy = mat['z']
yy = hoyt(512,20,30.72*10**6,30.72*10**5,q)
h = ( np.random.randn(len(y))+1j*np.random.randn(len(y)) ) /np.sqrt(2)
h_q = ( np.random.randn(len(y))+1j*q*np.random.randn(len(y)) ) /np.sqrt(2)
plt.plot(20*np.log10(abs(h[np.arange(600)])),label='H Rayleigh',alpha=0.2)
plt.plot(20*np.log10(abs(h_q[np.arange(600)])),label='H Hoyt q=%.1f'%q,alpha=0.2)
plt.plot(20*np.log10(abs(y[np.arange(600)])),label='Rayleigh')
plt.plot(20*np.log10(abs(yy[np.arange(600)])),label='Hoyt q=%.1f'%q)
plt.plot([0, 600], [0, 0], ':r',linewidth=3,label='No Fading') # 0 dB
plt.legend()
plt.figure()
plt.plot(20*np.log10(abs(h[np.arange(1200)])),label='H Rayleigh',alpha=0.2)
plt.plot(20*np.log10(abs(h_q[np.arange(1200)])),label='H Hoyt q=%.1f'%q,alpha=0.2)
plt.plot(20*np.log10(abs(y[np.arange(1200)])),label='Rayleigh')
plt.plot(20*np.log10(abs(yy[np.arange(1200)])),label='Hoyt q=%.1f'%q)
plt.plot([0,1200], [0, 0], ':r',linewidth=3,label='No Fading') # 0 dB
plt.legend()
plt.figure()

plt.plot(20*np.log10(abs(h)),label='H Rayleigh',alpha=0.2)
plt.plot(20*np.log10(abs(h_q)),label='H Hoyt q=%.1f'%q,alpha=0.2)
plt.plot(20*np.log10(abs(y)),label='Rayleigh')
plt.plot(20*np.log10(abs(yy)),label='Hoyt q=%.1f'%q)
plt.plot([0,len(y)], [0, 0], ':r',linewidth=3,label='No Fading') # 0 dB
plt.legend()
plt.figure()
plt.hist((abs(h)),bins='auto',density=True,alpha=0.6,label='H Rayleigh')
plt.hist((abs(h_q)),bins='auto',density=True,alpha=0.6,label='H Hoyt q=%.1f'%q)
plt.hist((abs(y)),bins='auto',density=True,alpha=0.6,label='Rayleigh')
plt.hist((abs(yy)),bins='auto',density=True,alpha=0.6,label='Hoyt q=%.1f'%q)
plt.legend()
plt.show()