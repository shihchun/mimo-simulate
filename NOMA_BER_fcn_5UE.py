import numpy as np
import matplotlib.pyplot as plt
from ModulationPy import PSKModem, QAMModem
from functions import NOMA_SIC,NOMA_SPC,NOMA_channel,awgn,threshold_bit,PL_Hata
import scipy.linalg as la
from PL3gpp38901 import PL_RMa,PL_UMa,PL_UMi,PL_InH # load pathloss
import matplotlib
plt.rcParams['axes.unicode_minus'] = False # 負號問題
plt.rcParams['mathtext.fontset'] = 'custom' 
plt.rcParams['mathtext.it'] = 'STIXGeneral:italic' 
plt.rcParams['mathtext.bf'] = 'STIXGeneral:italic:bold' 
matplotlib.rc('font', family='mingliu') # , weight='bold') # 設定新細明體
# matplotlib.rc('font', family='PingFang HK') # , weight='bold') # 設定蘋方字體

# from matplotlib.font_manager import FontManager
# fm = FontManager()
# mat_fonts = set(f.name for f in fm.ttflist)
# print (mat_fonts)

N = 5*10**5
N = int(2*1e6)
#N = 3000
Pt = np.arange(0,40+1,2) #Transmit power (dBm) 0:2:40
pt = (10**(-3))*10**(Pt/10)  #db2pow(Pt) #Transmit power (linear scale)

BW = 10**6   #Bandwidth = 1 MHzs
No = -174 + 10*np.log10(BW)	#Noise power (dBm)
no = (10**(-3))*10**(No/10) #db2pow(No) #Noise power (linear scale) 
# 10^(-Eb_N0_dB(ii)/20)*n    10**()
d1 = 1000; d2 = 500; d3 = 300; d4 = 200; d5 = 70#Distances 

gg = 3/4
a1 = 0.9
a2 = gg*(1-a1)
a3 = gg*(1-(a1+a2))
a4 = gg*(1-(a1+a2+a3))
a5 = gg*(1-(a1+a2+a3+a4))
a5 = 1-(a1+a2+a3+a4)

eta = 8	#Path loss exponent
# nSample= int(N/(2**0)) # bpsk
nSample= int(N/(2**1)) # qpsk/4QAM


hh1 = (np.random.randn(nSample) + 1j*np.random.randn(nSample))/np.sqrt(2)
hh2 = (np.random.randn(nSample) + 1j*np.random.randn(nSample))/np.sqrt(2)
hh3 = (np.random.randn(nSample) + 1j*np.random.randn(nSample))/np.sqrt(2)
hh4 = (np.random.randn(nSample) + 1j*np.random.randn(nSample))/np.sqrt(2)
hh5 = (np.random.randn(nSample) + 1j*np.random.randn(nSample))/np.sqrt(2)

#Create QPSKModulator and QPSKDemodulator objects
# modem = PSKModem(2）
modem = PSKModem(4) 
modem = QAMModem(4)
# modem.plot_const() # plot constellation
modem.soft_decision = False

#Do super position coding
alpha=[a1,a2,a3,a4,a5]
SPC = NOMA_SPC(N, alpha, modem)
x = SPC['x']

ber1 = [0]*len(Pt)
ber2 = [0]*len(Pt)
ber3 = [0]*len(Pt)
ber4 = [0]*len(Pt)
ber5 = [0]*len(Pt)
comb = np.zeros( (5,nSample),dtype='cfloat' )

for u in np.arange(len(Pt)):
    #  np.sqrt(d1**(-eta)) ~= 0dbm-Ploss
    Antenna_gain=12
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'big')
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'small/medium')
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'suburban')
    # PL_Hata(900*10**6,d1,70,np.random.rand()*10,'open')
    # PL_RMa(3.5,d1,70,1.5)
    # PL_UMa(3.5,d1,1.5)
    # PL_UMi(3.5,d1,1.5)
    # PL_InH(3.5,10)
    PL1 = PL_RMa(3.5,d1,70,1.5,condition='NLOS')
    PL2 = PL_RMa(3.5,d2,70,1.5,condition='NLOS')
    PL3 = PL_RMa(3.5,d3,70,1.5,condition='NLOS')
    PL4 = PL_RMa(3.5,d4,70,1.5,condition='NLOS')
    PL5 = PL_RMa(3.5,d5,70,1.5,condition='NLOS')
    G1 = np.sqrt((10**((Pt[u]-(PL1-Antenna_gain) )/10))/2)
    G2 = np.sqrt((10**((Pt[u]-(PL2-Antenna_gain) )/10))/2)
    G3 = np.sqrt((10**((Pt[u]-(PL3-Antenna_gain) )/10))/2)
    G4 = np.sqrt((10**((Pt[u]-(PL4-Antenna_gain) )/10))/2)
    G5 = np.sqrt((10**((Pt[u]-(PL5-Antenna_gain) )/10))/2)

    h1 = G1*hh1
    h2 = G2*hh2
    h3 = G3*hh3
    h4 = G4*hh4
    h5 = G5*hh5
    h = [h1,h2,h3,h4,h5] # SISO channel 
    y = NOMA_channel(x, h, pt[u], BW,-174) # SISO
    for _ in range(5):
        # awgn additive noise
        y[_] = awgn(y[_],Pt[u])
        # equalize
        k = y[_]/h[_] # h[_]
        # modulator additive AWGN noise
        #k = awgn(k,Pt[u])
        comb[_] = k

    eq1 = comb[0]
    eq2 = comb[1]
    eq3 = comb[2]
    eq4 = comb[3]
    eq5 = comb[4]
    
    dec1 = NOMA_SIC(eq1, h1, pt[u], alpha, modem)
    dec2 = NOMA_SIC(eq2, h2, pt[u], alpha, modem)
    dec3 = NOMA_SIC(eq3, h3, pt[u], alpha, modem)
    dec4 = NOMA_SIC(eq4, h4, pt[u], alpha, modem)
    dec5 = NOMA_SIC(eq5, h5, pt[u], alpha, modem)

    ber1[u] = np.sum( dec1['0'] != SPC['0'])/N
    ber2[u] = np.sum( dec2['1'] != SPC['1'])/N
    ber3[u] = np.sum( dec3['2'] != SPC['2'])/N
    ber4[u] = np.sum( dec3['3'] != SPC['3'])/N
    ber5[u] = np.sum( dec3['4'] != SPC['4'])/N

    rem1 = dec1['rem0']
    rem2 = dec2['rem1']
    rem3 = dec3['rem2']
    rem4 = dec4['rem3']
    rem5 = dec5['rem4']

    # input signal SPC no need h
    decxx = NOMA_SIC(np.sqrt(pt[u])*x, h[0], pt[u], alpha, modem)
    remm1 = decxx['rem0']
    remm2 = decxx['rem1']
    remm3 = decxx['rem2']
    remm4 = decxx['rem3']
    remm5 = decxx['rem4']

    # compute abs avoid np.sqrt get -value less than 0
    # EVM = np.sqrt( ( (I-I')**2+(Q-Q')**2 ) /N )
    # EVM% = np.sqrt( ( (I-I')**2+(Q-Q')**2 ) /N )*100
    # EVM dB = 20*np.log10(EVM) 3% is ~= -30.45757490560675 dB
    EVM1 = np.sqrt( sum(abs( (rem1.real-remm1.real)**2 -(rem1.imag-remm1.imag)**2 )) /nSample )
    EVM2 = np.sqrt( sum(abs( (rem2.real-remm2.real)**2 -(rem2.imag-remm2.imag)**2 )) /nSample )
    EVM3 = np.sqrt( sum(abs( (rem3.real-remm3.real)**2 -(rem3.imag-remm3.imag)**2 )) /nSample )
    EVM4 = np.sqrt( sum(abs( (rem4.real-remm4.real)**2 -(rem4.imag-remm4.imag)**2 )) /nSample )
    EVM5 = np.sqrt( sum(abs( (rem5.real-remm5.real)**2 -(rem5.imag-remm5.imag)**2 )) /nSample )
    EVM1_dB = 20*np.log10(EVM1)
    EVM2_dB = 20*np.log10(EVM2)
    EVM3_dB = 20*np.log10(EVM3)
    EVM4_dB = 20*np.log10(EVM4)
    EVM5_dB = 20*np.log10(EVM5)
    
fig2 = plt.figure()
plt.semilogy(Pt, ber1,'r-',marker='o',label='UE1 弱用戶 %dm'%d1)
plt.semilogy(Pt, ber2,'g--',marker='o',label='UE2 %dm'%d2)
plt.semilogy(Pt, ber3,'b-.',marker='o',label='UE3 %dm'%d3)
plt.semilogy(Pt, ber4,'c:',marker='o',label='UE4 %dm'%d4)
plt.semilogy(Pt, ber5,'k-',marker='v',label='UE5 強用戶 %dm'%d5,markerfacecolor='none')
plt.title('NOMA multiple Users')
plt.xlabel('Transmit power (dBm)')
plt.ylabel('BER')
plt.legend()
plt.grid(True,which='both')
plt.tight_layout()


fig = plt.figure()
plt.title('Constellation')
plt.plot(np.real( rem1), np.imag( rem1  ), 'ro',label = 'UE1 EVM=%.3f'%(EVM1*100)+'%',markerfacecolor='none')
plt.plot(np.real( rem2 ), np.imag( rem2  ), 'gv',label = 'UE2 EVM=%.3f'%(EVM2*100)+'%',markerfacecolor='none')
plt.plot(np.real( rem3 ), np.imag( rem3  ), 'bs',label = 'UE3 EVM=%.3f'%(EVM3*100)+'%',markerfacecolor='none')
plt.plot(np.real( rem4 ), np.imag( rem4  ), 'c<',label = 'UE4 EVM=%.3f'%(EVM4*100)+'%',markerfacecolor='none')
plt.plot(np.real( rem5 ), np.imag( rem5  ), 'k*',label = 'UE5 EVM=%.3f'%(EVM5*100)+'%',markerfacecolor='none')
plt.xlabel(r'I')
plt.ylabel(r'Q')
plt.legend(loc='upper right')
plt.grid(True,which='both')
plt.tight_layout()


fig = plt.figure()
plt.title('疊加碼星座圖')
plt.plot(np.real( remm1), np.imag( remm1  ), 'ro',label = 'Dec1',markerfacecolor='none')
plt.plot(np.real( remm2 ), np.imag( remm2  ),'gv',label = 'Dec2',markerfacecolor='none')
plt.plot(np.real( remm3 ), np.imag( remm3  ),'bs',label = 'Dec3',markerfacecolor='none')
plt.plot(np.real( remm4 ), np.imag( remm4  ),'c<',label = 'Dec4',markerfacecolor='none',markersize=10)
plt.plot(np.real( remm5 ), np.imag( remm5  ),'k*',label = 'Dec5',markerfacecolor='none',markersize=10)
plt.xlabel(r'I')
plt.ylabel(r'Q')
plt.legend(loc='upper right')
plt.grid(True,which='both')
plt.tight_layout()

plt.show()

# from plotly.tools import mpl_to_plotly

# plotly_fig = mpl_to_plotly(fig)
# plotly_fig.update_layout(template="none")
# plotly_fig.write_html("constellation.html")

# plotly_fig = mpl_to_plotly(fig2)
# plotly_fig.update_layout(template="none")
# plotly_fig.write_html("BER.html")