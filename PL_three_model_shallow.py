import matplotlib.pyplot as plt
import numpy as np


def PL_free_space(fc, dist, Gt=1, Gr=1, L=1):
    l = 3*(10**8)/fc #波長
    PL = -10*np.log10(Gt*Gr*l*l/(4*np.pi*dist)**2)*L
    return PL

def PL_log(fc,dist,d0,n,sigma=0):
    l = 3*(10**8)/fc #波長
    PL = PL_free_space(fc,d0) + 10*n*np.log10(dist/d0) + sigma*np.random.randn()
    return PL

d = np.linspace(1,10000+1,100)
PL = np.zeros(len(d))
fc = 2.4*(10**9)
d0 = 1
sigma = 4
i=1
PTx=0.001 ##watt
PTxdBm=10*np.log10(PTx*1000)
PTxdBm = 0

plt.figure('Free-space path loss model')
for i in range(len(d)):
    # PL[i] = PTxdBm - PL_free_space(fc,d[i])
    PL[i] = PL_free_space(fc,d[i])
# plt.semilogx(d,PL,label='Gt=1,Gr=1')
plt.plot(d,PL,label='Gt=1,Gr=1')
for i in range(len(d)):
    # PL[i] = PTxdBm - PL_free_space(fc,d[i],1,0.5)
    PL[i] = PL_free_space(fc,d[i],1,0.5)
# plt.semilogx(d,PL,label='Gt=1,Gr=0.5')
plt.plot(d,PL,label='Gt=1,Gr=0.5')
for i in range(len(d)):
    # PL[i] = PTxdBm - PL_free_space(fc,d[i],0.5,0.5)
    PL[i] = PL_free_space(fc,d[i],0.5,0.5)
plt.semilogx(d,PL,label='Gt=0.5,Gr=0.5')
# plt.plot(d,PL,label='Gt=0.5,Gr=0.5')
plt.legend()
plt.grid(True,which='both')
plt.xlabel('distance (m)')
plt.ylabel('path loss (dB)')
plt.title(r'Free-space path loss model   fc=%.1fGHz'%(fc/(10**9)))

plt.figure('log-distance path loss model')
for j in range(3):
    if j==0:
        n=2
    elif j==1:
        n=3
    else:
        n=6
    for i in range(len(d)):
        # PL[i] = PTxdBm-PL_log(fc,d[i],d0,n)
        PL[i] = PL_log(fc,d[i],d0,n)
    plt.semilogx(d,PL,label='n={0}'.format(n))
    # plt.plot(d,PL,label='n={0}'.format(n))
plt.legend()
plt.grid(True,which='both')
plt.xlabel('distance (m)')
plt.ylabel('path loss (dB)')
plt.title(r'log-distance path loss model   fc=%.1fGHz'%(fc/(10**9)))

plt.figure('log-normal shadowing path loss model')
n = 2
for j in range(3):
    for i in range(len(d)):
        PL[i] = PL_log(fc,d[i],d0,n,sigma)
        # PL[i] = PTxdBm - PL_log(fc,d[i],d0,n,sigma)
    plt.semilogx(d,PL,label='path {0}'.format(j+1),alpha=0.6)
    # plt.plot(d,PL,label='path {0}'.format(j+1),alpha=0.6)
plt.legend()
# plt.ylim(40)
plt.grid(True,which='both')
plt.xlabel('distance (m)')
plt.ylabel('path loss (dB)')
plt.title(r'log-normal shadowing path loss model   fc=%.1fGHz, std=%ddB, PLE=%d'%(fc/(10**9),sigma,n))
plt.show()
