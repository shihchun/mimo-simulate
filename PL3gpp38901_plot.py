import numpy as np
import matplotlib.pyplot as plt
import matplotlib
plt.rcParams['axes.unicode_minus'] = False # 負號問題
plt.rcParams['mathtext.fontset'] = 'custom' 
plt.rcParams['mathtext.it'] = 'STIXGeneral:italic' 
plt.rcParams['mathtext.bf'] = 'STIXGeneral:italic:bold' 
matplotlib.rc('font', family='mingliu') # , weight='bold') # 設定新細明體
# matplotlib.rc('font', family='PingFang HK') # , weight='bold') # 設定蘋方字體

# from matplotlib.font_manager import FontManager
# fm = FontManager()
# mat_fonts = set(f.name for f in fm.ttflist)
# print (mat_fonts)
from PL3gpp38901 import PL_RMa,PL_UMa,PL_UMi,PL_InH
# Code
PTx=0.001 ##watt
PTxdBm=10*np.log10(PTx*1000)

# fc=3.5GHz, 28GHz
fc=3.5; htx=70; hrx=1.5
distance=np.linspace(10,5000,5000)
d = np.linspace(10,150,500) # InH indor office

y_RMa_LOS = PL_RMa(fc,distance.copy(),htx,hrx) # np.copy cuz function change my value !? python bug
y_RMa_NLOS = PL_RMa(fc,distance.copy(),htx,hrx,condition='NLOS')

y_RMa_LOS_shad = PL_RMa(fc,distance.copy(),htx,hrx,shad=True)
y_RMa_NLOS_shad = PL_RMa(fc,distance.copy(),htx,hrx,condition='NLOS',shad=True)

y_UMa_LOS = PL_UMa(fc,distance.copy(),hrx)
y_UMa_NLOS = PL_UMa(fc,distance.copy(),hrx,condition='NLOS')
y_UMa_LOS_shad = PL_UMa(fc,distance.copy(),hrx,shad=True)
y_UMa_NLOS_shad = PL_UMa(fc,distance.copy(),hrx,condition='NLOS',shad=True)

y_UMi_LOS = PL_UMi(fc,distance.copy(),hrx)
y_UMi_NLOS = PL_UMi(fc,distance.copy(),hrx,condition='NLOS')
y_UMi_LOS_shad = PL_UMi(fc,distance.copy(),hrx,shad=True)
y_UMi_NLOS_shad = PL_UMi(fc,distance.copy(),hrx,condition='NLOS',shad=True)

# for InH and freq blow 6GHz use 6 GHz
y_InH_LOS = PL_InH(fc,d.copy())
y_InH_NLOS = PL_InH(fc,d.copy(),condition='NLOS')

y_InH_LOS_shad = PL_InH(fc,d.copy(),shad=True)
y_InH_NLOS_shad = PL_InH(fc,d.copy(),condition='NLOS',shad=True)

plt.semilogx(distance,y_RMa_LOS_shad,label='RMa空曠地區LOS 陰影 $h_{avg}$=20m, $W_{avg}$=10m',alpha=0.6)
plt.semilogx(distance,y_RMa_NLOS_shad,label='RMa空曠地區NLOS 陰影 $h_{avg}$=20m, $W_{avg}$=10m',alpha=0.6)

plt.semilogx(distance,y_RMa_LOS,'-',linewidth=2,label='RMa空曠地區LOS $h_{avg}$=20m, $W_{avg}$=10m')
plt.semilogx(distance,y_RMa_NLOS,'--',linewidth=2,label='RMa空曠地區NLOS $h_{avg}$=20m, $W_{avg}$=10m')

plt.semilogx(distance,y_UMa_LOS_shad,label='UMa大都市LOS 陰影 $h_{BS}$=25m only',alpha=0.6)
plt.semilogx(distance,y_UMa_NLOS_shad,label='UMa大都市NLOS 陰影 $h_{BS}$=25m only',alpha=0.7)

plt.semilogx(distance,y_UMa_LOS,'-.',linewidth=2,label='UMa大都市LOS$h_{BS}$=25m only')
plt.semilogx(distance,y_UMa_NLOS,':',linewidth=3,label='UMa大都市NLOS $h_{BS}$=25m only')

# plt.semilogx(distance,y_UMi_LOS_shad,label='UMi Rural Micro LOS 陰影 $h_{BS}$=10m only',alpha=0.4)
# plt.semilogx(distance,y_UMi_NLOS_shad,label='UMi Rural Micro LOS 陰影 $h_{BS}$=10m only',alpha=0.4)

# plt.semilogx(distance,y_UMi_LOS,label='UMi Rural Micro LOS $h_{BS}$=10m only')
# plt.semilogx(distance,y_UMi_NLOS,label='UMi Rural Micro NLOS $h_{BS}$=10m only')

plt.semilogx(d,y_InH_LOS_shad,label='InH Indor Office LOS 陰影, $d_{3d}$距離',alpha=0.6)
plt.semilogx(d,y_InH_NLOS_shad,label='InH Indor Office NLOS 陰影, $d_{3d}$距離',alpha=0.4)

plt.semilogx(d,y_InH_LOS,label='InH Indor Office LOS, $d_{3d}$距離')
plt.semilogx(d,y_InH_NLOS,label='InH Indor Office NLOS, $d_{3d}$距離')

plt.title( r'3GPP 138.901模型 $h_{UT}$=%.1fm, $h_{BS}$=%.1f $f_c$=%.1fGHz'%(htx,hrx,fc) )
plt.xlabel(r'$d_{2d}$ 距離[m]'); 
plt.ylabel(r'路徑損耗 $P_{loss}$[dB]')
# plt.ylabel(r'接收平均功率 $P_{rx}$ [dB]')
plt.grid(True,which='both') 
plt.legend()
plt.tight_layout()


# def d3dd(d2d,bs,ut):
#     return np.sqrt(d2d**2+(bs-ut)**2)
# # compare to monster value, 會有誤差，他程式有些d_2d d_3d 寫錯，不過數字會接近，也會有的計算結果一樣
# # https://github.com/Sonohi/monster/blob/master/channel/3GPP38901/losProb3gpp38901.m
# area = 'UMi'
# print("loss3gpp38901('%s',%d,%.4f,%d,%d,%.1f,20,18,false)"%(area,distance[10],d3dd(distance[10],htx,hrx),fc,htx,hrx))
# print(y_UMi_NLOS[10])
# print("loss3gpp38901('%s',%d,%.4f,%d,%d,%.1f,20,18,losProb3gpp38901('%s',%d,%.1f))"%(area,distance[10],d3dd(distance[10],htx,hrx),fc,htx,hrx,area,distance[10],hrx))
# print(y_UMi_LOS[10])
# print("loss3gpp38901('%s',%d,%.4f,%d,%d,%.1f,20,18,false)"%(area,distance[-1],d3dd(distance[-1],htx,hrx),fc,htx,hrx))
# print(y_UMi_NLOS[-1])
# print("loss3gpp38901('%s',%d,%.4f,%d,%d,%.1f,20,18,losProb3gpp38901('%s',%d,%.1f))"%(area,distance[-1],d3dd(distance[-1],htx,hrx),fc,htx,hrx,area,distance[-1],hrx))
# print(y_UMi_LOS[-1])

PTx=0.001 ##watt
PTxdBm=10*np.log10(PTx*1000)

yy_RMa_LOS = PTxdBm-PL_RMa(fc,distance.copy(),htx,hrx) # np.copy cuz function change my value !? python bug
yy_RMa_NLOS = PTxdBm-PL_RMa(fc,distance.copy(),htx,hrx,condition='NLOS')

yy_RMa_LOS_shad = PTxdBm-PL_RMa(fc,distance.copy(),htx,hrx,shad=True)
yy_RMa_NLOS_shad = PTxdBm-PL_RMa(fc,distance.copy(),htx,hrx,condition='NLOS',shad=True)

yy_UMa_LOS = PTxdBm-PL_UMa(fc,distance.copy(),hrx)
yy_UMa_NLOS = PTxdBm-PL_UMa(fc,distance.copy(),hrx,condition='NLOS')
yy_UMa_LOS_shad = PTxdBm-PL_UMa(fc,distance.copy(),hrx,shad=True)
yy_UMa_NLOS_shad = PTxdBm-PL_UMa(fc,distance.copy(),hrx,condition='NLOS',shad=True)

yy_UMi_LOS = PTxdBm-PL_UMi(fc,distance.copy(),hrx)
yy_UMi_NLOS = PTxdBm-PL_UMi(fc,distance.copy(),hrx,condition='NLOS')
yy_UMi_LOS_shad = PTxdBm-PL_UMi(fc,distance.copy(),hrx,shad=True)
yy_UMi_NLOS_shad = PTxdBm-PL_UMi(fc,distance.copy(),hrx,condition='NLOS',shad=True)

yy_InH_LOS = PTxdBm-PL_InH(fc,d.copy())
yy_InH_NLOS = PTxdBm-PL_InH(fc,d.copy(),condition='NLOS')
yy_InH_LOS_shad = PTxdBm - PL_InH(fc,d.copy(),shad=True)
yy_InH_NLOS_shad = PTxdBm - PL_InH(fc,d.copy(),condition='NLOS',shad=True)

plt.figure()
plt.plot(distance,yy_RMa_LOS_shad,label='RMa空曠地區LOS 陰影 $h_{avg}$=20m, $W_{avg}$=10m',alpha=0.6)
plt.plot(distance,yy_RMa_NLOS_shad,label='RMa空曠地區NLOS 陰影 $h_{avg}$=20m, $W_{avg}$=10m',alpha=0.6)

plt.plot(distance,yy_RMa_LOS,'-',linewidth=2,label='RMa空曠地區LOS $h_{avg}$=20m, $W_{avg}$=10m')
plt.plot(distance,yy_RMa_NLOS,'--',linewidth=2,label='RMa空曠地區NLOS $h_{avg}$=20m, $W_{avg}$=10m')

plt.plot(distance,yy_UMa_LOS_shad,label='UMa大都市LOS 陰影 $h_{BS}$=25m only',alpha=0.6)
plt.plot(distance,yy_UMa_NLOS_shad,label='UMa大都市NLOS 陰影 $h_{BS}$=25m only',alpha=0.7)


plt.plot(distance,yy_UMa_LOS,'-.',linewidth=2,label='UMa大都市LOS$h_{BS}$=25m only')
plt.plot(distance,yy_UMa_NLOS,':',linewidth=3,label='UMa大都市NLOS $h_{BS}$=25m only')


# plt.plot(distance,yy_UMi_LOS_shad,label='UMi Rural Micro LOS 陰影 $h_{BS}$=10m only',alpha=0.4)
# plt.plot(distance,yy_UMi_NLOS_shad,label='UMi Rural Micro LOS 陰影 $h_{BS}$=10m only',alpha=0.4)

# plt.plot(distance,yy_UMi_LOS,label='UMi Rural Micro LOS $h_{BS}$=10m only')
# plt.plot(distance,yy_UMi_NLOS,label='UMi Rural Micro NLOS $h_{BS}$=10m only')

plt.plot(d,yy_InH_LOS_shad,label='InH Indor Office LOS 陰影, $d_{3d}$距離',alpha=0.6)
plt.plot(d,yy_InH_NLOS_shad,label='InH Indor Office NLOS 陰影, $d_{3d}$距離',alpha=0.4)

plt.plot(d,yy_InH_LOS,label='InH Indor Office LOS, $d_{3d}$距離')
plt.plot(d,yy_InH_NLOS,label='InH Indor Office NLOS, $d_{3d}$距離')


plt.title( r'3GPP 138.901模型 $P_{Tx}$=1mW, $h_{UT}$=%.1fm, $h_{BS}$=%.1f $f_c$=%.1fGHz'%(htx,hrx,fc) )
plt.xlabel(r'$d_{2d}$ 距離[m]'); 
plt.ylabel(r'接收平均功率 $P_{rx}$ [dB]')
plt.grid(True,which='both') 
plt.legend()
plt.tight_layout()
plt.show()