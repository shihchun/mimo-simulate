import matplotlib.pyplot as plt
import numpy as np

# https://www.mathworks.com/matlabcentral/fileexchange/46854-log_normal_shadowing-m
# https://ww2.mathworks.cn/help/matlab/math/updating-your-random-number-generator-syntplt.html

d = 2**(np.arange(11)+1) # 對數距離 m
PL = np.zeros(11)
Pr_power = np.zeros(11)
fc = 1.5*(10**9)
d0 = 1
sigma = 3
i=1
plt.figure('Log-distance path loss model')
for n in [2,3,4,6]: # PLE
    for i in range(len(d)):
        Pr_power[i] = 10*np.log10(float(d[i])**(-n) *10**3)
        print(Pr_power[i])
    plt.plot(d,Pr_power,marker='o',label=r'$\eta={0}$'.format(n))

plt.legend()
plt.grid(True,which='both')

plt.xlabel('distance (m)')
plt.ylabel('path loss (dB)')
plt.title(r'log-distance path loss model $ pathloss = \sqrt{d^{(-\eta)}}$ $\eta$=Pathloss exponent')
plt.grid(True,which='both')
plt.tight_layout()
plt.show()
